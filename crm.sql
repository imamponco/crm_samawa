-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 02 Feb 2020 pada 15.04
-- Versi server: 10.1.36-MariaDB
-- Versi PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crm`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `agen`
--

CREATE TABLE `agen` (
  `no_agen` int(11) NOT NULL,
  `id_agen` varchar(5) NOT NULL,
  `nama_agen` varchar(20) DEFAULT NULL,
  `jenis_kelamin_agen` varchar(1) DEFAULT NULL,
  `no_telp_agen` varchar(15) DEFAULT NULL,
  `alamat_agen` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `agen`
--

INSERT INTO `agen` (`no_agen`, `id_agen`, `nama_agen`, `jenis_kelamin_agen`, `no_telp_agen`, `alamat_agen`) VALUES
(1, 'AG001', 'Gaido', 'M', '08123123123', 'Oke'),
(2, 'AG002', 'Junaedi', 'M', '08591210', 'Mantapancing'),
(3, 'AG003', 'Rahmadi', 'M', '0812339910', 'Lengkap');

-- --------------------------------------------------------

--
-- Struktur dari tabel `content`
--

CREATE TABLE `content` (
  `no_content` int(11) NOT NULL,
  `id_content` varchar(5) NOT NULL,
  `id_user` varchar(5) NOT NULL,
  `jenis_content` varchar(6) DEFAULT NULL,
  `message` text NOT NULL,
  `created_ct_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `content`
--

INSERT INTO `content` (`no_content`, `id_content`, `id_user`, `jenis_content`, `message`, `created_ct_at`) VALUES
(8, 'CT008', 'AM003', 'KDR004', 'Ada apa nih', '2020-01-18 06:15:42'),
(11, 'CT011', 'AM003', 'KDR004', 'Gajelas', '2020-01-18 10:17:35'),
(13, 'CT013', 'CO001', 'PR013', 'yow', '2020-01-26 11:39:51');

-- --------------------------------------------------------

--
-- Struktur dari tabel `country`
--

CREATE TABLE `country` (
  `id_country` int(11) NOT NULL,
  `country_code` varchar(2) NOT NULL DEFAULT '',
  `country_name` varchar(100) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `country`
--

INSERT INTO `country` (`id_country`, `country_code`, `country_name`) VALUES
(1, 'AF', 'Afghanistan'),
(2, 'AL', 'Albania'),
(3, 'DZ', 'Algeria'),
(4, 'DS', 'American Samoa'),
(5, 'AD', 'Andorra'),
(6, 'AO', 'Angola'),
(7, 'AI', 'Anguilla'),
(8, 'AQ', 'Antarctica'),
(9, 'AG', 'Antigua and Barbuda'),
(10, 'AR', 'Argentina'),
(11, 'AM', 'Armenia'),
(12, 'AW', 'Aruba'),
(13, 'AU', 'Australia'),
(14, 'AT', 'Austria'),
(15, 'AZ', 'Azerbaijan'),
(16, 'BS', 'Bahamas'),
(17, 'BH', 'Bahrain'),
(18, 'BD', 'Bangladesh'),
(19, 'BB', 'Barbados'),
(20, 'BY', 'Belarus'),
(21, 'BE', 'Belgium'),
(22, 'BZ', 'Belize'),
(23, 'BJ', 'Benin'),
(24, 'BM', 'Bermuda'),
(25, 'BT', 'Bhutan'),
(26, 'BO', 'Bolivia'),
(27, 'BA', 'Bosnia and Herzegovina'),
(28, 'BW', 'Botswana'),
(29, 'BV', 'Bouvet Island'),
(30, 'BR', 'Brazil'),
(31, 'IO', 'British Indian Ocean Territory'),
(32, 'BN', 'Brunei Darussalam'),
(33, 'BG', 'Bulgaria'),
(34, 'BF', 'Burkina Faso'),
(35, 'BI', 'Burundi'),
(36, 'KH', 'Cambodia'),
(37, 'CM', 'Cameroon'),
(38, 'CA', 'Canada'),
(39, 'CV', 'Cape Verde'),
(40, 'KY', 'Cayman Islands'),
(41, 'CF', 'Central African Republic'),
(42, 'TD', 'Chad'),
(43, 'CL', 'Chile'),
(44, 'CN', 'China'),
(45, 'CX', 'Christmas Island'),
(46, 'CC', 'Cocos (Keeling) Islands'),
(47, 'CO', 'Colombia'),
(48, 'KM', 'Comoros'),
(49, 'CD', 'Democratic Republic of the Congo'),
(50, 'CG', 'Republic of Congo'),
(51, 'CK', 'Cook Islands'),
(52, 'CR', 'Costa Rica'),
(53, 'HR', 'Croatia (Hrvatska)'),
(54, 'CU', 'Cuba'),
(55, 'CY', 'Cyprus'),
(56, 'CZ', 'Czech Republic'),
(57, 'DK', 'Denmark'),
(58, 'DJ', 'Djibouti'),
(59, 'DM', 'Dominica'),
(60, 'DO', 'Dominican Republic'),
(61, 'TP', 'East Timor'),
(62, 'EC', 'Ecuador'),
(63, 'EG', 'Egypt'),
(64, 'SV', 'El Salvador'),
(65, 'GQ', 'Equatorial Guinea'),
(66, 'ER', 'Eritrea'),
(67, 'EE', 'Estonia'),
(68, 'ET', 'Ethiopia'),
(69, 'FK', 'Falkland Islands (Malvinas)'),
(70, 'FO', 'Faroe Islands'),
(71, 'FJ', 'Fiji'),
(72, 'FI', 'Finland'),
(73, 'FR', 'France'),
(74, 'FX', 'France, Metropolitan'),
(75, 'GF', 'French Guiana'),
(76, 'PF', 'French Polynesia'),
(77, 'TF', 'French Southern Territories'),
(78, 'GA', 'Gabon'),
(79, 'GM', 'Gambia'),
(80, 'GE', 'Georgia'),
(81, 'DE', 'Germany'),
(82, 'GH', 'Ghana'),
(83, 'GI', 'Gibraltar'),
(84, 'GK', 'Guernsey'),
(85, 'GR', 'Greece'),
(86, 'GL', 'Greenland'),
(87, 'GD', 'Grenada'),
(88, 'GP', 'Guadeloupe'),
(89, 'GU', 'Guam'),
(90, 'GT', 'Guatemala'),
(91, 'GN', 'Guinea'),
(92, 'GW', 'Guinea-Bissau'),
(93, 'GY', 'Guyana'),
(94, 'HT', 'Haiti'),
(95, 'HM', 'Heard and Mc Donald Islands'),
(96, 'HN', 'Honduras'),
(97, 'HK', 'Hong Kong'),
(98, 'HU', 'Hungary'),
(99, 'IS', 'Iceland'),
(100, 'IN', 'India'),
(101, 'IM', 'Isle of Man'),
(102, 'ID', 'Indonesia'),
(103, 'IR', 'Iran (Islamic Republic of)'),
(104, 'IQ', 'Iraq'),
(105, 'IE', 'Ireland'),
(106, 'IL', 'Israel'),
(107, 'IT', 'Italy'),
(108, 'CI', 'Ivory Coast'),
(109, 'JE', 'Jersey'),
(110, 'JM', 'Jamaica'),
(111, 'JP', 'Japan'),
(112, 'JO', 'Jordan'),
(113, 'KZ', 'Kazakhstan'),
(114, 'KE', 'Kenya'),
(115, 'KI', 'Kiribati'),
(116, 'KP', 'Korea, Democratic People\'s Republic of'),
(117, 'KR', 'Korea, Republic of'),
(118, 'XK', 'Kosovo'),
(119, 'KW', 'Kuwait'),
(120, 'KG', 'Kyrgyzstan'),
(121, 'LA', 'Lao People\'s Democratic Republic'),
(122, 'LV', 'Latvia'),
(123, 'LB', 'Lebanon'),
(124, 'LS', 'Lesotho'),
(125, 'LR', 'Liberia'),
(126, 'LY', 'Libyan Arab Jamahiriya'),
(127, 'LI', 'Liechtenstein'),
(128, 'LT', 'Lithuania'),
(129, 'LU', 'Luxembourg'),
(130, 'MO', 'Macau'),
(131, 'MK', 'North Macedonia'),
(132, 'MG', 'Madagascar'),
(133, 'MW', 'Malawi'),
(134, 'MY', 'Malaysia'),
(135, 'MV', 'Maldives'),
(136, 'ML', 'Mali'),
(137, 'MT', 'Malta'),
(138, 'MH', 'Marshall Islands'),
(139, 'MQ', 'Martinique'),
(140, 'MR', 'Mauritania'),
(141, 'MU', 'Mauritius'),
(142, 'TY', 'Mayotte'),
(143, 'MX', 'Mexico'),
(144, 'FM', 'Micronesia, Federated States of'),
(145, 'MD', 'Moldova, Republic of'),
(146, 'MC', 'Monaco'),
(147, 'MN', 'Mongolia'),
(148, 'ME', 'Montenegro'),
(149, 'MS', 'Montserrat'),
(150, 'MA', 'Morocco'),
(151, 'MZ', 'Mozambique'),
(152, 'MM', 'Myanmar'),
(153, 'NA', 'Namibia'),
(154, 'NR', 'Nauru'),
(155, 'NP', 'Nepal'),
(156, 'NL', 'Netherlands'),
(157, 'AN', 'Netherlands Antilles'),
(158, 'NC', 'New Caledonia'),
(159, 'NZ', 'New Zealand'),
(160, 'NI', 'Nicaragua'),
(161, 'NE', 'Niger'),
(162, 'NG', 'Nigeria'),
(163, 'NU', 'Niue'),
(164, 'NF', 'Norfolk Island'),
(165, 'MP', 'Northern Mariana Islands'),
(166, 'NO', 'Norway'),
(167, 'OM', 'Oman'),
(168, 'PK', 'Pakistan'),
(169, 'PW', 'Palau'),
(170, 'PS', 'Palestine'),
(171, 'PA', 'Panama'),
(172, 'PG', 'Papua New Guinea'),
(173, 'PY', 'Paraguay'),
(174, 'PE', 'Peru'),
(175, 'PH', 'Philippines'),
(176, 'PN', 'Pitcairn'),
(177, 'PL', 'Poland'),
(178, 'PT', 'Portugal'),
(179, 'PR', 'Puerto Rico'),
(180, 'QA', 'Qatar'),
(181, 'RE', 'Reunion'),
(182, 'RO', 'Romania'),
(183, 'RU', 'Russian Federation'),
(184, 'RW', 'Rwanda'),
(185, 'KN', 'Saint Kitts and Nevis'),
(186, 'LC', 'Saint Lucia'),
(187, 'VC', 'Saint Vincent and the Grenadines'),
(188, 'WS', 'Samoa'),
(189, 'SM', 'San Marino'),
(190, 'ST', 'Sao Tome and Principe'),
(191, 'SA', 'Saudi Arabia'),
(192, 'SN', 'Senegal'),
(193, 'RS', 'Serbia'),
(194, 'SC', 'Seychelles'),
(195, 'SL', 'Sierra Leone'),
(196, 'SG', 'Singapore'),
(197, 'SK', 'Slovakia'),
(198, 'SI', 'Slovenia'),
(199, 'SB', 'Solomon Islands'),
(200, 'SO', 'Somalia'),
(201, 'ZA', 'South Africa'),
(202, 'GS', 'South Georgia South Sandwich Islands'),
(203, 'SS', 'South Sudan'),
(204, 'ES', 'Spain'),
(205, 'LK', 'Sri Lanka'),
(206, 'SH', 'St. Helena'),
(207, 'PM', 'St. Pierre and Miquelon'),
(208, 'SD', 'Sudan'),
(209, 'SR', 'Suriname'),
(210, 'SJ', 'Svalbard and Jan Mayen Islands'),
(211, 'SZ', 'Swaziland'),
(212, 'SE', 'Sweden'),
(213, 'CH', 'Switzerland'),
(214, 'SY', 'Syrian Arab Republic'),
(215, 'TW', 'Taiwan'),
(216, 'TJ', 'Tajikistan'),
(217, 'TZ', 'Tanzania, United Republic of'),
(218, 'TH', 'Thailand'),
(219, 'TG', 'Togo'),
(220, 'TK', 'Tokelau'),
(221, 'TO', 'Tonga'),
(222, 'TT', 'Trinidad and Tobago'),
(223, 'TN', 'Tunisia'),
(224, 'TR', 'Turkey'),
(225, 'TM', 'Turkmenistan'),
(226, 'TC', 'Turks and Caicos Islands'),
(227, 'TV', 'Tuvalu'),
(228, 'UG', 'Uganda'),
(229, 'UA', 'Ukraine'),
(230, 'AE', 'United Arab Emirates'),
(231, 'GB', 'United Kingdom'),
(232, 'US', 'United States'),
(233, 'UM', 'United States minor outlying islands'),
(234, 'UY', 'Uruguay'),
(235, 'UZ', 'Uzbekistan'),
(236, 'VU', 'Vanuatu'),
(237, 'VA', 'Vatican City State'),
(238, 'VE', 'Venezuela'),
(239, 'VN', 'Vietnam'),
(240, 'VG', 'Virgin Islands (British)'),
(241, 'VI', 'Virgin Islands (U.S.)'),
(242, 'WF', 'Wallis and Futuna Islands'),
(243, 'EH', 'Western Sahara'),
(244, 'YE', 'Yemen'),
(245, 'ZM', 'Zambia'),
(246, 'ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_pemesanan`
--

CREATE TABLE `detail_pemesanan` (
  `no_dt` int(11) NOT NULL,
  `id_detail_pemesanan` varchar(6) NOT NULL,
  `id_header_pemesanan` varchar(5) NOT NULL,
  `id_harga` varchar(5) DEFAULT NULL,
  `id_paket` varchar(5) DEFAULT NULL,
  `id_jadwal` varchar(5) DEFAULT NULL,
  `pax` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_pemesanan`
--

INSERT INTO `detail_pemesanan` (`no_dt`, `id_detail_pemesanan`, `id_header_pemesanan`, `id_harga`, `id_paket`, `id_jadwal`, `pax`) VALUES
(4, 'DET004', 'HD004', 'HR005', 'P001', 'J001', 1),
(6, 'DET006', 'HD006', 'HR002', 'P002', 'J003', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `equipment`
--

CREATE TABLE `equipment` (
  `no_eq` int(11) NOT NULL,
  `id_barang` varchar(5) NOT NULL,
  `id_user` varchar(5) NOT NULL,
  `nama_barang` varchar(25) NOT NULL,
  `stok` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `equipment`
--

INSERT INTO `equipment` (`no_eq`, `id_barang`, `id_user`, `nama_barang`, `stok`) VALUES
(1, 'BR001', 'AO002', 'Tas Jansport', 105),
(4, 'BR004', 'AO002', 'Koper Samawa', 50),
(5, 'BR005', 'AO002', 'Tas Slempang', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `harga`
--

CREATE TABLE `harga` (
  `no_hr` int(11) NOT NULL,
  `id_harga` varchar(5) NOT NULL,
  `id_paket` varchar(5) DEFAULT NULL,
  `diskon` int(11) UNSIGNED NOT NULL,
  `tax` int(11) UNSIGNED NOT NULL,
  `other` int(11) UNSIGNED NOT NULL,
  `jumlah` int(11) UNSIGNED NOT NULL,
  `created_at` date DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `harga`
--

INSERT INTO `harga` (`no_hr`, `id_harga`, `id_paket`, `diskon`, `tax`, `other`, `jumlah`, `created_at`, `deleted_at`) VALUES
(1, 'HR001', 'P001', 250000, 0, 0, 25000000, '2019-11-20', '2020-01-12'),
(2, 'HR002', 'P002', 0, 0, 0, 35000000, '2020-01-03', NULL),
(3, 'HR003', 'P003', 0, 0, 0, 20000000, '2020-01-11', NULL),
(4, 'HR004', 'P001', 0, 0, 0, 25000000, '2020-01-12', '2020-01-12'),
(5, 'HR005', 'P001', 500000, 2000, 500, 25000000, '2020-01-12', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `header_pemesanan`
--

CREATE TABLE `header_pemesanan` (
  `no_hd` int(11) NOT NULL,
  `id_header_pemesanan` varchar(5) NOT NULL,
  `nik` varchar(17) NOT NULL,
  `tanggal_pesan` date DEFAULT NULL,
  `subtotal` int(11) DEFAULT NULL,
  `grand_total` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `bukti_pembayaran` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `header_pemesanan`
--

INSERT INTO `header_pemesanan` (`no_hd`, `id_header_pemesanan`, `nik`, `tanggal_pesan`, `subtotal`, `grand_total`, `status`, `bukti_pembayaran`) VALUES
(4, 'HD004', '123456', '2020-01-18', 25000000, 24502500, 1, 'b68c4a2942a4424ae9a3aed18e18baac.jpg'),
(6, 'HD006', '123456', '2020-02-02', 35000000, 35000000, 1, 'b2d8aee7139bc295dad9f882a26f16a4.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jadwal`
--

CREATE TABLE `jadwal` (
  `no_jd` int(11) NOT NULL,
  `id_jadwal` varchar(5) NOT NULL,
  `id_paket` varchar(5) NOT NULL,
  `nama_maskapai` varchar(25) NOT NULL,
  `no_flight` varchar(10) NOT NULL,
  `tgl_keberangkatan` date NOT NULL,
  `kuota` int(3) NOT NULL,
  `kota_asal` varchar(25) NOT NULL,
  `jam_terbang` time NOT NULL,
  `kota_tujuan` varchar(25) NOT NULL,
  `jam_tiba` time NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jadwal`
--

INSERT INTO `jadwal` (`no_jd`, `id_jadwal`, `id_paket`, `nama_maskapai`, `no_flight`, `tgl_keberangkatan`, `kuota`, `kota_asal`, `jam_terbang`, `kota_tujuan`, `jam_tiba`, `status`) VALUES
(1, 'J001', 'P001', 'Qatar Airways', '30994885', '2020-12-16', 30, 'Jakarta', '13:53:00', 'Osaka', '20:52:00', 1),
(2, 'J002', 'P001', 'Garuda Indonesia', '991002210', '2018-12-21', 20, 'Jakarta', '23:08:00', 'Tokyo', '07:00:00', 0),
(3, 'J003', 'P002', 'Lion Air', '309201319', '2020-01-20', 10, 'Jakarta', '10:00:00', 'Jogjakarta', '12:00:00', 0),
(4, 'J004', 'P003', 'Qatar Airline', '3001239120', '2021-12-12', 30, 'Jakarta', '03:03:00', 'Uzbekiztan', '08:00:00', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `paket`
--

CREATE TABLE `paket` (
  `no_paket` int(11) NOT NULL,
  `id_paket` varchar(5) NOT NULL,
  `nama_paket` varchar(30) DEFAULT NULL,
  `gambar_paket` varchar(100) DEFAULT NULL,
  `country_code` varchar(2) DEFAULT NULL,
  `durasi` int(3) DEFAULT NULL,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `paket`
--

INSERT INTO `paket` (`no_paket`, `id_paket`, `nama_paket`, `gambar_paket`, `country_code`, `durasi`, `keterangan`) VALUES
(1, 'P001', 'Japan Tour 7 Night', '3e940af42d87b137dd44108bab4ac33f.jpg', 'JP', 7, 'Mantap japan'),
(2, 'P002', 'Borobudur Paket', 'cfb7ad007f4eefe2b097c999fb453d8c.jpg', 'ID', 5, 'Borobudur trip mantap'),
(3, 'P003', 'Uzbekistan Tour', 'a3da39442111675a7a1624a56bc19630.jpg', 'UZ', 7, 'Mari jalan jalan ke uzbekistan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pelanggan`
--

CREATE TABLE `pelanggan` (
  `nik` varchar(17) NOT NULL,
  `id_user` varchar(5) NOT NULL,
  `id_agen` varchar(5) NOT NULL,
  `jenis_kelamin` varchar(1) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `tempat_lahir` varchar(20) DEFAULT NULL,
  `no_telp` varchar(15) DEFAULT NULL,
  `alamat` text,
  `no_passport` varchar(17) DEFAULT NULL,
  `relationship` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pelanggan`
--

INSERT INTO `pelanggan` (`nik`, `id_user`, `id_agen`, `jenis_kelamin`, `tanggal_lahir`, `tempat_lahir`, `no_telp`, `alamat`, `no_passport`, `relationship`) VALUES
('12345', 'CO023', 'AG001', 'F', '2017-10-30', 'fghjk', '1267', 'xscdfghj', 'Zxcdfbgnhjk', NULL),
('123456', 'CO001', 'AG001', 'M', '1979-09-19', 'Jakarta', '0859291010', 'Jalan jalan yuk', '123456', 'Belum Menikah'),
('1234567', 'CO025', 'AG001', 'M', '2019-12-28', 'Jakarta', '1234567', 'asd', '12345678', NULL),
('12345678901', 'CO026', 'AG001', 'M', '2019-01-31', 'Jakarta', '1234567890', 'asd', '12314567891', NULL),
('312312312312', 'CO021', 'AG001', 'M', '2018-12-31', 'Jakarta', '123123123123', 'ehehehe', NULL, NULL),
('32109120', 'CO024', 'AG001', 'M', '2020-01-28', 'Jakarta', '123456', 'Jalan', '1234567', NULL),
('3213123', 'CO017', 'AG001', 'M', '2004-12-31', 'Jakarta', '123', 'asd', '654321', 'Belum Menikah'),
('3321312312', 'CO022', 'AG002', 'M', '2005-08-31', 'Bandung', '0857271812389', 'ehehe', '30123912371', 'Menikah');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi_barang`
--

CREATE TABLE `transaksi_barang` (
  `no_transaksi` int(11) NOT NULL,
  `id_transaksi_barang` varchar(5) NOT NULL,
  `id_barang` varchar(5) NOT NULL,
  `jumlah_barang` int(11) NOT NULL,
  `total_harga` int(11) NOT NULL,
  `created_tr_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi_barang`
--

INSERT INTO `transaksi_barang` (`no_transaksi`, `id_transaksi_barang`, `id_barang`, `jumlah_barang`, `total_harga`, `created_tr_at`) VALUES
(3, 'TB003', 'BR001', 20, 2500000, '2019-11-21'),
(4, 'TB004', 'BR001', 5, 150000, '2020-01-18');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `no` int(11) NOT NULL,
  `id_user` varchar(5) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(60) NOT NULL,
  `email` varchar(30) NOT NULL,
  `verified_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`no`, `id_user`, `username`, `password`, `email`, `verified_at`) VALUES
(5, 'AF005', 'Putri Syarifun', '$2y$10$Of48K4xyGVliYXnv3N80wOs.o6GrFyo0xDmRGoW15YGWw.RcZhEHe', 'finance@mail.com', '2019-11-21 01:40:12'),
(3, 'AM003', 'Shaqila', '$2y$10$Qy8RoyhaQ42XXIvGjMNFU.OpopYMCTLx.vX1r9HXlFFwzz/8BAtd.', 'marketing@mail.com', '2019-11-21 09:40:34'),
(1, 'AO001', 'Muhammad Rieza', '$2y$10$O.24bpZwCCCMbSDliR.M1.JA77.GceZCzZ6M6q.KtYNAmbVoB4/gq', 'rieza@mail.com', '2019-11-21 08:53:57'),
(2, 'AO002', 'Muhammad Afgan Syahriroti', '$2y$10$2oMHzb80jZUe/JlMXVp3eehFiXFkWYHTzC.ZgPKZJyPMe/Kw3n2uu', 'operasional@mail.com', '2019-11-21 08:53:53'),
(6, 'AV006', 'Wakhid Arizki', '$2y$10$HboY9EbWXxEQb7rtbME1/uYIqQfM7PgXQUURHjg2kNNuAXxnpH0o6', 'visa@mail.com', '2019-11-21 01:56:49'),
(4, 'CO001', 'Dhani', '$2y$10$Bf3O/uapu2AfO8zhOya2MuupWP.JuMnSdueviF20DKgVq9KIENd9.', 'pelanggan@mail.com', '2019-12-01 22:36:12'),
(17, 'CO017', 'imamponco', '$2y$10$dex3BQMRCMK6LocjtZFVQOtkxyFapohDCJoT8Zg92Jar8JZmUOQoW', 'user@mail.com', '2020-01-03 08:56:45'),
(21, 'CO021', 'user2', '$2y$10$18U4qnHNomRMj4d0WW/IXOmJ1kTrjjz/gszZgBM/ovtCOwvVmXpPS', 'user3@mail.com', NULL),
(22, 'CO022', 'Sulistio', '$2y$10$Cqy6XHdtk6EKFaqMWY9UiOVn9PqpG.PFkOtoTgZu.iWiLJVMK.fzO', 'yoman@mail.com', '2019-12-29 14:25:32'),
(23, 'CO023', 'fghjhghgjhl', '$2y$10$3GhIl0f.h.Vg/XoQO2Az2egGyz6lDheqFUuZwfh7KY3PBzwTN/7jW', 'imam@gmail.com', NULL),
(24, 'CO024', 'test', '$2y$10$FeV3Aqk0RQmoAN2jiUOgTunECiTc3fhg8dUHhRz4MHgaWgADHXdCG', 'pelanggan2@mail.com', '2020-01-28 02:15:02'),
(25, 'CO025', 'test', '$2y$10$OKj6XwucPfa1YgTaa/SmO.QYAqt2d5vRTqhVa97VK6gXjaAZ3S3nW', 'test@mail.com', '2020-01-29 11:26:32'),
(26, 'CO026', 'coba dong', '$2y$10$jIMY4OROnrqgJkPjBIU3gesV3W1f9ryczoe1c67/3juIy02drNqjW', 'coba99@mail.com', NULL),
(7, 'PD007', 'H Narkowi', '$2y$10$fmWFqWQMeZKUHwgngPbJTuYG2C8ra35sVgMkLZIeevGCLH.7wckHu', 'direktur@mail.com', '2019-11-21 02:48:48');

-- --------------------------------------------------------

--
-- Struktur dari tabel `visa`
--

CREATE TABLE `visa` (
  `no_visa` int(11) NOT NULL,
  `id_visa` varchar(5) NOT NULL,
  `id_header_pemesanan` varchar(5) NOT NULL,
  `nik` varchar(17) NOT NULL,
  `issuing_office` varchar(25) NOT NULL,
  `date_of_issue` date NOT NULL,
  `date_of_expiry` date NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `agen`
--
ALTER TABLE `agen`
  ADD PRIMARY KEY (`id_agen`),
  ADD UNIQUE KEY `no_agen` (`no_agen`);

--
-- Indeks untuk tabel `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`id_content`) USING BTREE,
  ADD UNIQUE KEY `no_content` (`no_content`) USING BTREE,
  ADD KEY `fk_id_user_content` (`id_user`);

--
-- Indeks untuk tabel `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`country_code`),
  ADD UNIQUE KEY `id` (`id_country`);

--
-- Indeks untuk tabel `detail_pemesanan`
--
ALTER TABLE `detail_pemesanan`
  ADD PRIMARY KEY (`id_detail_pemesanan`),
  ADD KEY `no_dt` (`no_dt`),
  ADD KEY `fk_harga` (`id_harga`),
  ADD KEY `fk_id_paket` (`id_paket`),
  ADD KEY `fk_jadwal` (`id_jadwal`),
  ADD KEY `fk_header_pemesanan` (`id_header_pemesanan`);

--
-- Indeks untuk tabel `equipment`
--
ALTER TABLE `equipment`
  ADD PRIMARY KEY (`id_barang`),
  ADD KEY `no_eq` (`no_eq`),
  ADD KEY `fk_user_equipment` (`id_user`);

--
-- Indeks untuk tabel `harga`
--
ALTER TABLE `harga`
  ADD PRIMARY KEY (`id_harga`),
  ADD KEY `no_eq` (`no_hr`),
  ADD KEY `fk_paket_harga` (`id_paket`);

--
-- Indeks untuk tabel `header_pemesanan`
--
ALTER TABLE `header_pemesanan`
  ADD PRIMARY KEY (`id_header_pemesanan`),
  ADD KEY `no_hd` (`no_hd`),
  ADD KEY `fk_pelanggan_header` (`nik`);

--
-- Indeks untuk tabel `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`id_jadwal`),
  ADD KEY `no_jd` (`no_jd`),
  ADD KEY `id_paket` (`id_paket`);

--
-- Indeks untuk tabel `paket`
--
ALTER TABLE `paket`
  ADD PRIMARY KEY (`id_paket`),
  ADD UNIQUE KEY `no_paket` (`no_paket`);

--
-- Indeks untuk tabel `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`nik`),
  ADD KEY `fk_user` (`id_user`),
  ADD KEY `fk_agen` (`id_agen`);

--
-- Indeks untuk tabel `transaksi_barang`
--
ALTER TABLE `transaksi_barang`
  ADD PRIMARY KEY (`id_transaksi_barang`),
  ADD KEY `no_transaksi` (`no_transaksi`),
  ADD KEY `fk_id_barang` (`id_barang`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`) USING BTREE,
  ADD UNIQUE KEY `no` (`no`) USING BTREE;

--
-- Indeks untuk tabel `visa`
--
ALTER TABLE `visa`
  ADD PRIMARY KEY (`id_visa`),
  ADD KEY `no_visa` (`no_visa`),
  ADD KEY `fk_nik` (`nik`),
  ADD KEY `fk_header_pemesanan_visa` (`id_header_pemesanan`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `agen`
--
ALTER TABLE `agen`
  MODIFY `no_agen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `content`
--
ALTER TABLE `content`
  MODIFY `no_content` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT untuk tabel `country`
--
ALTER TABLE `country`
  MODIFY `id_country` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;

--
-- AUTO_INCREMENT untuk tabel `detail_pemesanan`
--
ALTER TABLE `detail_pemesanan`
  MODIFY `no_dt` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `equipment`
--
ALTER TABLE `equipment`
  MODIFY `no_eq` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `harga`
--
ALTER TABLE `harga`
  MODIFY `no_hr` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `header_pemesanan`
--
ALTER TABLE `header_pemesanan`
  MODIFY `no_hd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `jadwal`
--
ALTER TABLE `jadwal`
  MODIFY `no_jd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `paket`
--
ALTER TABLE `paket`
  MODIFY `no_paket` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `transaksi_barang`
--
ALTER TABLE `transaksi_barang`
  MODIFY `no_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT untuk tabel `visa`
--
ALTER TABLE `visa`
  MODIFY `no_visa` int(11) NOT NULL AUTO_INCREMENT;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `content`
--
ALTER TABLE `content`
  ADD CONSTRAINT `fk_id_user_content` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

--
-- Ketidakleluasaan untuk tabel `detail_pemesanan`
--
ALTER TABLE `detail_pemesanan`
  ADD CONSTRAINT `fk_harga` FOREIGN KEY (`id_harga`) REFERENCES `harga` (`id_harga`),
  ADD CONSTRAINT `fk_header_pemesanan` FOREIGN KEY (`id_header_pemesanan`) REFERENCES `header_pemesanan` (`id_header_pemesanan`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_id_paket` FOREIGN KEY (`id_paket`) REFERENCES `paket` (`id_paket`),
  ADD CONSTRAINT `fk_jadwal` FOREIGN KEY (`id_jadwal`) REFERENCES `jadwal` (`id_jadwal`);

--
-- Ketidakleluasaan untuk tabel `equipment`
--
ALTER TABLE `equipment`
  ADD CONSTRAINT `fk_user_equipment` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

--
-- Ketidakleluasaan untuk tabel `harga`
--
ALTER TABLE `harga`
  ADD CONSTRAINT `fk_paket_harga` FOREIGN KEY (`id_paket`) REFERENCES `paket` (`id_paket`);

--
-- Ketidakleluasaan untuk tabel `header_pemesanan`
--
ALTER TABLE `header_pemesanan`
  ADD CONSTRAINT `fk_pelanggan_header` FOREIGN KEY (`nik`) REFERENCES `pelanggan` (`nik`);

--
-- Ketidakleluasaan untuk tabel `jadwal`
--
ALTER TABLE `jadwal`
  ADD CONSTRAINT `jadwal_ibfk_1` FOREIGN KEY (`id_paket`) REFERENCES `paket` (`id_paket`);

--
-- Ketidakleluasaan untuk tabel `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD CONSTRAINT `fk_agen` FOREIGN KEY (`id_agen`) REFERENCES `agen` (`id_agen`),
  ADD CONSTRAINT `fk_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

--
-- Ketidakleluasaan untuk tabel `transaksi_barang`
--
ALTER TABLE `transaksi_barang`
  ADD CONSTRAINT `fk_id_barang` FOREIGN KEY (`id_barang`) REFERENCES `equipment` (`id_barang`);

--
-- Ketidakleluasaan untuk tabel `visa`
--
ALTER TABLE `visa`
  ADD CONSTRAINT `fk_header_pemesanan_visa` FOREIGN KEY (`id_header_pemesanan`) REFERENCES `header_pemesanan` (`id_header_pemesanan`),
  ADD CONSTRAINT `fk_nik` FOREIGN KEY (`nik`) REFERENCES `pelanggan` (`nik`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
