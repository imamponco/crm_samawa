<?php
/**
 * 
 */
class User_models extends CI_Model
{
	
	public function get($email)
	{
		$query = $this->db->get_where('user',array("email"=>$email));

		return $query->row();
	}

	public function insert_user($data)
	{
		$query = $this->db->insert('user',$data);
		$read = $this->db->get_where('user',array('email'=>$data["email"]));
		return $read->row();
	}


	public function email_exists($email)
   	{
	    $this->db->where('email', $email);
	    $query = $this->db->get('user');
	    $this->db->last_query();
	    if( $query->num_rows() > 0 )
	     { 
	       return TRUE; 
	     } 
	    else 
	     { 
	       return FALSE; 
	     }
 	}

 	function getTable($namatabel){
		$query = $this->db->get($namatabel);
		return $query->result();

	}
 }
?>