<?php
/**
 * 
 */
class Global_models extends CI_Model
{
	
	function getTable($namatabel){
		$query = $this->db->get($namatabel);
		return $query->result();

	}

	public function get_id_user($username,$password)
	{
		 
		$query = $this->db->get_where('user',array('username' => $username));
		return $query->row();
	}

	public function insert_data($field,$data)
	{
		$query = $this->db->insert($field,$data);
		$read = $this->db->get_where($field,$data);
		return $read->row();
	}

	function get_paket_by_id($id){
		$id_user = $this->session->userdata('id_user_pelanggan');

		$this->db->select('*');
		$this->db->from('paket');
		$this->db->join('harga', 'paket.id_paket = harga.id_paket','left');
		$this->db->join('jadwal', 'paket.id_paket = jadwal.id_paket','left');
		$this->db->where('paket.id_paket',$id);
		$this->db->where('harga.deleted_at',null);

		$value = $this->db->get()->result();

		$this->db->select('*');
		$this->db->from('user');
		$this->db->join('pelanggan', 'user.id_user = pelanggan.id_user','left');
		$this->db->where('user.id_user',$id_user);
		$value2 = $this->db->get()->result();

		$data[0] = $value;
		$data[1] = $value2;

		return $data;
	}

	function get_kuota($data){
	   $this->db->select('kuota,tgl_keberangkatan,id_jadwal');
	   $this->db->from('jadwal');
	   $this->db->where('id_jadwal',$data[0]);
	   $query = $this->db->get();
	   $kuota = $query->row();

	   $this->db->select('SUM(pax) as jumlah');
	   $this->db->from('detail_pemesanan');
	   $this->db->join('paket', 'detail_pemesanan.id_paket = paket.id_paket');
	   $this->db->where('detail_pemesanan.id_jadwal = "'.$data[0].'" AND detail_pemesanan.id_paket="'.$data[1].'"');
	   $isi = $this->db->get()->row();

	   $tersedia = $kuota->kuota - $isi->jumlah;

	   $result[0] = $tersedia;
	   $result[1] = $kuota->kuota;

	   return $result;
	}

	function getpaket(){
		$query = $this->db->get('paket');

		if($query->num_rows() == 1){
			$obj = $query->row();
			$data['id_paket'] = $obj->id_paket;
			$data['paket'] = $obj->nama_paket;
			$data['harga'] = $obj->harga;
 			return $data;
		}
	}

	function getharga($idpaket)
	{
	    $query = $this->db
	        ->where('id_paket',$idpaket)
	        ->get('paket');

	    // $query2 = $this->db
	    //     ->where('id_paket',$idpaket)
	    //     ->get('jadwal');

	    // $this->db->select("*");
     //    $this->db->from('jadwal');
     //    $this->db->join('paket', 'jadwal.id_paket = paket.id_paket','left');
     //    $this->db->where('jadwal.id_paket = "'.$idpaket.'"');
     //    $query = $this->db->get();


	    if ($query->num_rows() == 1)
	    {
	        $obj = $query->row();
	        $data['harga']	= $obj->harga;
	        $data['paket']	= $obj->nama_paket;
	        return $data;
	    }
	}

	function getjadwal($idpaket){
		$query = $this->db
	        ->where('id_paket',$idpaket)
	        ->get('jadwal');

	    // $this->db->select("*");
     //    $this->db->from('jadwal');
     //    $this->db->join('paket', 'jadwal.id_paket = paket.id_paket','left');
     //    $this->db->where('jadwal.id_paket = "'.$idpaket.'"');
     //    $query = $this->db->get();

	    if ($query->num_rows() > 0)
	    {
	        $obj = $query->result();
	        return $obj;
	    }
	}

	 //show all images
    function show_images(){
        $query = $this->db->get('images');
        return $query;
    }




}
?>