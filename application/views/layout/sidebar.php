<?php
$menu_back = $this->session->userdata('menu_back');
?>
<input type="hidden" name="menu_back" id="menu_back" value="<?=$menu_back?>">
<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="<?=base_url();?>assets/admin/dist/img/logo-gaido.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light" style="font-size: 18px;">E-CRM Gaido</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <!-- <div class="image">
          <img src="<?=base_url();?>assets/admin/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div> -->
        <div class="info">
          <div class="nav-item has-treeview ">
          <h3 class="m-0 text-white"><?=$this->session->userdata('nama');?></h3>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
            <a id="menu_home" href="<?=base_url();?>dashboard" class="nav-link active">
              <i class="nav-icon fa fa-dashboard"></i>
              <p>
                Dashboard
              </p>
            </a>
          
              

          
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-th"></i>
              <p>
                Menu
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
          <ul class="nav nav-treeview">

        <?php
          switch ($this->session->userdata('permission')) {
            case 'AO':
            ?>
            <!-- Admin Operasional -->
             <li class="nav-item has-treeview menu-open">
                <a href="#" class="nav-link">
                  <i class="nav-icon fa fa-table"></i>
                  <p>
                    Operasional
                    <i class="fa fa-angle-left right"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a id="menu_user" href="<?=base_url();?>User/list" class="nav-link">
                      <i class="fa fa-circle-o nav-icon"></i>
                      <p>User</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a id="menu_paket" href="<?=base_url();?>Paket/list" class="nav-link">
                      <i class="fa fa-circle-o nav-icon"></i>
                      <p>Paket</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a id="menu_agen" href="<?=base_url();?>Agen/list" class="nav-link">
                      <i class="fa fa-circle-o nav-icon"></i>
                      <p>Agen</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a id="menu_jadwal" href="<?=base_url();?>Jadwal/list" class="nav-link">
                      <i class="fa fa-circle-o nav-icon"></i>
                      <p>Jadwal</p>
                    </a>
                  </li>
                   <li class="nav-item">
                    <a id="menu_equipment" href="<?=base_url();?>Equipment/list" class="nav-link">
                      <i class="fa fa-circle-o nav-icon"></i>
                      <p>Equipment</p>
                    </a>
                  </li>
                  
                  
                </ul>
              </li>
        <?php
            break;
        ?>

        <?php
            case 'AF':
        ?>
          <!-- Admin Finance -->
          <li class="nav-item has-treeview menu-open">
              <a href="#" class="nav-link">
                <i class="nav-icon fa fa-edit"></i>
                <p>
                  Finance
                  <i class="fa fa-angle-left right"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a id="menu_pemesanan" href="<?=base_url();?>Pemesanan/list" class="nav-link">
                    <i class="nav-icon fa fa-circle-o text-info"></i>
                    <p>Data Transaksi</p>
                  </a>
                </li>
              </ul>
            </li>

        <?php
            break;
        ?>


        <?php
            case 'AM':
        ?>
        <!-- Admin Marketing -->
         <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-envelope-o"></i>
              <p>
                Marketing
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                  <a id="menu_customer" href="<?=base_url();?>Customer/list" class="nav-link">
                    <i class="fa fa-circle-o nav-icon"></i>
                    <p>Pelanggan</p>
                  </a>
              </li>
              <li class="nav-item">
                <a id="menu_kritik" href="<?=base_url();?>Kritik/list" class="nav-link">
                  <i class="nav-icon fa fa-circle-o text-danger"></i>
                  <p>Kritik dan Saran</p>
                </a>
              </li>
              <li class="nav-item">
                <a id="menu_konten" href="<?=base_url();?>Konten/list" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Konten Penawaran</p>
                </a>
              </li>
            </ul>
          </li>
        <?php  
            break;
        ?>


        <?php
            case 'AV':
        ?>
          <!-- Admin Visa -->
          <li class="nav-item has-treeview menu-open">
              <a href="#" class="nav-link">
                <i class="nav-icon fa fa-book"></i>
                <p>
                  Visa
                  <i class="fa fa-angle-left right"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a id="menu_visa" href="<?=base_url();?>Visa/list" class="nav-link">
                    <i class="fa fa-circle-o nav-icon"></i>
                    <p>Visa Pelanggan</p>
                  </a>
                </li>
              </ul>
            </li>
        <?php
            break;
        ?>

        <?php
            case 'PD':
        ?>
        <!-- President Director -->
        <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-pie-chart"></i>
              <p>
                Laporan
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a id="menu_keuangan" href="<?=base_url();?>Laporan/laporan_keuangan" class="nav-link">
                  <i class="nav-icon fa fa-circle-o text-info"></i>
                  <p>Laporan Keuangan</p>
                </a>
              </li>
              <li class="nav-item">
                <a id="menu_lap_pelanggan" href="<?=base_url();?>Laporan/laporan_pelanggan" class="nav-link">
                  <i class="nav-icon fa fa-circle-o text-info"></i>
                  <p>Laporan Pelanggan</p>
                </a>
              </li>
              <li class="nav-item">
                <a id="menu_lap_keberangkatan" href="<?=base_url();?>Laporan/laporan_keberangkatan" class="nav-link">
                  <i class="nav-icon fa fa-circle-o text-info"></i>
                  <p>Laporan Keberangkatan</p>
                </a>
              </li>
            </ul>
          </li>
        <?php 
            break;
        ?>


        <?php 
            default:
        ?>
        <!-- Default -->

        <?php 
            break;
          }
        ?>
        
        </li>
        </ul>
        <li class="nav-header">
        <form action="" method="post">
        <button type="submit" class="btn btn-block btn-danger" formaction="<?=base_url();?>login/logout">Logout</button>
        </form>
        </li>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>