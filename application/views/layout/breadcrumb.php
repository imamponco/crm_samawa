<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <?php
  $tittle = $this->uri->segment(1);
  ?>
  <title>Gaido Monitoring | <?=$tittle?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url();?>assets/admin/plugins/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url();?>assets/admin/dist/css/adminlte.css">
  <!-- Datatables -->
  <link rel="stylesheet" href="<?=base_url();?>assets/admin/plugins/datatables/dataTables.bootstrap4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?=base_url();?>assets/admin/plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?=base_url();?>assets/admin/plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?=base_url();?>assets/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?=base_url();?>assets/admin/plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?=base_url();?>assets/admin/plugins/daterangepicker/daterangepicker-bs3.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?=base_url();?>assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- Favicon -->
  <link rel="shortcut icon" href="<?=base_url();?>assets/admin/dist/img/icon.png">

  <!-- jQuery -->
  <script src="<?=base_url();?>assets/jquery-3.4.1.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
  


</head>

<!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?=base_url();?>/dashboard" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?=base_url();?>user/edit_user/<?=$this->session->userdata('id_user')?>" class="nav-link">Profile</a>
      </li>
      <!-- <li class="nav-item d-none d-sm-inline-block">
        <a href="https://Gaidotour.co.id/kontak-Gaido" class="nav-link">Contact</a>
      </li> -->
    </ul>

    <!-- SEARCH FORM -->
    <!-- <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fa fa-search"></i>
          </button>
        </div>
      </div>
    </form> -->

    <!-- Right navbar linkss -->
    <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->
    <!--   <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="fa fa-comments-o"></i>
          <span class="badge badge-danger navbar-badge">3</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <a href="#" class="dropdown-item"> -->
            <!-- Message Start -->
            <!-- <div class="media">
              <img src="<?=base_url();?>assets/admin/dist/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  Brad Diesel
                  <span class="float-right text-sm text-danger"><i class="fa fa-star"></i></span>
                </h3>
                <p class="text-sm">Call me whenever you can...</p>
                <p class="text-sm text-muted"><i class="fa fa-clock-o mr-1"></i> 4 Hours Ago</p>
              </div>
            </div> -->
            <!-- Message End -->
          <!-- /a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item"> -->
            <!-- Message Start -->
            <!-- <div class="media">
              <img src="<?=base_url();?>assets/admin/dist/img/user8-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  John Pierce
                  <span class="float-right text-sm text-muted"><i class="fa fa-star"></i></span>
                </h3>
                <p class="text-sm">I got your message bro</p>
                <p class="text-sm text-muted"><i class="fa fa-clock-o mr-1"></i> 4 Hours Ago</p>
              </div>
            </div> -->
           <!--  Message End -->
          <!-- </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item"> -->
            <!-- Message Start -->
            <!-- <div class="media">
              <img src="<?=base_url();?>assets/admin/dist/img/user3-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  Nora Silvester
                  <span class="float-right text-sm text-warning"><i class="fa fa-star"></i></span>
                </h3>
                <p class="text-sm">The subject goes here</p>
                <p class="text-sm text-muted"><i class="fa fa-clock-o mr-1"></i> 4 Hours Ago</p>
              </div>
            </div> -->
            <!-- Message End -->
         <!--  </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
        </div>
      </li> -->

       <!-- MODAL -->
  <div id="myModal" class="modal col-5">
    <!-- Modal content -->
    <div class="wrap">
      <div class="modal-content">
        <div class="modal-header">
          <span class="close">&times;</span>
            <h2>Forgot Your Password?</h2>
        </div>
        <div class="modal-body">
          <form action="<?=base_url()?>login/forgot_password" method="post">
            <table>
              <tr>
                <div class="form-group">
                <td><label class="enter">Enter your email</label></td>
                <td><span id="titik">:</span></td>
                <td><input  class="form-control" id="inputemail" type="email" required="required" name="email"></td>
                <td><span id="button"></span></td>
                <td><button type="submit" class="btn btn-primary btn-block">Send!</button></td>
                </div>
              </tr>
              <tr>
                <td><p class="enter">Kami akan mengirimkan password baru ke email anda.</p></label></td>
              </tr>
            </table>
          </form>

        </div>
        <div class="modal-footer">
        </div>

      </div> 
    </div>
  </div>
      
      <?php $id_user =  $this->session->userdata('id_user'); ?>


      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#" id="notifikasi">
          <i class='fa fa-bell-o'></i>
          <span id="number_notification">
          </span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" style="overflow-y:auto;" id="konten_notifikasi">
          <span class="dropdown-item dropdown-header">Notifications</span>
          <!-- <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fa fa-envelope mr-2"></i> 4 new messages
            <span class="float-right text-muted text-sm">3 mins</span>
          </a> -->
          <div class="dropdown-divider"></div>
           <a href="<?=base_url()?>User/list" class="dropdown-item">
              <i class="fa fa-users mr-2"></i> 3 new users
            </a>
            <!--  <span class="float-right text-muted text-sm">12 hours</span> -->
           
          
         <!--  <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fa fa-file mr-2"></i> 3 new reports
            <span class="float-right text-muted text-sm">2 days</span>
          </a> -->
          <span class="dropdown-item dropdown-footer"></span>
          <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item dropdown-footer" onclick="ok()">Clear All Notifications</a>
        </div>
      </li>
      <!-- <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"><i
            class="fa fa-th-large"></i></a>
      </li> -->
    </ul>
  </nav>
  <!-- /.navbar -->

  <script type="text/javascript">

    $(document).ready(function() {
      ok();
    });

    function ok(){
      var data = '<?=$id_user?>';
      data = JSON.stringify(data);
         $.ajax({
          url  : "<?php echo base_url('Ajax/get_number_notification'); ?>",
          data : "data="+data,
          type : "POST",
          success: function(data) 
          { 
            $('#number_notification').html(data);
          }
        });
    }

     $('#notifikasi').click(function (e) {
      e.preventDefault();
      var data = '<?=$id_user?>';
      data = JSON.stringify(data);
      console.log(data)
      $.ajax({
          url  : "<?php echo base_url('Ajax/get_notification'); ?>",
          data : "data="+data,
          type : "POST",
          success: function(data) 
          { 
            ok();
            $('#konten_notifikasi').html(data);
          }
        });
    });

    function delete_notification(){
      var data = '<?=$id_user?>';
      data = JSON.stringify(data);
      $.ajax({
          url  : "<?php echo base_url('Ajax/clear_notification'); ?>",
          data : "data="+data,
          type : "POST",
          success: function(data) 
          { 
            console.log(data);
          }
        });
    };

  </script>