<html>
    <head>
        <title>Belajaphp.net - Codeigniter Datatable</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <h3>DATA KARYAWAN</h3>
            <table id="table" class="table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                    <tr>
                    <th>Nama Lengkap</th>
                    <th>Jenis Kelamin</th>
                    <th>Tanggal Lahir</th>
                    <th>Tempat Lahir</th>
                    <th>No Passport</th>
                    <th>Issuing Office</th>
                    <th>Date of Issue</th>
                    <th>Date of Expiry</th>
                    <th>Relationship</th>
                    <th>No Telp</th>
                    <th>Alamat</th>
                    <th>NIK</th>
                    <th>Room Type</th>
                    <th>Agen</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript">
            var save_method; //for save method string
            var table;
            var no = 1;
 
            $(document).ready(function() {
                //datatables
                $.noConflict();
                table = $('#table').DataTable({ 
                    "processing": true, //Feature control the processing indicator.
                    "serverSide": true, //Feature control DataTables' server-side processing mode.
                    "order": [], //Initial no order.
                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        "url": '<?php echo site_url('Customer/json'); ?>',
                        "type": "POST"
                    },
                    //Set column definition initialisation properties.
                    "columns": [
                        {"columns": "id_pelanggan",width:100},
                        {"columns": "nama",width:170},
                        {"columns": "jenis_kelamin",width:100},
                        {"columns": "tanggal_lahir",width:100},
                        {"columns": "tempat_lahir",width:100},
                        {"columns": "no_passport",width:100},
                        {"columns": "issuing_office",width:100},
                        {"columns": "date_of_issue",width:100},
                        {"columns": "date_of_expiry",width:100},
                        {"columns": "relationship",width:100},
                        {"columns": "no_telp",width:100},
                        {"columns": "alamat",width:100},
                        {"columns": "nik",width:100},
                        {"columns": "room_type",width:100},
                        {"columns": "nama_agen",width:100}
                    ],
 
                });
 
            });
        </script>
 
 
    </body>
</html>

