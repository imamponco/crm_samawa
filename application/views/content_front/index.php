

<!-- Modal Register -->
  <!-- <div class="modal fade" id="modalPelanggan" role="dialog">
    <div class="modal-dialog"> -->
    
      <!-- Modal content-->
      <!-- <div class="modal-content">
        <div class="modal-header" style="padding:35px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="glyphicon glyphicon-user"></span> Register</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
          <form role="form" action="/register/do_register_front">
            <div class="form-group">
              <label for="username"><span class="glyphicon glyphicon-user"></span> Nama Lengkap</label>
              <input type="text" class="form-control" id="username" placeholder="Masukan nama lengkap anda...">
            </div>
            <div class="form-group">
              <label for="email"><span class="glyphicon glyphicon-envelope"></span> Email</label>
              <input type="text" class="form-control" id="email" placeholder="Masukan alamat email...">
            </div>
            <div class="form-group">
              <label for="psw"><span class="glyphicon glyphicon-lock"></span> Password</label>
              <input type="text" class="form-control" id="psw" placeholder="Enter password">
            </div>
             <div class="form-group">
              <label for="repassword"><span class="glyphicon glyphicon-lock"></span> Re-Password</label>
              <input type="text" class="form-control" id="repassword" placeholder="Enter Re-password">
            </div>
            <input type="hidden" name="role" value="CO">
              <button type="submit" class="btn btn-success btn-block"><span class="glyphicon glyphicon-arrow-right"></span> Next</button>
          </form>
        </div>
        <div class="modal-footer"> -->
          <!-- <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button> -->
          <!-- <p class="pull-left">Have an account? Login <a href="#" id='login'>Here</a></p> -->
          <!-- <p>Forgot <a href="#">Password?</a></p> -->
       <!--  </div>
      </div> -->

 <!--    </div>
  </div> -->

   <!-- end:fh5co-header -->
  <aside id="fh5co-hero" class="js-fullheight">
    <div class="flexslider js-fullheight">
      <ul class="slides">
        <li style="background-image: url(<?=base_url();?>assets/admin/dist/img/slider-1-gaido.jpg);">
          <div class="overlay-gradient"></div>
          <div class="container">
            <div class="col-md-12 col-md-offset-0 text-center slider-text">
              <div class="slider-text-inner js-fullheight">
                <div class="desc">
                  <p><span>Gaido Travel And Tour</span></p>
                  <!-- <h2>Bayar sekali umroh 5 kali!</h2> -->
                 <!--  <p>
                    <a href="#" class="btn btn-primary btn-lg">Book Now</a>
                  </p> -->
                </div>
              </div>
            </div>
          </div>
        </li>
        <li style="background-image: url(<?=base_url();?>assets/admin/dist/img/slider-2-gaido.jpg);">
          <div class="overlay-gradient"></div>
          <div class="container">
            <div class="col-md-12 col-md-offset-0 text-center slider-text">
              <div class="slider-text-inner js-fullheight">
                <div class="desc">
                  <p><span>Gaido Travel And Tour</span></p>
                 <!--  <h2>Make Your Vacation Comfortable</h2>
                  <p>
                    <a href="#" class="btn btn-primary btn-lg">Book Now</a>
                  </p> -->
                </div>
              </div>
            </div>
          </div>
        </li>
        <li style="background-image: url(<?=base_url();?>assets/admin/dist/img/slider-3-gaido.jpg);">
          <div class="overlay-gradient"></div>
          <div class="container">
            <div class="col-md-12 col-md-offset-0 text-center slider-text">
              <div class="slider-text-inner js-fullheight">
                <div class="desc">
                  <p><span>Gaido Travel And Tour</span></p>
                  <!-- <h2>A Best Place To Enjoy Your Life</h2>
                  <p>
                    <a href="#" class="btn btn-primary btn-lg">Book Now</a>
                  </p> -->
                </div>
              </div>
            </div>
          </div>
        </li>
        
        </ul>
      </div>
  </aside>
  <div class="wrap">
    <div class="container">
      <div class="row">
        <div id="availability">
         <!--  <form action="#">
            <div class="a-col" style="width: 40%;">
              <section>
               <select class="cs-select cs-skin-border">
                  <option value="" disabled selected>Paket</option>
                 <?php foreach($paket as $row):?>
                  <option value="<?=$row->id_paket?>"><?=$row->nama_paket?></option>
                <?php endforeach;?>
                </select>
              </section>
            </div> -->
            <!-- ss -->
          <!-- <div class="a-col alternate" style="width: 40%;">
              <section>
                <select class="cs-select cs-skin-border">
                  <option value="" disabled selected>Tanggal Keberangkatan</option>
                  <option value="date">28/12/2019</option>
                </select>
              </section>
            </div>
            <div class="a-col action">
              <a href="#">
                <span>Check</span>
                Availability
              </a>
            </div>
          </form> -->
        </div>
      </div>
    </div>
  </div>

  <?php?>
  
  <div id="fh5co-counter-section" class="fh5co-counters" style="margin-top: 15%;">
    <div class="container">
      <div class="row">
        <div class="col-md-3 text-center">
          <span class="fh5co-counter js-counter" data-from="0" data-to="<?=($jml_user->jml_user ? $jml_user->jml_user : 0)?>" data-speed="5000" data-refresh-interval="50"></span>
          <span class="fh5co-counter-label">Pengguna Kami</span>
        </div>
        <div class="col-md-3 text-center">
          <span class="fh5co-counter js-counter" data-from="0" data-to="<?=($jml_paket->jml_paket ? $jml_paket->jml_paket : 0)?>" data-speed="5000" data-refresh-interval="50"></span>
          <span class="fh5co-counter-label">Paket</span>
        </div>
        <div class="col-md-3 text-center">
          <span class="fh5co-counter js-counter" data-from="0" data-to="<?=($jml_transaksi->jml_transaksi ? $jml_transaksi->jml_transaksi : 0)?>" data-speed="5000" data-refresh-interval="50"></span>
          <span class="fh5co-counter-label">Transaksi</span>
        </div>
        <div class="col-md-3 text-center">
          <span class="fh5co-counter js-counter" data-from="0" data-to="<?=($jml_review->jml_review ? $jml_review->jml_review  : 0)?>" data-speed="5000" data-refresh-interval="50"></span>
          <span class="fh5co-counter-label">Penilaian &amp; Ulasan</span>
        </div>
      </div>
    </div>
  </div>

  <div id="featured-hotel" class="fh5co-bg-color">
    <div class="container">
      
      <div class="row">
        <div class="col-md-12">
          <div class="section-title text-center">
            <h2>Paket Terbaru</h2>
          </div>
        </div>
      </div>

      <div class="row">
         <?php 
          $no = 1;
          foreach($paket as $row):
          if($no == 1){
        ?>
        <div class="feature-full-1col">
          <div class="image" style="background-image: url(<?=base_url();?>assets/images/<?=$row->gambar_paket?>);">
            <div class="descrip text-center">
                <p><small>Harga</small><span>Rp.<?=$row->jumlah?></span></p>
            </div>
          </div>
          <div class="desc">
            <input type="hidden" name="hidden" value="<?=$row->id_paket?>" id="paket-<?=$no?>">
            <h2><?=$row->nama_paket?></h2>
            <h3><?=$row->durasi?> Days</h3>
            <p><?=$row->keterangan?></p>
            <p><a href="#" class="btn btn-primary btn-luxe-primary" id="book-<?=$no?>" onclick="book(this.id)">Book Now <i class="ti-angle-right"></i></a></p>
          </div>
        </div>
        <div class="feature-full-2col">
        <?php
        }else{
        ?>
       
          <div class="f-hotel">
            <div class="image" style="background-image: url(<?=base_url();?>assets/images/<?=$row->gambar_paket?>);">
              <div class="descrip text-center">
                <p><small>Harga</small><span>Rp.<?=$row->jumlah?></span></p>
              </div>
            </div>
            <div class="desc">
              <input type="hidden" name="hidden" value="<?=$row->id_paket?>" id="paket-<?=$no?>">
              <h2><?=$row->nama_paket?></h2>
              <h3><?=$row->durasi?> Days</h3>
              <p><?=$row->keterangan?></p>
              <p><a href="#" class="btn btn-primary btn-luxe-primary" id="book-<?=$no?>" onclick="book(this.id)">Book Now <i class="ti-angle-right"></i></a></p>
            </div>
          </div>
          
        <?php
          }

          $no++;
          endforeach;
        ?>
       </div>

    </div>
  </div>

  <div id="hotel-facilities">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="section-title text-center">
            <h2>Layanan Kami</h2>
          </div>
        </div>
      </div>
      
      <div id="tabs">
        <nav class="tabs-nav">
          <a href="#" class="active" data-tab="tab1" style="margin-left: 20rem;">
            <i class="flaticon icon"></i>
            <span>Resmi</span>
          </a>
          <a href="#" data-tab="tab2">
            <i class="flaticon icon"></i>
            <span>Komitmen</span>
          </a>
          <a href="#" data-tab="tab3">
          
            <i class="flaticon icon"></i>
            <span>Pembimbing Professional</span>
          </a>
          <a href="#" data-tab="tab4">    
            <i class="flaticon icon"></i>
            <span>Fasilitas Berkualitas</span>
          </a>
        </nav>
        <div class="tab-content-container">
          <div class="tab-content active show" data-tab-content="tab1">
            <div class="container">
              <div class="row">
                <div class="col-md-6">
                  <img src="<?=base_url();?>assets/admin/dist/img/resmi.jpg" class="img-responsive" alt="Image">
                </div>
                <div class="col-md-6">
                  <span class="super-heading-sm">Gaido Travel And Tour</span>
                  <h3 class="heading">Resmi</h3>
                  <p>Gaido Travel And Tour telah resmi terdaftar di Kementerian Agama Republik Indonesia, smemiliki izin Umroh dan Haji serta sudah tersertifikasi, sehingga terjamin aman dan terpercaya untuk berkerjasama dengan kami.</p>
                 <!--  <p class="service-hour">
                    <span>Service Hours</span>
                    <strong>7:30 AM - 8:00 PM</strong>
                  </p> -->
                </div>
              </div>
            </div>
          </div>
          <div class="tab-content" data-tab-content="tab2">
            <div class="container">
              <div class="row">
                <div class="col-md-6">
                  <img src="<?=base_url();?>assets/admin/dist/img/komitmen.png" class="img-responsive" alt="Image">
                </div>
                <div class="col-md-6">
                  <span class="super-heading-sm">Gaido Travel And Tour</span>
                  <h3 class="heading">Komitmen</h3>
                  <p>Sesuai dengan visi dan misi kami, Gaido Travel And Tour berkomitmen Menjaga Kualitas Layanan dan Kemuliaan beribadah.</p>
                 <!--  <p class="service-hour">
                    <span>Service Hours</span>
                    <strong>7:30 AM - 8:00 PM</strong>
                  </p> -->
                </div>
              </div>
            </div>
          </div>
          <div class="tab-content" data-tab-content="tab3">
            <div class="container">
              <div class="row">
                <div class="col-md-6">
                  <img src="<?=base_url();?>assets/admin/dist/img/pembimbing.png" class="img-responsive" alt="Image">
                </div>
                <div class="col-md-6">
                  <span class="super-heading-sm">Gaido Travel And Tour</span>
                  <h3 class="heading">Pembimbing Ibadah</h3>
                  <p>Jangan khawatir akan perjalanan umroh dan haji anda karena selama perjalanan yang akan kami sediakan kamu akan didampingi oleh pembimbing berkompeten dari Jakarta dan Muthawif yang pengalaman.</p>
                 <!--  <p class="service-hour">
                    <span>Service Hours</span>
                    <strong>7:30 AM - 8:00 PM</strong>
                  </p> -->
                </div>
              </div>
            </div>
          </div>
          <div class="tab-content" data-tab-content="tab4">
            <div class="container">
              <div class="row">
                <div class="col-md-6">
                  <img src="<?=base_url();?>assets/admin/dist/img/fasilitas.png" class="img-responsive" alt="Image">
                </div>
                <div class="col-md-6">
                  <span class="super-heading-sm">Gaido Travel And Tour</span>
                  <h3 class="heading">Fasilitas Berkualitas</h3>
                  <p>Tidak hanya kualitasn alayanan dan kemuliaan beribadah Gaido Travel And Tour juga menyediakan fasilitas hotel terbaik yang dekat dengan Masjidil Haram & Masjid Nabawi, juga penerbangan yang telah berstandar IATA.</p>
                 <!--  <p class="service-hour">
                    <span>Service Hours</span>
                    <strong>7:30 AM - 8:00 PM</strong>
                  </p> -->
                </div>
              </div>
            </div>
          </div>
         <!--  <div class="tab-content" data-tab-content="tab5">
            <div class="container">
              <div class="row">
                <div class="col-md-6">
                  <img src="<?=base_url();?>assets/admin/dist/img/tab_img_5.jpg" class="img-responsive" alt="Image">
                </div>
                <div class="col-md-6">
                  <span class="super-heading-sm">World Class</span>
                  <h3 class="heading">Spa</h3>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias officia perferendis modi impedit, rem quasi veritatis. Consectetur obcaecati incidunt, quae rerum, accusamus sapiente fuga vero at. Quia, labore, reprehenderit illum dolorem quae facilis reiciendis quas similique totam sequi ducimus temporibus ex nemo, omnis perferendis earum fugit impedit molestias animi vitae.</p>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam neque blanditiis eveniet nesciunt, beatae similique doloribus, ex impedit rem officiis placeat dignissimos molestias temporibus, in! Minima quod, consequatur neque aliquam.</p>
                  <p class="service-hour">
                    <span>Service Hours</span>
                    <strong>7:30 AM - 8:00 PM</strong>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-content" data-tab-content="tab6">
            <div class="container">
              <div class="row">
                <div class="col-md-6">
                  <img src="<?=base_url();?>assets/admin/dist/img/tab_img_6.jpg" class="img-responsive" alt="Image">
                </div>
                <div class="col-md-6">
                  <span class="super-heading-sm">World Class</span>
                  <h3 class="heading">Gym</h3>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias officia perferendis modi impedit, rem quasi veritatis. Consectetur obcaecati incidunt, quae rerum, accusamus sapiente fuga vero at. Quia, labore, reprehenderit illum dolorem quae facilis reiciendis quas similique totam sequi ducimus temporibus ex nemo, omnis perferendis earum fugit impedit molestias animi vitae.</p>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam neque blanditiis eveniet nesciunt, beatae similique doloribus, ex impedit rem officiis placeat dignissimos molestias temporibus, in! Minima quod, consequatur neque aliquam.</p>
                  <p class="service-hour">
                    <span>Service Hours</span>
                    <strong>7:30 AM - 8:00 PM</strong>
                  </p>
                </div>
              </div>
            </div>
          </div> -->
        </div>
      </div>
    </div>
  </div>

  <div id="testimonial">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="section-title text-center">
            <h2>Happy Customer Says...</h2>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="testimony">
            <blockquote>
              &ldquo;Kualitas umroh dengan kualitas layanan terbaik emang ga salah kalo pilih Gaido Travel And Tour sebagai biroperjalanan kamu! &rdquo;
            </blockquote>
            <p class="author"><cite>Muhammad Sultan Rafi</cite></p>
          </div>
        </div>
        <div class="col-md-4">
          <div class="testimony">
            <blockquote>
              &ldquo;Staff dan layanannya bagus banget, rasanya pengen umroh lagi bareng Gaido Travel And Tour!!!&rdquo;
            </blockquote>
            <p class="author"><cite>Shana Nandya S.</cite></p>
          </div>
        </div>
        <div class="col-md-4">
          <div class="testimony">
            <blockquote>
              &ldquo;Alhamdulillah bisa umroh dengan Gaido tour, pembimbing,staff dan layanannya bagus banget, sepanjang perjalanan dapet pengalaman yang tak terlupakan.&rdquo;
            </blockquote>
            <p class="author"><cite>Akbar Riski</cite></p>
          </div>
        </div>
      </div>
    </div>
  </div>


  <!-- <div id="fh5co-blog-section">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="section-title text-center">
            <h2>Our Blog</h2>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="blog-grid" style="background-image: url(<?=base_url();?>assets/admin/dist/img/image-1.jpg);">
            <div class="date text-center">
              <span>09</span>
              <small>Aug</small>
            </div>
          </div>
          <div class="desc">
            <h3><a href="#">Most Expensive Hotel</a></h3>
          </div>
        </div>
        <div class="col-md-4">
          <div class="blog-grid" style="background-image: url(<?=base_url();?>assets/admin/dist/img/image-2.jpg);">
            <div class="date text-center">
              <span>09</span>
              <small>Aug</small>
            </div>
          </div>
          <div class="desc">
            <h3><a href="#">1st Anniversary of Luxe Hotel</a></h3>
          </div>
        </div>
        <div class="col-md-4">
          <div class="blog-grid" style="background-image: url(<?=base_url();?>assets/admin/dist/img/image-3.jpg);">
            <div class="date text-center">
              <span>09</span>
              <small>Aug</small>
            </div>
          </div>
          <div class="desc">
            <h3><a href="#">Discover New Adventure</a></h3>
          </div>
        </div>
      </div>
    </div>
  </div> -->

