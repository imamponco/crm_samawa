<!-- Main content -->
<div id="featured-hotel" class="fh5co-bg-color">
  <h3 class="section-tittle text-center"></h3>
    <section class="container">
       <?php if($this->session->flashdata('edit')):?>
        <div class="col-3">
          <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fa fa-check"></i> Success!</h5>
                <?php echo $this->session->flashdata('edit'); ?>
          </div>
        </div>
      <?php endif; ?>
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title text-center">KRITIK DAN SARAN</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div id="example1_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">

              <!-- <div class="row">
                <div class="col-sm-12 col-md-2">
                  <label>
                     <a href="<?=base_url();?>jadwal/add_jadwal">
                        <button type="button" class="btn btn-block btn-success">Pesan Paket</button>
                     </a>
                   </label>
                </div>
              </div> -->
              </div>
    
      <div class="container-fluid">
          <div class="row">
            <!-- left column -->
              <div class="col-md-12">
                <!-- general form elements -->
                  <div class="card card-primary">
                    <div class="card-header">
                      <h3 class="card-title">KRITIK DAN SARAN</h3>
                        </div>
                        <!-- /.card-header -->
                          <!-- form start -->
                            <div class="card-body">
                              <table class="table table-bordered table-striped">
                                <thead>
                                  <tr>
                                    <center>
                                      <th>No</th>
                                      <th>Nama</th>
                                      <th>Pesan</th>
                                      <th>Tanggal Balasan</th>
                                      <th>Action</th>
                                    </center>
                                  </tr>
                                </thead>
                                 <tbody>
                                 <?php
                                  $id_user = $this->session->userdata('id_user_pelanggan'); 
                                  $no = 1;
                                  foreach ($kritik as $row) :
                                  ?>
                                  <tr>
                                    <td><?=$no?></td>
                                    <td><?=$row->username ?></td>
                                    <td><?=$row->message?></td>
                                    <td><?=$row->created_ct_at?></td>
                                    <td>
                                    <?php if($row->id_user==$id_user):?>
                                    <a href="<?=base_url();?>front/kritik/delete_balasan/<?=$row->id_content?>/<?=$row->jenis_content?>" onclick="return confirm('Apakah anda yakin menghapus data ini?')">
                                    <button type="button"  class="btn btn-block btn-danger">Delete</button>
                                    <?php endif;?>
                                    </td>
                                  </tr>
                                </tbody>
                                <?php
                                $no++;
                                endforeach;
                                ?>
                              </table>
                            </div>
                         </div>
                        <!-- /.card-body -->
                      <div class="card-footer">

                  </div>  
                </div>
                <!-- /.card -->   
              </div>
              <!-- /.card-body -->
            </div>

        <div class="container-fluid">
          <div class="row">
            <!-- left column -->
              <div class="col-md-12">
                <!-- general form elements -->
                  <div class="card card-primary">
                    <div class="card-header">
                      <h3 class="card-title">MASUKAN KRITIK DAN SARAN</h3>
                        </div>
                        <!-- /.card-header -->
                          <!-- form start -->
                          <?php if(!empty($kritik)){?>
                            <form role="form" id="form" action="<?=base_url();?>front/kritik/do_add_balas/<?=$kritik[0]->jenis_content?>" method="post">
                          <?php }else{ ?>
                          <form role="form" id="form" action="<?=base_url();?>front/kritik/do_add_kritik" method="post">
                          <?php } ?>
                          <div class="card-body">
                          <div class="row">
                            <?php foreach ($kritik as $jk) :?>
                              <input type="hidden" name="jenis_content" value="<?=$jk->jenis_content?>">
                            <?php endforeach;?>
                            <div class="col-12">
                              <div class="form-group">
                                <label>Pesan</label>
                                <textarea class="form-control" name="message" rows="3" placeholder="Enter ..."><?=set_value('message');?></textarea>
                              </div>
                            </div>
                            
                            
                            
                          </div>
                         </div>
                        <!-- /.card-body -->
                      <div class="card-footer">
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                  </form>
                </div>

                <!-- /.card -->   
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>

          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- warpper -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

 <!--  <script type="text/javascript">
          $(document).ready(function() {
         var table = $('#example').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
    } );
 
    table.buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );
    } );
        </script> -->