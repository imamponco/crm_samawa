<div id="featured-hotel" class="fh5co-bg-color">
    <div class="container">

      <div class="row">
        <div class="col-md-12">
          <div class="section-title text-center">
            <h2>Cara Pemesanan Paket</h2>
          </div>
        </div>
      </div>

      <div class="row">
         <div class="media-body">
               <ol>
                 <li>Pilih paket yang anda inginkan dan klik book now.</li>
                 <li>Masukan jumlah orang lalu klik continue</li>
                 <li>Pastikan data yang tercantum benar lalu klik pesan</li>
                 <li>Setelah berhasil melakukan pemesanan anda bisa melakukan pembayaran melalui bank ke rekening 127-00-0734463-1 an. Gaido Tours</li>
                 <li>Upload bukti pembayaran anda di tab itenary diatas atau dapat klik <a href="<?=base_url()?>/front/itenary" id="book-123456" onclick="book(this.id)">disini</a></li>
                 <li>Waktu pengunggahan bukti pembayaran paling lambat 1 jam. lebih dari itu maka paket yang telah dipesan akan dibatalkan.</li>
                 <li>Gaido tours akan melakukan validasi pembayaran kurang lebih selama 1x24 jam setelah anda mengupload pembayaran.</li>
                 <li>Setelah divalidasi anda dapat mencetak invoice pada tab itenary</li>
               </ol>
            </h3>
          </div>
      </div>
      
      <div class="row">
        <div class="col-md-12">
          <div class="section-title text-center">
            <h2>Paket <?=$kategori?></h2>
          </div>
        </div>
      </div>

      <div class="row">
        <?php 
          $no = 1;
          foreach($paket as $row):
          if($no == 1){
        ?>
        <div class="feature-full-1col">
          <div class="image" style="background-image: url(<?=base_url();?>assets/images/<?=$row->gambar_paket?>);">
            <div class="descrip text-center">
                <p><small>Harga</small><span>Rp.<?=$row->jumlah?></span></p>
            </div>
          </div>
          <div class="desc">
            <input type="hidden" name="hidden" value="<?=$row->id_paket?>" id="paket-<?=$no?>">
            <h2><?=$row->nama_paket?></h2>
            <h3><?=$row->durasi?> Days</h3>
            <p><?=$row->keterangan?></p>
            <p><a href="#" class="btn btn-primary btn-luxe-primary" id="book-<?=$no?>" onclick="book(this.id)">Book Now <i class="ti-angle-right"></i></a></p>
          </div>
        </div>
        <div class="feature-full-2col">
        <?php
        }else{
        ?>
        
          <div class="f-hotel">
            <div class="image" style="background-image: url(<?=base_url();?>assets/images/<?=$row->gambar_paket?>);">
              <div class="descrip text-center">
                <p><small>Harga</small><span>Rp.<?=$row->jumlah?></span></p>
              </div>
            </div>
            <div class="desc">
              <input type="hidden" name="hidden" value="<?=$row->id_paket?>" id="paket-<?=$no?>">
              <h2><?=$row->nama_paket?></h2>
              <h3><?=$row->durasi?> Days</h3>
              <p><?=$row->keterangan?></p>
              <p><a href="#" class="btn btn-primary btn-luxe-primary" id="book-<?=$no?>" onclick="book(this.id)">Book Now <i class="ti-angle-right"></i></a></p>
            </div>
          </div>

        <?php
          }
          $no++;
          endforeach;
        ?>
      </div>
        


      <!--     <div class="f-hotel">
            <div class="image" style="background-image: url(<?=base_url();?>assets/front/images/hotel_feture_3.jpg);">
              <div class="descrip text-center">
                <p><small>For as low as</small><span>$99/night</span></p>
              </div>
            </div>
            <div class="desc">
              <h3>D’Morvie</h3>
              <p>Pellentesque habitant morbi tristique senectus et netus ett mauada fames ac turpis egestas. Etiam euismod tempor leo, in suscipit urna condimentum sed. </p>
              <p><a href="#" class="btn btn-primary btn-luxe-primary">Book Now <i class="ti-angle-right"></i></a></p>
            </div>
          </div> -->

        
      </div>

    </div>
  </div>
