<!-- Main content -->
<style>
  .rapih{
    margin-right: 25px;
    margin-left: 25px;
  }
</style>

<div id="featured-hotel" class="fh5co-bg-color">
  <h3 class="section-tittle text-center"></h3>
    <section class="content">
      <?php
      if($this->session->flashdata('error')){?>
      <div class="rapih">
        <div class="col-4">
          <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fa fa-check"></i> Gagal!</h5>
                <?php echo $this->session->flashdata('error'); ?>
          </div>
        </div>
      </div>
      <?php
      }else if($this->session->flashdata('edit')){
      ?>
      <div class="rapih">
          <div class="col-3">
            <div class="alert alert-success alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h5><i class="icon fa fa-check"></i> Success!</h5>
                  <?php echo $this->session->flashdata('edit'); ?>
            </div>
          </div>
      </div>
      <?php
        }
      ?>
        
     
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">EDIT DATA USER</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
            <?php foreach ($user as $row) { ?>
            <form role="form" id="form" action="<?=base_url();?>front/settings/do_edit_user?>" method="post">
              <div class="card-body">
                    <div class="row" style="margin-left: 25px; margin-right: 25px;">
                      <div class="col-12">
                        <div class="form-group">
                          <label>Nama Lengkap</label>
                          <input type="text" name="username" class="form-control" placeholder="username" value="<?=$row->username?>">
                        </div>
                      </div>
          
                      <div class="col-12">
                        <div class="form-group">
                          <label>Password</label>
                          <input type="password" name="password" class="form-control" placeholder="Password" value="">
                        </div>
                      </div>

                      <div class="col-12">
                        <div class="form-group">
                          <label>Re-Password</label>
                          <input type="password" name="repassword" class="form-control" placeholder="Re-Password" value="">
                        </div>
                      </div>


                      <div class="col-12">
                        <div class="form-group">
                          <label>Email</label>
                          <input type="email" name="email" class="form-control" placeholder="Email" value="<?=$row->email?>">
                        </div>
                      </div>

                      <div class="col-12">
                        <div class="form-group">
                          <label for="exampleInputPassword1">NIK</label>
                          <input type="text" name="nik" class="form-control" placeholder="NIK" value="<?=$row->nik?>">
                        </div>
                      </div>
                      
                      <div class="col-12">
                        <div class="form-group">
                          <label>Jenis Kelamin</label>
                          <select class="form-control" name="jenis_kelamin">
                            <option value="<?=$row->jenis_kelamin?>" selected="selected"><?=$row->jenis_kelamin?></option>
                            <option value="M">M</option>
                            <option value="F">F</option>
                          </select>
                        </div>
                      </div> 
                      
                      <div class="col-12">
                        <div class="form-group">
                          <label>Tanggal Lahir</label>
                          <input type="date" name="tgl_lahir" class="form-control" value="<?=$row->tanggal_lahir?>">
                        </div>
                      </div>

                      <div class="col-12">
                        <div class="form-group">
                          <label>Tempat Lahir</label>
                          <input type="text" name="tempat_lahir" class="form-control" placeholder="Tempat Lahir" value="<?=$row->tempat_lahir?>">
                        </div>
                      </div>

                      <div class="col-12">
                        <div class="form-group">
                          <label>Relationship</label>
                          <select class="form-control" name="relationship">
                            <option value="<?=$row->relationship?>" selected="selected"><?=$row->relationship?></option>
                            <option value="Menikah">Menikah</option>
                            <option value="Belum Menikah">Belum Menikah</option>
                          </select>
                        </div>
                      </div> 

                      <div class="col-12">
                        <div class="form-group">
                          <label>Nomor Passport</label>
                          <input type="text" name="no_passport" class="form-control" placeholder="Nomor Passport" value="<?=$row->no_passport?>">
                        </div>
                      </div>

                       <div class="col-12">
                        <div class="form-group">
                          <label>No Telfon</label>
                          <input type="text" name="no_telp" class="form-control" placeholder="Nomor Telefon" value="<?=$row->no_telp?>">
                        </div>
                      </div>

                      <div class="col-12">
                        <div class="form-group">
                          <label>Alamat Lengkap</label>
                          <textarea class="form-control" name="alamat" rows="3" placeholder="Enter ..."><?=$row->alamat?></textarea>
                        </div>
                      </div>

                          <?php
                          }
                        ?>
                    </div>
                </div>
                <!-- /.card-body -->

                    <div class="card-footer" style="margin-right: 25px; margin-left: 25px;">
                      <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                  </form>
                </div>
                <!-- /.card -->   
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- <script type="text/javascript">
          $(document).ready(function() {
         var table = $('#example').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
    } );
 
    table.buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );
    } );
        </script> -->