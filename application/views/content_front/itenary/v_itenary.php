<!-- Main content -->
<div id="featured-hotel" class="fh5co-bg-color">
  <h3 class="section-tittle text-center"></h3>
    <section class="container">
       <?php if($this->session->flashdata('edit')):?>
        <div class="col-3">
          <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fa fa-check"></i> Success!</h5>
                <?php echo $this->session->flashdata('edit'); ?>
          </div>
        </div>
      <?php endif; ?>
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title text-center">MY ITERNARY</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div id="example1_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">

              <!-- <div class="row">
                <div class="col-sm-12 col-md-2">
                  <label>
                     <a href="<?=base_url();?>jadwal/add_jadwal">
                        <button type="button" class="btn btn-block btn-success">Pesan Paket</button>
                     </a>
                   </label>
                </div>
              </div> -->
              </div>

              
            
        <div class="container-fluid">
          <div class="row">
            <!-- left column -->
              <div class="col-md-12">
                <!-- general form elements -->
                  <div class="card card-primary">
                    
                        <!-- /.card-header -->
                          <!-- form start -->
                          <div class="card-body">
                            <table id="example" class="table table-bordered table-striped" style="background-color: #FFFF;  overflow-x:scroll;">
                            <thead>
                            <tr>
                              <center>
                                <th>No</th>
                                <th>Nama Paket</th>
                                <th>Nama Maskapai</th>
                                <th>No Penerbangan</th>    
                                <th>Tanggal Keberangkatan</th>
                                <th>Kota Asal</th>
                                <th>Kota Tujuan</th>
                                <th>Jam Terbang</th>
                                <th>Jam Tiba</th>
                                <th>Jumlah</th>
                                <th>Harga</th>
                                <th>Status</th>
                                <th>Bukti Pembayaran</th>
                                <th>Action</th>
                              </center>
                            </tr>
                            </thead>
                            <tbody>
                                 <?php 
                                  $no = 1;
                                  foreach ($itenary as $row) {
                                  ?>

                                  <tr>
                                    <td><?=$no?></td>
                                    <td><?=$row->nama_paket?></td>
                                    <td><?=$row->nama_maskapai ?></td>
                                    <td><?=$row->no_flight?></td>
                                    <td><?=$row->tgl_keberangkatan?></td>
                                    <td><?=$row->kota_asal?></td>
                                    <td><?=$row->kota_tujuan?></td>
                                    <td><?=$row->jam_terbang?></td>
                                    <td><?=$row->jam_tiba?></td>
                                    <td><?=$row->pax?></td>
                                    <td><?=$row->grand_total?></td>
                                    <?php 

                                    if($row->status_pemesanan == NULL || $row->status_pemesanan == 0){
                                      $status = "Belum Lunas";
                                    }else{
                                      $status = "Lunas";
                                    }

                                    ?>
                                    <td><?=$status?></td>
                                    <td>
                                      <form action="" method="post" enctype="multipart/form-data" id="form_bukti">
                                      <?php
                                        if($row->status_pemesanan == 1){
                                      ?>

                                      <a href="<?=base_url()?>invoice/print/<?=$row->id_header_pemesanan?>">
                                      <button type="button"  class="btn btn-block btn-info">Cetak Invoice</button>
                                    
                                     <!--  <a href="<?=base_url();?>jadwal/#/<?=$row->id_jadwal?>" onclick="return confirm('Apakah anda yakin jadwal telah selesai?')"> -->
                                     <!--  <button type="button"  class="btn btn-block btn-danger">Delete</button> -->
                                      <?php
                                        }else if(empty($row->bukti_pembayaran)){
                                      ?>
                                      <span id="button-delete-<?=$no?>">
                                        <input type="file" id="pembayaran-<?=$no?>" onclick="upload(this.id)" class="btn-block">
                                        <input type="hidden" value="<?=$row->id_header_pemesanan?>" id="header-<?=$no?>">
                                        <input type="hidden" value="<?=$no?>" id="bar-<?=$no?>">
                                        <div id="progress-wrp-<?=$no?>">
                                            <div class="progress-bar"></div>
                                            <div class="status">0%</div>
                                        </div>
                                      </span>
                                      <?php
                                        }else{
                                      ?>
                                        <span id="button-delete-<?=$no?>">
                                          <form action='#' method='post'>
                                                <a href='#' id='delete-<?=$no?>' onclick='delete_image(this.id)'>
                                                  <input type='hidden' id='image_name-<?=$no?>' value='<?=$row->bukti_pembayaran?>'>
                                                  <input type='hidden' id='id_header-<?=$no?>' value='<?=$row->id_header_pemesanan?>'> 
                                                  <input type='hidden' id='no-<?=$no?>' value='<?=$no?>'>  
                                                  <button type='button'  class='btn btn-block btn-danger'>Delete</button>
                                          </form>
                                        </span>
                                      <?php
                                        }
                                      ?>
                                      <!-- </a> -->
                                      </form>
                                    </td>
                                    <td>
                                      <a href="<?=base_url()?>front/itenary/delete_itenary/<?=$row->id_header_pemesanan?>" onclick="return confirm('Apakah anda yakin ingin menghapus pemesanan ini?')">
                                      <button type="button"  class="btn btn-block btn-danger">X</button>
                                    </td>
                                    
                                  </tr>
                                  <?php
                                    $no++;
                                   }
                                  ?>
                            </tbody>

                           </table>
                          </div>
                         </div>
                        <!-- /.card-body -->
                      <div class="card-footer">

                  </div>  
                </div>
                <!-- /.card -->   
              </div>
              <!-- /.card-body -->
            </div>

        <div class="container-fluid">
          <div class="row">
            <!-- left column -->
              <div class="col-md-12">
                <!-- general form elements -->
                  <div class="card card-primary">
                    <div class="card-header">
                      <h3 class="card-title text-center">VISA</h3>
                        </div>
                        <!-- /.card-header -->
                          <div class="card-body">
                          <table id="example" class="table table-bordered table-striped" style="background-color: #FFFF;">
                            <thead>
                            <tr>
                              <center>
                                <th>No</th>
                                <th>Nama Lengkap</th>
                                <th>NIK</th>
                                <th>Country</th>
                                <th>No Passport</th>
                                <th>Issuing Office</th>
                                <th>Status</th>
                                
                              </center>
                            </tr>
                            </thead>
                            <tbody>
                                 <?php
                                  $no = 1;
                                  foreach ($visa as $row) {
                                  ?>

                                  <tr>
                                    <td><?=$no?></td>
                                    <td><?=$row->username ?></td>
                                    <td><?=$row->nik ?></td>
                                    <td><?=$row->country_name?></td>
                                    <td><?=$row->no_passport?></td>
                                    <td><?=$row->issuing_office?></td>
                                    <td>
                                    <?php
                                      $status = $row->flag;
                                      switch ($status) {
                                        case '0':
                                          $show = "Proses";
                                          $proses = 1;
                                          echo "Menunggu";
                                          break;
                                        case '1':
                                          $show = "Selesai";
                                          echo "Sedang Diproses";
                                          $proses = 2;
                                          break;
                                        case '2':
                                          $show = "Selesai Dibuat";
                                          echo "Selesai Dibuat";
                                          $proses = 3;
                                          break;
                                        
                                        default:
                                          $show = "Menunggu";
                                          echo $show;
                                          break;
                                      }
                                    ?>
                                    </td>
                                   <!--  <td> -->
                                    <?php if($show=="Selesai Dibuat"){?>
                                     <!--  <a href="<?=base_url();?>Visa/verify/<?=$row->id_visa?>/<?=$proses?>">
                                      <button type="button" class="btn btn-block btn-info" onclick="return confirm('Proses visa ini?')"><?=$show?></button>
                                      </a><br> -->
                                    <?php } else { ?>
                                    <!--  <a href="<?=base_url();?>Visa/verify/<?=$row->id_visa?>/<?=$proses?>">
                                      <button type="button" class="btn btn-block btn-info" onclick="return confirm('Proses visa ini?')"><?=$show?></button>
                                     </a><br> -->
                                    <?php } ?>
                                      <!-- <a href="<?=base_url();?>Visa/delete_user/<?=$row->id_visa?>" onclick="return confirm('Apakah anda yakin menghapus data ini?')">
                                      <button type="button"  class="btn btn-block btn-danger">Delete</button> -->
                                      <!-- </a> -->
                                    <!-- </td> -->
                                    
                                  </tr>
                                  <?php
                                    $no++;
                                   }
                                  ?>
                            </tbody>

                            </table>
                          </div>
                         </div>
                        <!-- /.card-body -->
                      <div class="card-footer"></div>
                </div>

          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- warpper -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">
    var value = "";

    function delete_image(id){
      var pecah = id.split('-');
      value = pecah[1];

      var r = confirm("Apakah anda yakin ingin menghapus bukti pembayaran?");
      if(r == true){
        var data = {
          id_header : $("#id_header-"+value).val(),
          image_name : $("#image_name-"+value).val(),
          no : $("#no-"+value).val()
        };

        data = JSON.stringify(data);
        console.log(data);
        
        $.ajax({
                url  : "<?php echo base_url('Ajax/delete_image'); ?>",
                data : "data="+data,
                type : "POST",
                success: function(data) 
                {
                  document.getElementById("button-delete-"+value).innerHTML = data;
                }
            });

      }else{

      }
    }

    function upload(id){
      var pecah = id.split('-');
      //Change id to your id
      $("#"+id).on("change", function (e) {
          var file = $(this)[0].files[0];
          var upload = new Upload(file);
          // maby check size or type here with upload.getSize() and upload.getType()

          // execute upload
          upload.doUpload(pecah[1]);
      });
    }

    var Upload = function (file) {
    this.file = file;
    };

    Upload.prototype.getType = function() {
        return this.file.type;
    };
    Upload.prototype.getSize = function() {
        return this.file.size;
    };
    Upload.prototype.getName = function() {
        return this.file.name;
    };
    Upload.prototype.doUpload = function (val) {
        value = val;
        var that = this;
        var formData = new FormData();
        var id_header = $("#header-"+value).val();
        console.log(id_header)
        // add assoc key values, this will be posts values
        formData.append("file", this.file, this.getName());
        formData.append("upload_file", true);
        formData.append("id_header",id_header);
        formData.append("no",value);
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('Ajax/upload'); ?>",
            xhr: function () {
                var myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) {
                    myXhr.upload.addEventListener('progress', that.progressHandling, false);
                }
                return myXhr;
            },
            success: function (data) {
                // your callback here
                document.getElementById("button-delete-"+value).innerHTML = data;
                console.log(data)
            },
            error: function (error) {
                // handle error
            },
            async: true,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            timeout: 60000
        });
    };

    Upload.prototype.progressHandling = function (event) {
        var percent = 0;
        var position = event.loaded || event.position;
        var total = event.total;
        var progress_bar_id = "#progress-wrp-" + value;
        if (event.lengthComputable) {
            percent = Math.ceil(position / total * 100);
        }
        // update progressbars classes so it fits your code
        $(progress_bar_id + " .progress-bar").css("width", +percent + "%");
        $(progress_bar_id + " .status").text(percent + "%");
    };


  </script>