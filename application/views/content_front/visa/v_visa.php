<!-- Main content -->
<div id="featured-hotel" class="fh5co-bg-color">
  <h3 class="section-tittle text-center"></h3>
    <section class="container">
       <?php if($this->session->flashdata('edit')):?>
        <div class="col-3">
          <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fa fa-check"></i> Success!</h5>
                <?php echo $this->session->flashdata('edit'); ?>
          </div>
        </div>
      <?php endif; ?>
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title text-center">VISA</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div id="example1_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">

              <!-- <div class="row">
                <div class="col-sm-12 col-md-2">
                  <label>
                     <a href="<?=base_url();?>jadwal/add_jadwal">
                        <button type="button" class="btn btn-block btn-success">Pesan Paket</button>
                     </a>
                   </label>
                </div>
              </div> -->
              </div>
    
       <div class="row">
          <div class="col-sm-12">
            <div style ="width:auto; height:auto; overflow-x:scroll">
              <div class="feature-full-1col">
                <table id="example" class="table table-bordered table-striped" style="background-color: #FFFF;">
                <thead>
                <tr>
                  <center>
                    <th>No</th>
                    <th>NIK</th>
                    <th>Country</th>
                    <th>No Passport</th>
                    <th>Issuing Office</th>
                    <th>Status</th>
                    
                  </center>
                </tr>
                </thead>
                <tbody>
                     <?php
                      $no = 1;
                      foreach ($visa as $row) {
                      ?>

                      <tr>
                        <td><?=$no?></td>
                        <td><?=$row->nik ?></td>
                        <td><?=$row->country?></td>
                        <td><?=$row->no_passport?></td>
                        <td><?=$row->issuing_office?></td>
                        <td>
                        <?php
                          $status = $row->status;
                          switch ($status) {
                            case '0':
                              $show = "Proses";
                              $proses = 1;
                              echo "Menunggu";
                              break;
                            case '1':
                              $show = "Selesai";
                              echo "Sedang Diproses";
                              $proses = 2;
                              break;
                            case '2':
                              $show = "Selesai Dibuat";
                              echo "Selesai Dibuat";
                              $proses = 3;
                              break;
                            
                            default:
                              $show = "Menunggu";
                              echo $show;
                              break;
                          }
                        ?>
                        </td>
                       <!--  <td> -->
                        <?php if($show=="Selesai Dibuat"){?>
                         <!--  <a href="<?=base_url();?>Visa/verify/<?=$row->id_visa?>/<?=$proses?>">
                          <button type="button" class="btn btn-block btn-info" onclick="return confirm('Proses visa ini?')"><?=$show?></button>
                          </a><br> -->
                        <?php } else { ?>
                        <!--  <a href="<?=base_url();?>Visa/verify/<?=$row->id_visa?>/<?=$proses?>">
                          <button type="button" class="btn btn-block btn-info" onclick="return confirm('Proses visa ini?')"><?=$show?></button>
                         </a><br> -->
                        <?php } ?>
                          <!-- <a href="<?=base_url();?>Visa/delete_user/<?=$row->id_visa?>" onclick="return confirm('Apakah anda yakin menghapus data ini?')">
                          <button type="button"  class="btn btn-block btn-danger">Delete</button> -->
                          <!-- </a> -->
                        <!-- </td> -->
                        
                      </tr>
                      <?php
                        $no++;
                       }
                      ?>
                </tbody>

                </table>
              
                </div>
              <!-- full-card -->
              </div>
            <!-- scroll bar -->
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- warpper -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- <script type="text/javascript">
          $(document).ready(function() {
         var table = $('#example').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
    } );
 
    table.buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );
    } );
        </script> -->