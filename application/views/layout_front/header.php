<?php
$menu = $this->session->userdata('menu');

?>
  <input type="hidden" name="menu" id="menu" value="<?=$menu?>">
  <div id="fh5co-wrapper">
  <div id="fh5co-page">
  <div id="fh5co-header">
    <header id="fh5co-header-section">
      <div class="container">
        <div class="nav-header">
          <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>
          <h1 id="fh5co-logo">
            <img src="<?=base_url();?>assets/admin/dist/img/logo-gaido.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8; padding: 5px;">
            <a href="#">Gaido Travel And Tour</a>
          </h1>
          <nav id="fh5co-menu-wrap" role="navigation">
            <ul class="sf-menu" id="fh5co-primary-menu">
              <li><a id="menu_home" class="nav-link active" href="<?=base_url();?>front/index">Home</a></li>
              <li>
                <a id="menu_paket" class="nav-link" href="<?=base_url();?>front/paket/list/terbaru" class="fh5co-sub-ddown">Paket</a>
                <!-- <ul class="fh5co-sub-menu">
                  <li><a id="" href="<?=base_url();?>front/paket/list/terbaru">Paket Umroh</a></li>
                </ul> -->
              </li>
             <!--  <li><a id="" href="services.html">Artikel</a></li>
              <li><a id="" href="contact.html">Galeri</a></li> -->
              <?php if(!$this->session->userdata('is_login_user')):?>
              <!-- <li>
                <a id="" href="#" class="fh5co-sub-ddown">Profil</a>
                <ul class="fh5co-sub-menu">
                  <li><a id="" href="#">Visi dan Misi</a></li>
                  <li><a id="" href="#">Struktur Organisasi</a></li>
                  <li><a id="" href="#">Manajemen</a></li> 
                </ul>
              </li> -->
              <li><a id="menu_kontak" class="nav-link" href="<?=base_url();?>front/index/kontak">Kontak</a></li>
              
              
              <li>
                <a href="#" class="fh5co-sub-ddown">Login / Register</a>
                <ul class="fh5co-sub-menu">
                  <li><a class="nav-link" href="#" id="login">Login</a></li>
                  <li><a class="nav-link" href="#infoPanel" id="modalToggle">Register</a></li>
                </ul>
              </li>
              <?php endif;?>
             
             <?php if($this->session->userdata('is_login_user')):?>
              <li>
                  <li><a id="menu_itenary" class="nav-link" href="<?=base_url();?>front/itenary">Itenary</a></li>
              </li>
              <li>
                  <li><a id="menu_kritik" class="nav-link" href="<?=base_url();?>front/kritik">Kritik dan Saran</a></li>
              </li>
              <li>
                  <li><a id="menu_notifikasi" class="nav-link" href="<?=base_url();?>front/kritik/notifikasi">Notifikasi</a></li>
              </li>
              <li>
                  <li><a id="menu_setting" class="nav-link" href="<?=base_url();?>front/settings">Setting</a></li>
              </li>
              <li>
                  <li><a id="menu_logout" class="nav-link" href="<?=base_url();?>login/logout_front">Logout</a></li>
              </li>
            <?php endif; ?>

            </ul>
          </nav>
        </div>
      </div>
    </header>
  </div>
<body>

<!--Modal: Modal Notification-->
<div class="modal fade bd-example-modal-lg" id="modalPush" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-info" role="document">
    <!--Content-->
    <div class="modal-content text-center">
      <!--Header-->
      <div class="modal-header d-flex">
        <button type="button" class="close" data-dismiss="modalPush" >&times;</button>
        <p class="heading">Notification</p>
      </div>

      <!--Body-->
      <div class="modal-body">

        <i class="fas fa-bell fa-4x animated rotateIn mb-4"></i>

        <p id="message"></p>

      </div>

      <!--Footer-->
      <div class="modal-footer flex-center">
         <button type="button" class="btn btn-info" onclick="close_modal()">OK</button>
      </div>
    </div>
    <!--/.Content-->
  </div>
</div>
<!--Modal: modalPush-->


<!-- Modal Login -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding:35px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="glyphicon glyphicon-lock"></span> Login</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
          <form role="form" action="<?=base_url()?>login/validation_front" method="post">
            <div class="form-group">
              <label for="usrname"><span class="glyphicon glyphicon-user"></span> Email</label>
              <input type="text" name="email" class="form-control" id="username_login" placeholder="Enter email">
            </div>
            <div class="form-group">
              <label for="psw"><span class="glyphicon glyphicon-eye-open"></span> Password</label>
              <input type="password" name="password" class="form-control" id="password_login" placeholder="Enter password">
            </div>
            <!-- <div class="checkbox">
              <label><input type="checkbox" value="">Remember me</label>
            </div> -->
              <button type="submit" class="btn btn-success btn-block"><span class="glyphicon glyphicon-off"></span> Login</button>
          </form>
        </div>
        <div class="modal-footer">
          <!-- <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button> -->
         <!--  <p class="pull-left">Not a member? <a href="#" id="register">Sign Up</a></p> -->
          <p><small>&copy; Gaido Travel And Tour 2020. All Rights Reserved. <br></small></p>
        </div>
      </div>

    </div>
  </div>

<!-- Modal Register -->
<div id="modal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title" id="wizard-title">Register</h3>
      </div>
      <div class="modal-body">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
          <li class="nav-item active">
            <a class="nav-link active" data-toggle="tab" href="#infoPanel" role="tab" aria-expanded="true">Userdata</a>
          <li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#ads" role="tab">Primary Information</a>
          <li>
        </ul>
        
        <!-- Userdata -->
        <div class="tab-content mt-2">
          <div class="tab-pane fade active in" id="infoPanel" role="tabpanel">
            <h4></h4>
            <form role="form" action="#" method="post">
            <div class="form-group">
              <label for="usrname"><span class="glyphicon glyphicon-user"></span> Username</label>
              <input type="text" class="form-control" placeholder="Username" name="username" id="username" required="required">
            </div>
            
            <div class="form-group">
              <label for="usrname"><span class="glyphicon glyphicon-envelope"></span> Email</label>
              <input type="email" class="form-control" placeholder="Email" name="email_register" id="email_register" required="required">
            </div>
            
            <div class="form-group">
              <label for="usrname"><span class="glyphicon glyphicon-lock"></span> Password</label>
              <input type="password" class="form-control" placeholder="Password" name="password" id="password" required="required">
            </div>
            
            <div class="form-group">
              <label for="usrname"><span class="glyphicon glyphicon-lock"></span> Re-password</label>
              <input type="password" class="form-control" placeholder="Retype password" name="repassword" id="repassword" required="required">
            </div>
            <input type="hidden" class="form-control" name="role" value="CO">
            <button class="btn btn-secondary" id="infoContinue">Continue</button>
          </div>
          
          <div class="tab-pane fade" id="ads" role="tabpanel">
            <h4></h4>
            <div class="form-group">
              <label for="usrname"><span class="glyphicon glyphicon-duplicate"></span> NIK</label>
              <input type="text" class="form-control" placeholder="Nomor induk kependudukan" name="nik" id="nik" required="required">
            </div>
            
            <!-- <div class="form-group">
              <label for="usrname"><span class="glyphicon glyphicon-user"></span> Nama Lengkap</label>
              <input type="text" class="form-control" placeholder="Full name" name="nama" id="nama" required="required">
            </div> -->
            
            <div class="form-group">
              <label for="jenis_kelamin"><span class="glyphicon glyphicon-resize-small"></span> Jenis Kelamin</label>
              <select class="form-control" name="jenis_kelamin" id="jenis_kelamin" required="required">
                        <option selected disabled>Choose a gender</option>
                        <option value="M">Laki-Laki</option>
                        <option value="F">Perempuan</option>
              </select>
            </div>
            
            <div class="form-group">
              <label for="usrname"><span class="glyphicon glyphicon-map-marker"></span> Tempat Lahir</label>
              <input type="text" class="form-control" placeholder="Tempat Lahir" name="tempat_lahir" id="tempat_lahir" required="required">
            </div>
            
            <div class="form-group">
              <label for="usrname"><span class="glyphicon glyphicon-eject"></span> Tanggal Lahir</label>
              <input type="date" class="form-control" placeholder="Tanggal Lahir" name="tgl_lahir" id="tgl_lahir" required="required">
            </div>
            
            <div class="form-group">
              <label for="usrname"><span class="glyphicon glyphicon-phone"></span> No Telp</label>
              <input type="text" class="form-control" placeholder="No Telp" name="no_telp" id="no_telp" required="required">
            </div>
            
            <div class="form-group">
              <label for="usrname"><span class="glyphicon glyphicon-home"></span> Alamat</label>
              <textarea name="alamat" class="form-control" placeholder="Alamat" id="alamat" required="required"></textarea>
            </div>

            <div class="form-group">
              <label for="id_agen"></span> Nama Agen (Optional)</label>
              <select class="form-control" name="id_agen" id="id_agen">
                        <option selected="selected"  value="AG001" disabled>Gaido Tour</option>
                        <?php foreach($agen as $row):?>
                        <option value="<?=$row->id_agen?>"><?=$row->nama_agen?></option>
                        <?php endforeach;?>
              </select>
            </div>

            <div class="form-group">
              <label for="no_passport"><span class="glyphicon folder-open"></span> No Passport </label>
              <input type="text" class="form-control" placeholder="No Passport" name="no_passport" id="no_passport">
            </div>
            
            <button class="btn btn-primary btn-block" id="activate">Finish</button>
          </div>
        </form>
        </div>
        <div class="progress mt-5">
          <div class="progress-bar" role="progressbar" style="width: 30%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">Step 1 of 2</div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <!-- <button type="button" class="btn btn-primary">Save for later</button> -->
      </div>
    </div>
  </div>
 </div>
 <!-- end modal register -->

 <!-- Modal Daftar Paket -->
<div id="modal_paket" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title" id="wizard-title">Daftar Paket</h3>
      </div>
      <div class="modal-body">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
          <li class="nav-item active">
            <a class="nav-link active" data-toggle="tab" href="#paket" role="tab" aria-expanded="true">Paket</a>
          <li>
          <li class="nav-item" id="tab-payment">
            <a class="nav-link" data-toggle="tab" href="#payment" role="tab">Payment</a>
          <li>
        </ul>
        
        <!-- Pesan Paket -->
        <form role="form" class="pesan_paket" action="#" method="post">
        <div class="tab-content mt-2">
          <div class="tab-pane fade active in" id="paket" role="tabpanel">
            <h4></h4>
            
            <div class="form-group">
              <label for="usrname"> Nama Paket</label>
              <input type="text" class="form-control" placeholder="Nama Paket"  name="id_paket" id="id_paket" required="required" disabled>
            </div>

            <div class="form-group">
              <label for="usrname"> Harga</label>
              <input type="text" class="form-control" placeholder="Harga"  name="Harga" id="harga" required="required" disabled>
            </div>
            
            <div class="form-group">
              <label for="usrname"> Diskon</label>
              <input type="text" class="form-control" placeholder="0" value="0" name="diskon" id="diskon" required="required" disabled>
            </div>
            
            <div class="form-group">
              <label for="usrname"> Durasi</label>
              <input type="text" class="form-control" placeholder="0" value="0" name="durasi" id="durasi" required="required" disabled>
            </div>
            
            <div class="form-group">
              <label for="usrname"> Keterangan</label>
              <textarea name="alamat" class="form-control" placeholder="keterangan" id="keterangan" required="required" disabled></textarea>
            </div>

            <div class="form-group">
              <label for="jenis_kelamin"> Jadwal </label>
              <select class="form-control" name="jadwal" id="jadwal" onchange="kuota(this.value)" required="required">        
              </select>
            </div>

             <div class="form-group">
              <label for="usrname"> Pax</label>
              <input type="hidden" name="maksimal" id="maksimal">
              <input type="number" class="form-control" placeholder="Jumlah Orang"  min="1" name="pax" id="pax" onchange="calculate()" required="required">
            </div>
            <button class="btn btn-secondary" id="continuePesan">Continue</button>
          </div>
          

          <div class="tab-pane fade" id="payment" role="tabpanel">
            <h4></h4>
            <div class="form-group">
              <label for="usrname"> Nama Lengkap</label>
              <input type="text" class="form-control" placeholder="Full name"  name="nama_lengkap" id="nama_lengkap" required="required" disabled>
            </div>
            
            <div class="form-group">
              <label for="usrname"> NIK</label>
              <input type="text" class="form-control" placeholder="NIK"  name="nik_konfirmasi" id="nik_konfirmasi" required="required" disabled>
            </div>

            <div class="form-group">
              <label for="usrname"></span> Paket</label>
              <input type="text" class="form-control" placeholder=""  name="id_paket_konfirmasi" id="id_paket_konfirmasi" required="required" disabled>
            </div>
            
            <div class="form-group">
              <label for="usrname"></span> Tanggal Keberangkatan</label>
              <select class="form-control" name="id_jadwal_konfirmasi" id="id_jadwal_konfirmasi" required="required">        
              </select>
            </div>

            <div class="form-group">
              <label for="usrname"></span> Harga</label>
              <input type="text" class="form-control" placeholder="No Telp" value="0"  name="harga_konfirmasi" id="harga_konfirmasi" required="required" disabled>
            </div>
            
            <div class="form-group">
              <label for="usrname"></span> Pax</label>
              <input type="transaction-amount" class="form-control" placeholder="Jumlah orang"  name="pax_konfirmasi" id="pax_konfirmasi" required="required" disabled>
            </div>


            <div class="form-group">
              <label for="usrname"></span> Subtotal</label>
              <input type="text" class="form-control" placeholder="0"  name="subtotal" id="subtotal" required="required" disabled>
            </div>

            <div class="form-group">
              <label for="usrname"></span> Diskon</label>
              <input type="text" class="form-control" placeholder="0" value="0"  name="diskon" id="diskon_konfirmasi" required="required" disabled>
            </div>

            <div class="form-group">
              <label for="usrname"></span> Tax</label>
              <input type="text" class="form-control" placeholder="0" value="0"  name="tax" id="tax" required="required" disabled>
            </div>

            <div class="form-group">
              <label for="usrname"></span> Other</label>
              <input type="text" class="form-control" placeholder="0" value="0"  name="other" id="other" required="required" disabled>
            </div>
            
            <div class="form-group">
              <label for="usrname"></span> Grand Total</label>
              <input type="text" class="form-control" placeholder="Grand Total"  name="grandtotal" id="grandtotal" required="required" disabled>
            </div>

            <button class="btn btn-primary btn-block" id="pesan">Pesan</button>
          </div>

        </form>
        </div>
        <div class="progress mt-5">
          <div class="progress-bar" role="progressbar" style="width: 30%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">Step 1 of 2</div>
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <!-- <button type="button" class="btn btn-primary">Save for later</button> -->
      </div>

    </div>
  </div>
</div>
<!-- end modal pesan -->