  <footer id="footer" class="fh5co-bg-color">
    <div class="container">
      <div class="row">
        <div class="col-md-3">
          <div class="copyright">
            <p><small>&copy; Gaido Travel And Tour 2020. <br> All Rights Reserved. <br></small></p>
           <!--  Designed by <a href="#" target="_blank">FreeHTML5.co</a> <br> Demo Images: <a href="#" target="_blank">Unsplash</a></small></p> -->
          </div>
        </div>
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-3">
              <h3>Perusahaan</h3>
              <ul class="link">
                <!-- <li><a href="#">Tentang Kami</a></li> -->
                <li><a href="<?=base_url()?>front/index/kontak">Kontak Kami</a></li>
              </ul>
            </div>
            <div class="col-md-3">
              <h3>Layanan Kami</h3>
              <ol class="link">
                <li>Resmi</li>
                <li>Komitmen</li>
                <li>Pembimbing Professional</li>
                <li>Fasilitas Berkualitas</li>
              </ol>
            </div>
            <!-- <div class="col-md-6">
              <h3>Subscribe</h3>
              <p>Sed cursus ut nibh in semper. Mauris varius et magna in fermentum. </p>
              <form action="#" id="form-subscribe">
                <div class="form-field">
                  <input type="email" placeholder="Email Address" id="email">
                  <input type="submit" id="submit" value="Send">
                </div>
              </form>
            </div> -->
          </div>
        </div>
        <!-- <div class="col-md-3">
          <ul class="social-icons">
            <li>
              <a href="#"><i class="icon-twitter-with-circle"></i></a>
              <a href="#"><i class="icon-facebook-with-circle"></i></a>
              <a href="#"><i class="icon-instagram-with-circle"></i></a>
              <a href="#"><i class="icon-linkedin-with-circle"></i></a>
            </li>
          </ul>
        </div> -->
      </div>
    </div>
  </footer>

  </div>
  <!-- END fh5co-page -->

  </div>
  <!-- END fh5co-wrapper -->

  <script type="text/javascript">
  $(document).ready(function() {
          let menu = $('#menu').val();
          $('.nav-link').removeClass('active');
          $('#'+menu).addClass('active');

  });

  $(document).ready(function() 
    {
        $("#book").click(function() 
        {

        });

    });

    $('#continuePesan').click(function (e) {
      e.preventDefault();
      var form1 = {
        jadwal: $('#jadwal').val(),
        pax: $('#pax').val(),
      }
      console.log(form1.username);
      if (form1.jadwal && form1.pax) {
        $('.progress-bar').css('width', '100%');
        $('.progress-bar').html('Step 2 of 2');
        $('#myTab a[href="#payment"]').tab('show');
      }else{
        e.preventDefault();
        var required = "Terdapat data yang belum diisi";
        alert(required);
      }
      
    });

    function book(id){ 
      var cek = "<?=$this->session->userdata('is_login_user')?>";
        if (cek == 1) {
            var pecah = id.split('-');
            var no = pecah[1];
            var id_paket = $("#paket-"+no).val();
            $.ajax({
              url  : "<?php echo base_url('Ajax/get_paket_by_id'); ?>",
              data : "id_paket="+id_paket,
              type : "POST",
              success: function(data) 
              { 
                $('#modal_paket').modal('toggle');
                var obj = JSON.parse(data);
  
                var x,jadwal,isi = "";
                for (x in obj[0]) {
                   jadwal += "<option value='"+ obj[0][x].id_jadwal + "-" + obj[0][x].id_paket+ "'>" + obj[0][x].tgl_keberangkatan+ "</option>";
                }

              isi = "<option value='"+ obj[0][0].id_jadwal + "-" + obj[0][0].id_paket+ "'>" + obj[0][0].tgl_keberangkatan+ "</option>";

             
              console.log(obj[1][0].username);
              (obj[0][0].nama_paket != "NaN" || obj[0][0].nama_paket != undefined ? $('#id_paket').val(obj[0][0].nama_paket) : $('#id_paket').val(0));
              (obj[0][0].jumlah != null || obj[0][0].nama_paket != undefined ? $('#harga').val(obj[0][0].jumlah) : $('#harga').val('0'));
              (obj[0][0].diskon != null || obj[0][0].diskon != undefined ? $('#diskon').val(obj[0][0].diskon) : $('#diskon').val('0'));
              (obj[0][0].durasi != null || obj[0][0].durasi != undefined ? $('#durasi').val(obj[0][0].durasi) : $('#durasi').val('0'));
              (obj[0][0].keterangan != null || obj[0][0].keterangan != undefined ? $('#keterangan').val(obj[0][0].keterangan) : $('#keterangan').val(0));
              (obj[0][0].jadwal != null || obj[0][0].jadwal != undefined ? $('#jadwal').val(obj[0][0].jadwal) : $('#jadwal').val(0));
              (obj[0][0].nama_paket != null || obj[0][0].nama_paket != undefined ? $('#id_paket_konfirmasi').val(obj[0][0].nama_paket) : $('#id_paket_konfirmasi').val(0));
              (obj[0][0].jumlah != null || obj[0][0].jumlah != undefined ? $('#harga_konfirmasi').val(obj[0][0].jumlah) : $('#harga_konfirmasi').val('0'));
              (obj[0][0].diskon != null || obj[0][0].diskon != undefined ? $('#diskon_konfirmasi').val(obj[0][0].diskon) : $('#diskon_konfirmasi').val('0'));
              (obj[1][0].username != null || obj[1][0].username != undefined? $('#nama_lengkap').val(obj[1][0].username) : $('#nama_lengkap').val());
              (obj[1][0].nik != null || obj[1][0].nik != undefined ? $('#nik_konfirmasi').val(obj[1][0].nik) : $('#nik_konfirmasi').val('0'));
              (obj[0][0].tax != null || obj[0][0].tax != undefined ? $('#tax').val(obj[0][0].tax) : $('#tax').val('0'));
              (obj[0][0].other != null || obj[0][0].other != undefined ? $('#other').val(obj[0][0].other) : $('#other').val('0'));
              (isi != null || isi != undefined ? $('#jadwal').html(isi) : $('#jadwal').html('<p>Description</p>'));
              (isi != null || isi != undefined ? $('#id_jadwal_konfirmasi').html(isi) : $('#id_jadwal_konfirmasi').html('<p>Description</p>'));

                // $('#id_paket').val(obj[0][0].nama_paket);
                // $("#harga").val(obj[0][0].jumlah);
                // $("#diskon").val(obj[0][0].diskon);
                // $("#durasi").val(obj[0][0].durasi);
                // $("#keterangan").val(obj[0][0].keterangan);
                // $("#jadwal").html(jadwal);

                // $("#id_paket_konfirmasi").val(obj[0][0].nama_paket);
                // $("#harga_konfirmasi").val(obj[0][0].jumlah);
                // $("#diskon_konfirmasi").val(obj[0][0].diskon);
                // $("#nama_lengkap").val(obj[1][0].username);
                // $("#nik_konfirmasi").val(obj[1][0].nik);
                // $("#tax").val(obj[0][0].tax);
                // $("#other").val(obj[0][0].other);
                // console.log(obj[1][0].nik)
                // $("#id_jadwal_konfirmasi").html(isi);

              
              }
            });
        }else{
              //open modal login
              $('#myModal').modal('show');
       }
    }

    function kuota(id){
      var data = id.split('-');
      data = JSON.stringify(data);
      // console.log(data)
      $.ajax({
          url  : "<?php echo base_url('Ajax/get_kuota'); ?>",
          data : "data="+data,
          type : "POST",
          success: function(data) 
          { 
            var obj = JSON.parse(data);
            var jadwal = "";
            jadwal = "<option value='"+ obj[1].id_jadwal +"'>" + obj[1].tgl_keberangkatan+ "</option>";
            document.getElementById('pax').max = obj[0];
            $("#maksimal").val(obj[0]);
            console.log($("#max").val(obj[0]))
            $("#id_jadwal_konfirmasi").html(jadwal);
          }
        });
    }

    function empty(data)
    {
      if(typeof(data) == 'number' || typeof(data) == 'boolean')
      { 
        return false; 
      }
      if(typeof(data) == 'undefined' || data === null)
      {
        return true; 
      }
      if(typeof(data.length) != 'undefined')
      {
        return data.length == 0;
      }
      var count = 0;
      for(var i in data)
      {
        if(data.hasOwnProperty(i))
        {
          count ++;
        }
      }
      return count == 0;
    }

    function calculate(){
      //get value from id
      var jumlah,harga,tax,diskon,other,subtotal,grandtotal,kurang;
      jumlah = $("#pax").val();
      harga = parseInt($("#harga").val());
      tax = parseInt($("#tax").val());
      diskon = parseInt($("#diskon").val());
      other = parseInt($("#other").val());

      //calculate
      subtotal = harga * jumlah;
      grandtotal = (subtotal + tax + other) - diskon;

      console.log(grandtotal)

      //change value
      $("#pax_konfirmasi").val(jumlah);
      $("#subtotal").val(subtotal);
      $("#grandtotal").val(grandtotal);
    }

    $('#pesan').click(function (e) {
      e.preventDefault();
      var id,id_jadwal,id_paket,id_user;
      id = $('#jadwal').val().split('-');
      id_jadwal = id[0];
      id_paket = id[1];
      id_user = "<?=$this->session->userdata('id_user_pelanggan')?>"

      var formPesan = {
        id_paket : id_paket,
        id_jadwal : id_jadwal,
        maksimal : $('#maksimal').val(),
        pax: $('#pax').val(),
        id_user: id_user,
        nik_konfirmasi: $('#nik_konfirmasi').val(),
        subtotal: $('#subtotal').val(),
        grandtotal: $('#grandtotal').val()
      };
      formPesan = JSON.stringify(formPesan);
      console.log(formPesan);
      
      $.ajax({
              url  : "<?php echo base_url('Ajax/pesan_paket'); ?>",
              data : "data="+formPesan,
              type : "POST",
              success: function(data) 
              {
                $('#modal_paket').modal('toggle');   
                $('#modalPush').modal('toggle');
                document.getElementById("message").innerHTML = data;
              }
          });
      });




  function notification (data){
      $('#modal').modal('toggle');
      $('#modalPush').modal('toggle');
      document.getElementById("message").innerHTML = data;
  }

  function open_modal(){
    $('#modal').modal('toggle');
  }
  
   function close_modal(){
   $('div').modal('hide');
   var ok = $('body').removeAttribute("style");
   alert(ok)
  }
  
  $('#login').click(function(){
     $('#myModal').modal('show');
  });
  

  $('#next1').click(function(){
     $('#modalPelanggan').modal('show');
  });

  $('.close').click(function(){
    $('div').modal('hide');
    $('body').removeAttribute("style");
    });


  // $(function () {
  // $('#register').click(function() {
  //   $('#myModal').modal('hide');
  //   $('#modalToggle').modal({
  //     backdrop: 'static'
  //   });
  // });
   

    $(document).ready(function() {

    $('#modalToggle').click(function() {
      $('#modal').modal({
       backdrop: 'static'
      });
    });

    $('#infoContinue').click(function (e) {
      e.preventDefault();
      var form1 = {
        username: $('#username').val(),
        email: $('#email_register').val(),
        password: $('#password').val(),
        repassword: $('#repassword').val(),
      }
      console.log(form1.username);
      if (form1.username && form1.email && form1.password == form1.repassword) {
        $('.progress-bar').css('width', '100%');
        $('.progress-bar').html('Step 2 of 2');
        $('#myTab a[href="#ads"]').tab('show');
      }else{
        e.preventDefault();
        var required = "Terdapat data yang belum diisi atau password tidak sama";
        alert(required);
      }
      
    });

    $('#activate').click(function (e) {
      e.preventDefault();
      var id_agen;
      if($('#id_agen').val()==null)
      {
        id_agen = "AG001";
      }else{
        id_agen = $('#id_agen').val();
      }
      var dataString = $("form").serialize();
      var formData = {
        username : $('#username').val(),
        email : $('#email_register').val(),
        password : $('#password').val(),
        repassword: $('#repassword').val(),
        nik: $('#nik').val(),
        nama: $('#nama').val(),
        jenis_kelamin: $('#jenis_kelamin').val(),
        tempat_lahir: $('#tempat_lahir').val(),
        tgl_lahir: $('#tgl_lahir').val(),
        no_telp: $('#no_telp').val(),
        alamat: $('#alamat').val(),
        no_passport: $('#no_passport').val(),
        id_agen: id_agen
      };
      dataString += "&id_agen="+id_agen;
      
      dataString = JSON.stringify(dataString);
      formData = JSON.stringify(formData);
      console.log(formData);
      
      $.ajax({
              url  : "<?php echo base_url('Ajax/register_front'); ?>",
              data : dataString,
              type : "POST",
              success: function(data) 
              {
                $('#modal').modal('toggle');   
                $('#modalPush').modal('toggle');
                document.getElementById("message").innerHTML = data;
              }
          });
      });

      /* alert(JSON.stringify(formData)); */
    });

  
  </script>

</body>
</html>