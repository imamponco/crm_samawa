<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
  <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Gaido Travel And Tour &mdash;</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Free HTML5 Template by FREEHTML5.CO" />
  <meta name="keywords" content="free html5, free template, free bootstrap, html5, css3, mobile first, responsive" />
  <meta name="author" content="FREEHTML5.CO" />

  <!-- 
  //////////////////////////////////////////////////////

  FREE HTML5 TEMPLATE 
  DESIGNED & DEVELOPED by FREEHTML5.CO
    
  Website:    http://freehtml5.co/
  Email:      info@freehtml5.co
  Twitter:    http://twitter.com/fh5co
  Facebook:     https://www.facebook.com/fh5co

  //////////////////////////////////////////////////////
   -->

    <!-- Facebook and Twitter integration -->
  <meta property="og:title" content=""/>
  <meta property="og:image" content=""/>
  <meta property="og:url" content=""/>
  <meta property="og:site_name" content=""/>
  <meta property="og:description" content=""/>
  <meta name="twitter:title" content="" />
  <meta name="twitter:image" content="" />
  <meta name="twitter:url" content="" />
  <meta name="twitter:card" content="" />

  <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
  <link rel="shortcut icon" href="<?=base_url();?>assets/admin/dist/img/icon.png">
 <link href='https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700italic,900,700,900italic' rel='stylesheet' type='text/css'>

  <!-- Stylesheets -->
  <!-- Dropdown Menu -->
  <link rel="stylesheet" href="<?=base_url();?>assets/front/css/superfish.css">
  <!-- Owl Slider -->
  <link rel="stylesheet" href="<?=base_url();?>assets/front/css/owl.carousel.css">
  <link rel="stylesheet" href="<?=base_url();?>assets/front/css/owl.theme.default.min.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?=base_url();?>assets/front/css/bootstrap-datepicker.min.css">
  <!-- CS Select -->
  <link rel="stylesheet" href="<?=base_url();?>assets/front/css/cs-select.css">
  <link rel="stylesheet" href="<?=base_url();?>assets/front/css/cs-skin-border.css">

  <!-- Datatables -->
  <link rel="stylesheet" href="<?=base_url();?>assets/admin/plugins/datatables/dataTables.bootstrap4.min.css">

  <!-- Themify Icons -->
  <link rel="stylesheet" href="<?=base_url();?>assets/front/css/themify-icons.css">
  <!-- Flat Icon -->
  <link rel="stylesheet" href="<?=base_url();?>assets/front/css/flaticon.css">
  <!-- Icomoon -->
  <link rel="stylesheet" href="<?=base_url();?>assets/front/css/icomoon.css">
  <!-- Flexslider  -->
  <link rel="stylesheet" href="<?=base_url();?>assets/front/css/flexslider.css">
  
  <!-- Style -->
  <link rel="stylesheet" href="<?=base_url();?>assets/front/css/style.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/admin/css/style.css">
  

  <!-- Modernizr JS -->
  <script src="<?=base_url();?>assets/front/js/modernizr-2.6.2.min.js"></script>
  <!-- FOR IE9 below -->
  <!--[if lt IE 9]>
  <script src="js/respond.min.js"></script>
  <![endif]-->

  
  <!-- Javascripts -->
  <script src="<?=base_url();?>assets/front/js/jquery-2.1.4.min.js"></script>
  <!-- Dropdown Menu -->
  <script src="<?=base_url();?>assets/front/js/hoverIntent.js"></script>
  <script src="<?=base_url();?>assets/front/js/superfish.js"></script>
  <!-- Bootstrap -->
  <script src="<?=base_url();?>assets/front/js/bootstrap.min.js"></script>
  <!-- Waypoints -->
  <script src="<?=base_url();?>assets/front/js/jquery.waypoints.min.js"></script>
  <!-- Counters -->
  <script src="<?=base_url();?>assets/front/js/jquery.countTo.js"></script>
  <!-- Stellar Parallax -->
  <script src="<?=base_url();?>assets/front/js/jquery.stellar.min.js"></script>
  <!-- Owl Slider -->
  <script src="<?=base_url();?>assets/front/js/owl.carousel.min.js"></script>
  <!-- Date Picker -->
  <script src="<?=base_url();?>assets/front/js/bootstrap-datepicker.min.js"></script>
  <!-- CS Select -->
  <script src="<?=base_url();?>assets/front/js/classie.js"></script>
  <script src="<?=base_url();?>assets/front/js/selectFx.js"></script>
  <!-- Flexslider -->
  <script src="<?=base_url();?>assets/front/js/jquery.flexslider-min.js"></script>

  <script src="<?=base_url();?>assets/front/js/custom.js"></script>

</head>

