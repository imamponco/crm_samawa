<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Reset Password</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https:/maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https:/code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url();?>assets/admin/dist/css/adminlte.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?=base_url();?>assets/admin/plugins/iCheck/square/blue.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https:/fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <style type="text/css">
  body{
      background-image: url('assets/login/background.jpg');
      background-size: cover;
    }
</style>
</head>
<body class="hold-transition register-page">
<!-- $this->session->flashdata('register') -->
<?php if($this->session->flashdata('register')):?>
  <div class="modal" id="myModal">
  <!-- Modal Content-->
  <center>
    <div class="col-3" style="padding-top: 10%;">
        <div class="alert alert-success alert-dismissible">
              <button type="button" class="close" id="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h5><i class="icon fa fa-check"></i> Success!</h5>
              <?php echo $this->session->flashdata('register'); echo " <a href=".base_url()."login>disini</a>"; ?>
        </div>
      </div>
  </center>
      
</div>
<?php endif; ?>

<?php echo validation_errors(); ?>
<div class="register-box">
  <div class="register-logo">
    <b>Reset Password</b><br>
  </div>

  <div class="card">
    <div class="card-body register-card-body">
      <p class="login-box-msg">Enter your new password.</p>

      <form action="<?=base_url();?>login/do_reset_password?token=<?=$_GET["token"]?>" method="post">
        <div class="form-group has-feedback">
          <input type="password" class="form-control" placeholder="Password" name="password" <?=set_value('password');?>>
          <span class="fa fa-lock form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <input type="password" class="form-control" placeholder="Retype password" name="repassword" <?=set_value('repassword');?>>
          <span class="fa fa-lock form-control-feedback"></span>
        </div>
        <div class="row">
          <!-- <div class="col-8">
            <div class="checkbox icheck">
              <label>
                <input type="checkbox"> I agree to the <a href="#">terms</a>
              </label>
            </div>
          </div> -->
          <!-- /.col -->
          <div class="col-12">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <div class="social-auth-links text-center">
        <!-- <center><p>- OR -</p></center>
         --><!-- <a href="#" class="btn btn-block btn-primary">
          <i class="fa fa-facebook mr-2"></i>
          Sign up using Facebook
        </a>
        <a href="#" class="btn btn-block btn-danger">
          <i class="fa fa-google-plus mr-2"></i>
          Sign up using Google+
        </a>
      </div> -->
    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
</div>
<!-- /.register-box -->

<!-- jQuery -->
<script src="<?=base_url();?>assets/admin/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?=base_url();?>assets/admin/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- iCheck -->
<script src="<?=base_url();?>assets/admin/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass   : 'iradio_square-blue',
      increaseArea : '20%' / optional
    })
  })


$('#myModal').modal('show');

$('#close').click(function(){
  $('#myModal').modal('hide');
  }); 

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>

</body>
</html>
