<!-- Main content -->
    <section class="content">
       <?php if($this->session->flashdata('edit')):?>
        <div class="col-3">
          <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fa fa-check"></i> Success!</h5>
                <?php echo $this->session->flashdata('edit'); ?>
          </div>
        </div>
      <?php endif; ?>
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Visa Pelanggan</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div id="example1_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                 <!-- filter -->
                <div class="row">
                 
                   <div class="col-sm-12 col-md-3">
                    <label>
                    <?php echo form_open('Visa/list');?>
                    <div class="form-group">
                      <label>Filter</label>
                        <select class="form-control" name="filter"> 
                        <option selected="selected" value="0">Menunggu</option>
                        <option value="1">Sedang Diproses</option>
                        <option value="2">Selesai Dibuat</option>
                       
                        </select>
                      </div>
                    </label>
                    </div>

                    <div class="col-sm-12 col-md-2">
                      <label>
                        Action

                        <!--  <a href="<?=base_url();?>Pemesanan/list"> -->
                            <button type="submit" class="btn btn-block btn-success">Go</button>
                        <!--  </a> -->
                       </label> 
                     <?php echo form_close();?>
                    </div>
                </div>

                <!-- <div class="row">
                <div class="col-sm-12 col-md-2"">
                <label>
                   <a href="<?=base_url();?>user/add_user">
                      <button type="button" class="btn btn-block btn-success">Add Data</button>
                   </a>
                  </div>
                </label>
                </div> -->

          </div>

       <div class="row">
          <div class="col-sm-12">
            <div style ="width:auto; height:auto; overflow-x:scroll">
              <table id="example" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <center>
                    <th>No</th>
                    <th>Nama Lengkap</th>
                    <th>NIK</th>
                    <th>Country</th>
                    <th>No Passport</th>
                    <th>Issuing Office</th>    
                    <th>Date of Issue</th>
                    <th>Date of Expiry</th>
                    <th>Relationship</th>
                    <th>Status</th>
                    <th>Action</th>
                  </center>
                </tr>
                </thead>
                <tbody>
                     <?php
                      $no = 1;
                      foreach ($visa as $row) {
                      ?>

                      <tr>
                        <td><?=$no?></td>
                        <td><?=$row->username ?></td>
                        <td><?=$row->nik ?></td>
                        <td><?=$row->country_name?></td>
                        <td><?=$row->no_passport?></td>
                        <td><?=$row->issuing_office?></td>
                        <td><?=$row->date_of_issue?></td>
                        <td><?=$row->date_of_expiry?></td>
                        <td><?=$row->relationship?></td>
                        <td>
                        <?php
                          $status = $row->flag;
                          switch ($status) {
                            case '0':
                              $show = "Proses";
                              $proses = 1;
                              echo "Menunggu";
                              break;
                            case '1':
                              $show = "Selesai";
                              echo "Sedang Diproses";
                              $proses = 2;
                              break;
                            case '2':
                              $show = "Selesai Dibuat";
                              echo "Selesai Dibuat";
                              $proses = 3;
                              break;
                            
                            default:
                              $show = "Menunggu";
                              echo $show;
                              break;
                          }
                        ?>
                        </td>
                        <td>
                        <?php if($show=="Selesai Dibuat"){?>
                         <!--  <a href="<?=base_url();?>Visa/verify/<?=$row->id_visa?>/<?=$proses?>">
                          <button type="button" class="btn btn-block btn-info" onclick="return confirm('Proses visa ini?')"><?=$show?></button>
                          </a><br> -->
                        <?php } else { ?>
                         <a href="<?=base_url();?>Visa/verify/<?=$row->id_visa?>/<?=$proses?>">
                          <button type="button" class="btn btn-block btn-info" onclick="return confirm('Proses visa ini?')"><?=$show?></button>
                         </a><br>
                        <?php } ?>
                          <a href="<?=base_url();?>Visa/delete_visa/<?=$row->id_visa?>" onclick="return confirm('Apakah anda yakin menghapus data ini?')">
                          <button type="button"  class="btn btn-block btn-danger">Delete</button>
                          <!-- </a> -->
                        </td>
                        
                      </tr>
                      <?php
                        $no++;
                       }
                      ?>
                </tbody>

               </table>

              </div>
            <!-- scroll bar -->
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- warpper -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">
          $(document).ready(function() {
         var table = $('#example').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
    } );
 
    table.buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );
    } );
        </script>