<!-- Main content -->
    <section class="content">
       <?php if($this->session->flashdata('edit')):?>
        <div class="col-3">
          <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fa fa-check"></i> Success!</h5>
                <?php echo $this->session->flashdata('edit'); ?>
          </div>
        </div>
      <?php endif; ?>
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Master Agen</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div id="example1_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                <div class="row">

                <div class="col-sm-12 col-md-2"">
                <label>
                   <a href="<?=base_url();?>Agen/add_agen">
                      <button type="button" class="btn btn-block btn-success">Add Data</button>
                   </a>
                  </div>
                </label>
                </div>

          </div>

       <div class="row">
          <div class="col-sm-12">
            <div style ="width:auto; height:auto; overflow-x:scroll">
              <table id="example" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <center>
                    <th>No</th>
                    <th>Nama Lengkap</th>
                    <th>Jenis Kelamin</th>    
                    <th>No Telp</th>
                    <th>Alamat</th>
                    <th>Action</th>
                  </center>
                </tr>
                </thead>
                <tbody>
                     <?php 
                      $no = 1;
                      foreach ($agen as $row) {
                      ?>

                      <tr>
                        <td><?=$no?></td>
                        <td><?=$row->nama_agen ?></td>
                        <td><?=$row->jenis_kelamin_agen?></td>
                        <td><?=$row->no_telp_agen?></td>
                        <td><?=$row->alamat_agen?></td>
                        <td>
                          <a href="<?=base_url();?>Agen/edit_agen/<?=$row->id_agen?>">
                          <button type="button" class="btn btn-block btn-primary">Edit</button>
                          </a><br>
                          <a href="<?=base_url();?>Agen/delete_agen/<?=$row->id_agen?>" onclick="return confirm('Apakah anda yakin menghapus data ini?')">
                          <button type="button"  class="btn btn-block btn-danger">Delete</button>
                          <!-- </a> -->
                        </td>
                        
                      </tr>
                      <?php
                        $no++;
                       }
                      ?>
                </tbody>

               </table>

              </div>
            <!-- scroll bar -->
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- warpper -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">
          $(document).ready(function() {
         var table = $('#example').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
    } );
 
    table.buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );
    } );
        </script>