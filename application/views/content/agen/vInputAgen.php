  <!-- Main content -->
    <section class="content">
      <?php if($this->session->flashdata('msg')):?>
        <div class="col-4">
          <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fa fa-check"></i> Success!</h5>
                <?php echo $this->session->flashdata('msg'); ?><a href="<?=base_url()?>/agen/list">Lihat</a>
          </div>
        </div>
      <?php endif; ?>

      <?php
      echo validation_errors();
      ?>
      
      <div class="container-fluid">
          <div class="row">
            <!-- left column -->
              <div class="col-md-12">
                <!-- general form elements -->
                  <div class="card card-primary">
                    <div class="card-header">
                      <h3 class="card-title">Input Data Agen</h3>
                        </div>
                        <!-- /.card-header -->
                          <!-- form start -->
                          <form role="form" id="form" action="<?=base_url();?>Agen/do_add_agen" method="post">
                            <div class="card-body">
                          <div class="row">
                            <div class="col-5">
                              <div class="form-group">
                                <label>Nama Agen</label>
                                <input type="text" name="nama_agen" class="form-control" placeholder="Nama lengkap" value="<?=set_value('nama_agen');?>">
                              </div>
                            </div>
                            
                            <div class="col-5">
                              <div class="form-group">
                                <label>Jenis Kelamin</label>
                                <select class="form-control" name="jenis_kelamin">
                                  <option value="" disabled selected>-- Jenis Kelamin --</option>
                                  <?php
                                  if (set_value('jenis_kelamin') != '') {
                                    echo " <option value='".set_value('jenis_kelamin')."' selected='selected'>".set_value('jenis_kelamin')."</option>";
                                  }
                                  ?>
                                  <option value="M">M</option>
                                  <option value="F">F</option>
                                </select>
                              </div>
                            </div> 
                            
                           
                            <div class="col-5">
                              <div class="form-group">
                                <label>No Telfon</label>
                                <input type="text" name="no_telp_agen" class="form-control" placeholder="Nomor Telefon" value="<?=set_value('no_telp_agen');?>">
                              </div>
                            </div>

                            <div class="col-5">
                              <div class="form-group">
                                <label>Alamat Lengkap</label>
                                <textarea class="form-control" name="alamat" rows="3" placeholder="Enter ..."><?=set_value('alamat');?></textarea>
                              </div>
                            </div>

                          </div>

                         </div>
                        <!-- /.card-body -->
                      <div class="card-footer">
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                  </form>
                </div>
                <!-- /.card -->   
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>