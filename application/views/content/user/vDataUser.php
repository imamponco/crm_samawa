<!-- Main content -->
    <section class="content">
       <?php if($this->session->flashdata('edit')):?>
        <div class="col-3">
          <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fa fa-check"></i> Success!</h5>
                <?php echo $this->session->flashdata('edit'); ?>
          </div>
        </div>
      <?php endif; ?>
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data User</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div id="example1_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                 <!-- filter -->
                <div class="row">
                 
                   <div class="col-sm-12 col-md-3">
                    <label>
                    <?php echo form_open('User/list');?>
                    <div class="form-group">
                      <label>Filter</label>
                        <select class="form-control" name="filter"> 
                        <option selected="selected" value="1">Belum diverifikasi</option>
                        <option value="0">Telah diverifikasi</option>
                       
                        </select>
                      </div>
                    </label>
                    </div>

                    <div class="col-sm-12 col-md-2">
                      <label>
                        Action

                        <!--  <a href="<?=base_url();?>Pemesanan/list"> -->
                            <button type="submit" class="btn btn-block btn-success">Go</button>
                        <!--  </a> -->
                       </label> 
                     <?php echo form_close();?>
                    </div>
                </div>
<!-- 
                <div class="row">
                <div class="col-sm-12 col-md-2"">
                <label>
                   <a href="<?=base_url();?>user/add_user">
                      <button type="button" class="btn btn-block btn-success">Add Data</button>
                   </a>
                  </div>
                </label>
                </div> -->

          </div>

       <div class="row">
          <div class="col-sm-12">
            <div style ="width:auto; height:auto; overflow-x:scroll">
              <table id="example" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <center>
                    <th>No</th>
                    <th>Username</th>
                    <th>Email</th>    
                    <th>Role</th>
                    <th>Action</th>
                  </center>
                </tr>
                </thead>
                <tbody>
                     <?php
                      $no = 1;
                      foreach ($user as $row) {
                      ?>

                      <tr>
                        <td><?=$no?></td>
                        <td><?=$row->username ?></td>
                        <td><?=$row->email?></td>
                        <td>
                        <?php
                          $role = substr($row->id_user,0,2);
                          switch ($role) {
                            case 'AO':
                              echo "Admin Operasional";
                              break;
                            case 'AF':
                              echo "Admin Finance";
                              break;
                            case 'AM':
                              echo "Admin Marketing";
                              break;
                            case 'AV':
                              echo "Admin Visa";
                              break;
                            case 'PD':
                              echo "President Director";
                              break;
                            case 'CO':
                              echo "Pelanggan";
                              break;
                            
                            default:
                              # code...
                              break;
                          }
                        ?>
                        </td>
                        <td>
                          <?php if($row->verified_at== null):?>
                          <a href="<?=base_url();?>User/verify/<?=$row->id_user?>">
                          <button type="button" class="btn btn-block btn-info" onclick="return confirm('Apakah anda yakin memverifikasi user ini?')">Verify</button>
                          </a><br>
                          <?php else: ?>
                          <a href="<?=base_url();?>User/edit_user/<?=$row->id_user?>">
                          <button type="button" class="btn btn-block btn-primary">Edit</button>
                          </a><br>
                          <a href="<?=base_url();?>User/delete_user/<?=$row->id_user?>" onclick="return confirm('Apakah anda yakin menghapus data ini?')">
                          <button type="button"  class="btn btn-block btn-danger">Delete</button>
                          <!-- </a> -->
                          <?php endif;?>
                        </td>
                        
                      </tr>
                      <?php
                        $no++;
                       }
                      ?>
                </tbody>

               </table>

              </div>
            <!-- scroll bar -->
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- warpper -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">
          $(document).ready(function() {
         var table = $('#example').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
    } );
 
    table.buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );
    } );
        </script>