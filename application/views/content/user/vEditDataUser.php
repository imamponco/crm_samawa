  <!-- Main content -->
    <section class="content">
      <?php
      if($this->session->flashdata('error')){?>
        <div class="col-4">
          <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fa fa-check"></i> Gagal!</h5>
                <?php echo $this->session->flashdata('error'); ?>
          </div>
        </div>
      <?php
      }else if($this->session->flashdata('edit')){
      ?>
      <div class="col-3">
          <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fa fa-check"></i> Success!</h5>
                <?php echo $this->session->flashdata('edit'); ?>
          </div>
        </div>
      <?php
        }
      ?>
        
     
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Data User</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
            <?php foreach ($user as $row) { ?>
              <form role="form" id="form" action="<?=base_url();?>user/do_edit_user/<?=$row->id_user?>" method="post">
                <div class="card-body">
              <div class="row">
                <div class="col-12">
                  <div class="form-group">
                    <label>Username</label>
                    <input type="text" name="username" class="form-control" placeholder="username" value="<?=$row->username?>">
                  </div>
                </div>
    
                <div class="col-6">
                  <div class="form-group">
                    <label>Password</label>
                    <input type="password" name="password" class="form-control" placeholder="Password" value="">
                  </div>
                </div>

                <div class="col-6">
                  <div class="form-group">
                    <label>Re-Password</label>
                    <input type="password" name="repassword" class="form-control" placeholder="Re-Password" value="">
                  </div>
                </div>


                <div class="col-12">
                  <div class="form-group">
                    <label>Email</label>
                    <input type="email" name="email" class="form-control" placeholder="Email" value="<?=$row->email?>">
                  </div>
                </div>
                    <?php
                    }
                  ?>

                </div>

            </div>
                <!-- /.card-body -->

                    <div class="card-footer">
                      <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                  </form>
                </div>
                <!-- /.card -->   
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>