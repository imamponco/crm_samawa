<?php
$year = date("Y");
$before = $year-1;

?>

<!-- Main content -->
    <section class="content">
       <?php if($this->session->flashdata('edit')):?>
        <div class="col-3">
          <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fa fa-check"></i> Success!</h5>
                <?php echo $this->session->flashdata('edit'); ?>
          </div>
        </div>
      <?php endif; ?>


 
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Laporan Keuangan</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div id="example1_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                 <!-- filter -->
                   <div class="row">
                    <div class="col-sm-12 col-md-2">
                    <label>
                       <?php echo form_open('Laporan/laporan_keuangan');?>
                       <div class="form-group">
                        <label>Filter</label>
                       <select class="form-control" name="filter">
                          <option value="0">Belum Lunas</option>
                          <option value="1">Lunas</option>
                        </select>
                      </div>
                    </label>
                    </div>

                    <div class="col-sm-12 col-md-2">
                    <label>
                       <div class="form-group">
                        <label>First</label>
                        <input type="date" name="first_date" class="form-control">
                      </div>
                    </label>
                    </div>

                    <div class="col-sm-12 col-md-2">
                    <label>
                       <div class="form-group">
                        <label>End</label>
                        <input type="date" name="end_date" class="form-control">
                      </div>
                    </label>
                    </div>

                    <div class="col-sm-12 col-md-2">
                      <label>
                        Action

                        <!--  <a href="<?=base_url();?>Pemesanan/list"> -->
                            <button type="submit" class="btn btn-block btn-success">Go</button>
                        <!--  </a> -->
                       </label> 
                     <?php echo form_close();?>
                    </div>
              </div>

       <div class="row">
          <div class="col-sm-12">
            <div style ="width:auto; height:auto; overflow-x:scroll">
              <table id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
                     <tr>
                            <th>No</th>
                            <th>Tanggal Transaksi</th>
                            <th>No Transaksi</th>
                            <th>Nama Lengkap</th>
                            <th>Subtotal</th>
                            <th>Diskon</th>
                            <th>Tax</th>
                            <th>Other</th>
                            <th>Grand Total</th>   
                    </tr>
                </thead>
                <tbody>
              <?php 
              $no = 1;
              $total = 0;
              foreach ($keuangan as $row) {
              ?>
              <tr>
                  <td><?=$no?></td>
                  <td><?=$row->tanggal_pesan?></td>
                  <td><?=$row->id_header_pemesanan?></td>
                  <td><?=$row->username ?></td>
                  <td><?=(!empty($row->subtotal) ? "Rp.".number_format($row->subtotal,0,',','.'): "Rp.".number_format(0,0,',','.'));?></td>
                  <td><?=(!empty($row->diskon) ? "Rp.".number_format($row->diskon,0,',','.'): "Rp.".number_format(0,0,',','.'));?></td>
                  <td><?=(!empty($row->tax) ? "Rp.".number_format($row->tax,0,',','.'): "Rp.".number_format(0,0,',','.'));?></td>
                  <td><?=(!empty($row->other) ? "Rp.".number_format($row->other,0,',','.'): "Rp.".number_format(0,0,',','.'));?></td>
                  <td><?=(!empty($row->grand_total) ? "Rp.".number_format($row->grand_total,0,',','.'): "Rp.".number_format(0,0,',','.'));?></td>
              </tr>
                
              <?php
                $total = $total + $row->grand_total;
                $no++;
              }
              ?>

              <?php if (!empty($total)):?>
              <tr>
                  <td><center><?php echo "Total";?></center></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td><?=(!empty($total) ? "Rp.".number_format($total,0,',','.'): "Rp.".number_format(0,0,',','.'));?></td>
              </tr>
              <?php endif ?> 
             </tbody> 
            </table>
            </div>
            <!-- scroll bar -->
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- warpper -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  

  <script type="text/javascript">
   $(document).ready(function() {
        var table = $('#example').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
    } );
 
    table.buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );
    } );
    </script>