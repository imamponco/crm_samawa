<!-- Main content -->
    <section class="content">
       <?php if($this->session->flashdata('edit')):?>
        <div class="col-3">
          <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fa fa-check"></i> Success!</h5>
                <?php echo $this->session->flashdata('edit'); ?>
          </div>
        </div>
      <?php endif; ?>
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Laporan Keberangkatan</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div id="example1_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                <!-- filter -->
                <div class="row">
                 
                   <div class="col-sm-12 col-md-3">
                    <label>
                    <?php echo form_open('Laporan/laporan_keberangkatan');?>
                    <div class="form-group">
                      <label>Filter</label>
                        <select class="form-control" name="filter">   
                        <option selected="selected" value="0">Belum</option>
                        <option value="1">Selesai</option>

                        </select>
                      </div>
                    </label>
                    </div>

                    <div class="col-sm-12 col-md-2">
                    <label>
                       <?php echo form_open('Laporan/laporan_pelanggan');?>
                       <div class="form-group">
                        <label>First</label>
                        <input type="date" name="first_date" class="form-control">
                      </div>
                    </label>
                    </div>

                    <div class="col-sm-12 col-md-2">
                    <label>
                       <div class="form-group">
                        <label>End</label>
                        <input type="date" name="end_date" class="form-control">
                      </div>
                    </label>
                    </div>

                    <div class="col-sm-12 col-md-2">
                      <label>
                        Action

                        <!--  <a href="<?=base_url();?>Pemesanan/list"> -->
                            <button type="submit" class="btn btn-block btn-success">Go</button>
                        <!--  </a> -->
                       </label> 
                     <?php echo form_close();?>
                    </div>
                </div>
          </div>

       <div class="row">
          <div class="col-sm-12">
            <div style ="width:auto; height:auto; overflow-x:scroll">
              <table id="example" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <center>
                    <th>No</th>
                    <th>Nama Paket</th>
                    <th>Nama Maskapai</th>
                    <th>No Penerbangan</th>    
                    <th>Tanggal Keberangkatan</th>
                    <th>Kota Asal</th>
                    <th>Kota Tujuan</th>
                    <th>Jam Terbang</th>
                    <th>Jam Tiba</th>
                    <th>Quota</th>
                    <th>Status</th>
                    <th>Action</th>
                  </center>
                </tr>
                </thead>
                <tbody>
                     <?php 
                      $no = 1;
                      foreach ($jadwal as $row) {

                      $this->db->select('count("id_jadwal") as jumlah');
                      $this->db->from('detail_pemesanan');
                      $this->db->join('paket', 'detail_pemesanan.id_paket = paket.id_paket');
                      $this->db->where('detail_pemesanan.id_jadwal = "'.$row->id_jadwal.'" AND detail_pemesanan.id_paket="'.$row->id_paket.'"');
                      $count = $this->db->get();
    
                      ?>

                      <tr>
                        <td><?=$no?></td>
                        <td><?=$row->nama_paket?></td>
                        <td><?=$row->nama_maskapai ?></td>
                        <td><?=$row->no_flight?></td>
                        <td><?=$row->tgl_keberangkatan?></td>
                        <td><?=$row->kota_asal?></td>
                        <td><?=$row->kota_tujuan?></td>
                        <td><?=$row->jam_terbang?></td>
                        <td><?=$row->jam_tiba?></td>
                        <?php
                        foreach ($count->result() as $i) { 
                        $jumlah = $row->kuota - $i->jumlah; 
                        ?>
                        <td><?php echo ($jumlah." / ".$row->kuota); ?></td>
                        <?php  
                          }
                         if($row->status == 1){
                           $status = "<font color=red>Telah Selesai</color>";
                         }else{
                           $status = "<font color=blue>Belum Selesai</color>";
                         }
                        ?>
                        <td><?=$status?></td>
                        <td>
                            <a href="<?=base_url();?>jadwal/manifest/vListManifest/<?=$row->id_jadwal?>/<?=$row->id_paket?>">
                                <button type="button" class="btn btn-block btn-info">Manifest</button>
                           </a></br>
                        </td>
                        
                      </tr>
                      <?php
                        $no++;
                       }
                      ?>
                </tbody>

               </table>

              </div>
            <!-- scroll bar -->
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- warpper -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">
          $(document).ready(function() {
         var table = $('#example').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
    } );
 
    table.buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );
    } );
        </script>