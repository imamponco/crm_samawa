<!-- Main content -->
<?php
  $tgl = @$_POST['year'];
  if(!empty($tgl)){
    $tahun = explode("-",$tgl);
    $year = $tahun[0];
    $before = $year-1;
  }else{
    $year = date("Y");
    $before = $year-1;
  }
  
  foreach ($agen as $row) {
   // for ($i=1; $i <= 12; $i++) {
      $query = $this->db->query("SELECT *,COUNT(p.id_agen) AS jumlah FROM pelanggan p
      LEFT JOIN USER u ON p.id_user = u.id_user
      LEFT JOIN agen a ON p.id_agen = a.id_agen
      WHERE YEAR(verified_at) = '".$year."'
      AND verified_at IS NOT NULL
      AND p.id_agen = '".$row->id_agen."'
      GROUP BY p.id_agen HAVING COUNT(*) >= 0
      ")->row();
      
      if (!empty($query)) {
        $data["$row->id_agen"]["id_agen"] = $query->id_agen;
        $data["$row->id_agen"]["nama_agen"] = $query->nama_agen;
        $data["$row->id_agen"]["jumlah"] = $query->jumlah;
      }else{
        $data["$row->id_agen"]["id_agen"] = $row->id_agen;
        $data["$row->id_agen"]["nama_agen"] = $row->nama_agen;
        $data["$row->id_agen"]["jumlah"] = 0;
      }
     
    // }
  } 
 
  ?>

    <section class="content">
       <?php if($this->session->flashdata('edit')):?>
        <div class="col-3">
          <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fa fa-check"></i> Success!</h5>
                <?php echo $this->session->flashdata('edit'); ?>
          </div>
        </div>
      <?php endif; ?>
    <div class="row">
      <!-- <div class="col-sm-12 col-md-6"> -->
       <!-- BAR CHART -->
        <!-- <div class="card card-success">
          <div class="card-header">
            <h3 class="card-title">Data Pelanggan</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
          </div>
          <div class="card-body">
            <div class="chart">
              <canvas id="barChart" style="height:230px"></canvas>
            </div>
          </div> -->
          <!-- /.card-body -->
        <!-- </div> -->
        <!-- /.card -->
     <!--  </div> -->
    <div class="col-sm-12 col-md-1"></div>
      <div class="col-sm-12 col-md-10">
        <!-- LINE CHART -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Data Pelanggan Tahun <?=date("Y")?> (Biru) & <?=date("Y")-1?> (Abu)</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
                <div class="chart">
                  <canvas id="lineChart" style="height:250px"></canvas>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
      </div>
    <div class="col-sm-12 col-md-1"></div>
    </div>

      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Laporan Pelanggan</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div id="example1_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                 <!-- filter -->
                 <div class="row">
                    <div class="col-sm-12 col-md-2">
                    <label>
                       <?php echo form_open('Laporan/laporan_pelanggan');?>
                       <div class="form-group">
                        <label>First</label>
                        <input type="date" name="first_date" class="form-control">
                      </div>
                    </label>
                    </div>

                    <div class="col-sm-12 col-md-2">
                    <label>
                       <div class="form-group">
                        <label>End</label>
                        <input type="date" name="end_date" class="form-control">
                      </div>
                    </label>
                    </div>

                    <div class="col-sm-12 col-md-2">
                      <label>
                        Action

                        <!--  <a href="<?=base_url();?>Pemesanan/list"> -->
                            <button type="submit" class="btn btn-block btn-success">Go</button>
                        <!--  </a> -->
                       </label> 
                     <?php echo form_close();?>
                    </div>
              </div>
      
       <div class="row">
          <div class="col-sm-12">
            <div style ="width:auto; height:auto; overflow-x:scroll">
              <table id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
                     <tr>
                            <th>No</th>
                            <th>Nama Agen</th>
                            <th>Nama Pelanggan</th>
                            <th>NIK</th>
                            <th>Jenis Kelamin</th>
                            <th>Tanggal Lahir</th>
                            <th>Tempat Lahir</th>
                            <th>No Telp</th>
                            <th>Alamat</th>     
                    </tr>
                </thead>
              <?php 
              $no = 1;
              foreach ($pelanggan as $row) {
              ?>
              <tbody>
                    <tr>
                        <td><?=$no?></td>
                        <td><?=$row->nama_agen ?></td>
                        <td><?=$row->username ?></td>
                        <td><?=$row->nik ?></td>
                        <td><?=$row->jenis_kelamin ?></td>
                        <td><?=$row->tanggal_lahir ?></td>
                        <td><?=$row->tempat_lahir ?></td>
                        <td><?=$row->no_telp ?></td>
                        <td><?=$row->alamat ?></td>  
                    </tr>
                  
              <?php
               $no++;
              }
              ?> 
             </tbody> 
            </table>
            </div>
              <!-- scroll bar -->
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- warpper -->
    </section>
    <!-- /.content -->

    <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Laporan Kinerja Agen Tahun <?=$year?></h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div id="example1_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                 <!-- filter -->
                 <div class="row">
                    <div class="col-sm-12 col-md-2">
                    <label>
                      <form action="#" method="post">
                       <div class="form-group">
                        <label>Tahun</label>
                        <input type="date" name="year" id="datepicker" class="form-control">
                      </div>
                    </label>
                    </div>

                    <div class="col-sm-12 col-md-2">
                      <label>
                        Filter

                        <!--  <a href="<?=base_url();?>Pemesanan/list"> -->
                            <button type="submit" class="btn btn-block btn-success">Go</button>
                        <!--  </a> -->
                       </label> 
                     </form>
                    </div>
              </div>
                 
       <div class="row">
          <div class="col-sm-12">
            <div style ="width:auto; height:auto; overflow-x:scroll">
              <table id="agen" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <th>No</th>
                    <th>Agen</th>
                    <th>Jumlah Pelanggan</th>
                </thead>
                
                <tbody>   
                <?php
                
                $no = 1;
                foreach ($agen as $ag) {
                ?>
                <tr>
                    <td><?=$no?></td>
                    <td><?=$data[$ag->id_agen]["nama_agen"] ?></td>
                    <td><?=$data[$ag->id_agen]["jumlah"] ?></td>
                </tr>
              <?php
              $no++;
                }
              ?>
                </tbody> 
            </table>
                </div>
              <!-- scroll bar -->
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- warpper -->
    </div>


  </div>
  <!-- /.content-wrapper -->


        
    


  <?php
    for ($a=1; $a <= 2 ; $a++) { 
     for ($i=1; $i <= 12; $i++) {
        if ($a==1) {
          $this->db->select("COUNT(id_user) AS jumlah");
          $this->db->from('user');
          $this->db->where('MONTH(verified_at) = '.$i.' AND YEAR(verified_at) = '.$year.'');
          $this->db->where('verified_at is NOT NULL', NULL, FALSE);
          $this->db->like('user.id_user', 'CO');
          $query = $this->db->get()->row();
          $data["co"][$i] = $query;
        }else{
          $this->db->select("COUNT(id_user) AS jumlah");
          $this->db->from('user');
          $this->db->where('MONTH(verified_at) = '.$i.' AND YEAR(verified_at) = '.$before.'');
          $this->db->where('verified_at is NOT NULL', NULL, FALSE);
          $this->db->like('user.id_user', 'CO');
          $query = $this->db->get()->row();
          $data2["co"][$i] = $query;
        }
      }
    } 

    
  ?>


  <script type="text/javascript">

    $(document).ready(function() {
		    var table = $('#agen').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
    } );
 
    table.buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );
		} );

    $(document).ready(function() {
        var table = $('#example').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
    } );
 
    table.buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );
    } );

    $(function () {

    //--------------
    //- AREA CHART -
    //--------------

    // Get context with jQuery - using jQuery's .get() method.
    var areaChartCanvas = $('#lineChart').get(0).getContext('2d')
    // This will get the first returned node in the jQuery collection.
    var areaChart       = new Chart(areaChartCanvas)

    var areaChartData = {
      labels  : ['January', 'February', 'March', 'April', 'May', 'June', 'July','August','September','October','November','December'],
      datasets: [
        {
          label               : '<?=$before?>',
          fillColor           : 'rgba(210, 214, 222, 1)',
          strokeColor         : 'rgba(210, 214, 222, 1)',
          pointColor          : 'rgba(210, 214, 222, 1)',
          pointStrokeColor    : '#c1c7d1',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(220,220,220,1)',
          data                : [
                                <?php
                                for ($i=1; $i <=12 ; $i++) { 
                                  if (!empty($data2["co"][$i]->jumlah)) {
                                  echo $data2["co"][$i]->jumlah.",";
                                  }else{
                                    echo "0 ,";
                                  }
                                }
                                ?>
                                ]
        },
        {
          label               : '<?=$year?>',
          fillColor           : 'rgba(60,141,188,0.9)',
          strokeColor         : 'rgba(60,141,188,0.8)',
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : [
                              <?php
                                for ($i=1; $i <=12 ; $i++) { 
                                  if (!empty($data["co"][$i]->jumlah)) {
                                  echo $data["co"][$i]->jumlah.",";
                                  }else{
                                    echo "0 ,";
                                  }
                                }
                              ?>
                              ]
        }
      ]

    }
    console.log(areaChartData.datasets);

    var areaChartOptions = {
      //Boolean - If we should show the scale at all
      showScale               : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : false,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - Whether the line is curved between points
      bezierCurve             : true,
      //Number - Tension of the bezier curve between points
      bezierCurveTension      : 0.3,
      //Boolean - Whether to show a dot for each point
      pointDot                : false,
      //Number - Radius of each point dot in pixels
      pointDotRadius          : 4,
      //Number - Pixel width of point dot stroke
      pointDotStrokeWidth     : 1,
      //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
      pointHitDetectionRadius : 20,
      //Boolean - Whether to show a stroke for datasets
      datasetStroke           : true,
      //Number - Pixel width of dataset stroke
      datasetStrokeWidth      : 2,
      //Boolean - Whether to fill the dataset with a color
      datasetFill             : true,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].lineColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio     : true,
      //Boolean - whether to make the chart responsive to window resizing
      responsive              : true
    }

    //-------------
    //- LINE CHART -
    //--------------
    var lineChartCanvas          = $('#lineChart').get(0).getContext('2d')
    var lineChart                = new Chart(lineChartCanvas)
    var lineChartOptions         = areaChartOptions
    lineChartOptions.datasetFill = false
    lineChart.Line(areaChartData, lineChartOptions)

    //-------------
    //- BAR CHART -
    //-------------
    var barChartCanvas                   = $('#barChart').get(0).getContext('2d')
    var barChart                         = new Chart(barChartCanvas)
    var barChartData                     = areaChartData
    barChartData.datasets[1].fillColor   = '#00a65a'
    barChartData.datasets[1].strokeColor = '#00a65a'
    barChartData.datasets[1].pointColor  = '#00a65a'
    var barChartOptions                  = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero        : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - If there is a stroke on each bar
      barShowStroke           : true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth          : 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing         : 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing       : 1,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to make the chart responsive
      responsive              : true,
      maintainAspectRatio     : true
    }

    barChartOptions.datasetFill = false
    barChart.Bar(barChartData, barChartOptions)
  })
    </script>