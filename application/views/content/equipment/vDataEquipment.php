<!-- Main content -->
    <section class="content">
       <?php if($this->session->flashdata('edit')):?>
        <div class="col-3">
          <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fa fa-check"></i> Success!</h5>
                <?php echo $this->session->flashdata('edit'); ?>
          </div>
        </div>
      <?php endif; ?>
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Equipment</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div id="example1_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                <div class="row">

                <div class="col-sm-12 col-md-2"">
                <label>
                   <a href="<?=base_url();?>equipment/add_equipment">
                      <button type="button" class="btn btn-block btn-success">Add Data</button>
                   </a>
                  </div>
                </label>
                </div>

          </div>

       <div class="row">
          <div class="col-sm-12">
            <div style ="width:auto; height:auto; overflow-x:scroll">
              <table id="example" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <center>
                    <th>No</th>
                    <th>Nama Barang</th>
                    <th>Stok</th>    
                    <th>Action</th>
                  </center>
                </tr>
                </thead>
                <tbody>
                     <?php 
                      $no = 1;
                      foreach ($equipment as $row) {
                      ?>

                      <tr>
                        <td><?=$no?></td>
                        <td><?=$row->nama_barang ?></td>
                        <td><?=$row->stok ?></td>
                        <td>
                          <a href="<?=base_url();?>equipment/detail/<?=$row->id_barang?>">
                          <button type="button" class="btn btn-block btn-info">Detail</button>
                          </a><br>
                          <a href="<?=base_url();?>equipment/edit_equipment/<?=$row->id_barang?>">
                          <button type="button" class="btn btn-block btn-primary">Edit</button>
                          </a><br>
                          <a href="<?=base_url();?>equipment/delete_equipment/<?=$row->id_barang?>" onclick="return confirm('Apakah anda yakin menghapus data ini?')">
                          <button type="button"  class="btn btn-block btn-danger">Delete</button>
                          <!-- </a> -->
                        </td>
                        
                      </tr>
                      <?php
                        $no++;
                       }
                      ?>
                </tbody>

               </table>

              </div>
            <!-- scroll bar -->
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- warpper -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">
          $(document).ready(function() {
         var table = $('#example').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
    } );
 
    table.buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );
    } );
        </script>