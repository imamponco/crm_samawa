  <!-- Main content -->
    <section class="content">
      <?php if($this->session->flashdata('msg')):?>
        <div class="col-4">
          <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fa fa-check"></i> Success!</h5>
                <?php echo $this->session->flashdata('msg'); ?>
          </div>
        </div>
      <?php endif; ?>
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Data Equipment</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
            <?php foreach ($equipment as $row) { ?>
              <form role="form" id="form" action="<?=base_url();?>equipment/do_edit_equipment/<?=$row->id_barang?>" method="post">
                <div class="card-body">
              <div class="row">
                <div class="col-5">
                  <div class="form-group">
                    <label>Nama Barang</label>
                    <input type="text" name="nama_equipment" class="form-control" placeholder="Nama Barang" value="<?=$row->nama_barang?>">
                  </div>
                </div>
                
    
                <div class="col-5">
                  <div class="form-group">
                    <label>Stok</label>
                    <input type="text" name="stok" class="form-control" placeholder="Stok" value="<?=$row->stok?>">
                  </div>
                </div>

                    <?php
                    }
                  ?>

                </div>

            </div>
                <!-- /.card-body -->

                    <div class="card-footer">
                      <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                  </form>
                </div>
                <!-- /.card -->   
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>