  <!-- Main content -->
    <section class="content">
      <?php if($this->session->flashdata('msg')):?>
        <div class="col-4">
          <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fa fa-check"></i> Success!</h5>
                <?php echo $this->session->flashdata('msg'); ?>
          </div>
        </div>
      <?php endif; ?>

      <?php
      if (isset($error)) {
        echo $error;
      }
      echo validation_errors();
      ?>

      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Data Jadwal</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
            <?php foreach ($jadwal as $row) { ?>
              <form role="form" id="form" action="<?=base_url();?>jadwal/do_edit_jadwal/<?=$row->id_jadwal?>/<?=$row->id_paket?>" method="post">
            <div class="card-body">
              <div class="row">

                <div class="col-5">
                     <div class="form-group">
                          <label>Nama Paket</label>
                          <select class="form-control" name="id_paket" required="required">
                            <option value="<?=$row->id_paket?>" selected="selected" readonly="readonly"><?=$row->nama_paket?></option>
                            ?>
                            <?php foreach ($paket as $baris) {?>
                            <option value="<?=$baris->id_paket?>"><?=$baris->nama_paket?></option>
                            <?php
                              }
                            ?>
                          </select>
                       </div>
                    </div>

               <div class="col-5">
                  <div class="form-group">
                    <label>Nama Maskapai</label>
                     <input type="text" name="nama_maskapai" class="form-control" placeholder="Nama Maskapai" value="<?=$row->nama_maskapai?>">
                    
                    </select>
                  </div>
                </div>

                <div class="col-5">
                      <div class="form-group">
                        <label>Kuota</label>
                        <input type="text" name="kuota" class="form-control" placeholder="Kuota" value="<?=$row->kuota;?>">
                      </div>
                  </div>
    

                <div class="col-5">
                  <div class="form-group">
                    <label>Nomor Penerbangan</label>
                    <input type="text" name="no_flight" class="form-control" placeholder="Nomor Penerbangan" value="<?=$row->no_flight?>">
                  </div>
                </div>

                <div class="col-5">
                      <div class="form-group">
                        <label>Kota Asal</label>
                        <input type="text" name="kota_asal" class="form-control" placeholder="Kota Asal" value="<?=$row->kota_asal?>">
                      </div>
                    </div>

                    <div class="col-5">
                      <div class="form-group">
                        <label>Jam Keberangkatan</label>
                        <input type="time" name="jam_terbang" class="form-control" placeholder="Jam Keberangkatan" value="<?=$row->jam_terbang?>">
                      </div>
                    </div>

                    <div class="col-5">
                      <div class="form-group">
                        <label>Kota Tujuan</label>
                        <input type="text" name="kota_tujuan" class="form-control" placeholder="Kota Tujuan" value="<?=$row->kota_tujuan?>">
                      </div>
                    </div>

                    <div class="col-5">
                      <div class="form-group">
                        <label>Jam Tiba</label>
                        <input type="time" name="jam_tiba" class="form-control" placeholder="Jam Tiba" value="<?=$row->jam_tiba?>">
                      </div>
                    </div>

                    <div class="col-5">
                      <div class="form-group">
                        <label>Tanggal Keberangkatan</label>
                        <input type="date" name="tgl_keberangkatan" class="form-control" value="<?=$row->tgl_keberangkatan?>">
                      </div>
                    </div>

                    <div class="col-5">
                      <div class="form-group">
                        <label>Status</label> 
                         <select class="form-control" name="status" required="required">
                            <option value="<?=$row->status?>" selected="selected" readonly="readonly">
                              <?php
                                if($row->status == 0){
                                  echo "Belum Selesai";
                                }else{
                                  echo "Selesai";
                                }
                              ?>
                            </option>
                            <option value="0">Belum Selesai</option>
                            <option value="1">Selesai</option>
                            ?>
                          </select>
                      </div>
                    </div>

                    <?php
                    }
                  ?>

                </div>

            </div>
                <!-- /.card-body -->

                    <div class="card-footer">
                      <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                  </form>
                </div>
                <!-- /.card -->   
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>