<!-- Main content -->

    <section class="content">
       <?php if($this->session->flashdata('edit')):?>
        <div class="col-3">
          <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fa fa-check"></i> Success!</h5>
                <?php echo $this->session->flashdata('edit'); ?>
          </div>
        </div>
      <?php endif; ?>
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Manifest</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div id="example1_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                <div class="row">
                <div class="col-sm-12 col-md-6">
               <!--  <label>
                   <a href="<?=base_url();?>Pemesanan/add_pemesanan">
                      <button type="button" class="btn btn-block btn-success">Add Data</button>
                   </a>
                 
                </label> -->
                 </div>
                </div>

       <div class="row">
          <div class="col-sm-12">
            <div style ="width:auto; height:auto; overflow-x:scroll">
              <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Lengkap</th>
                <th>NIK</th>
                <th>Jenis Kelamin</th>
                <th>Tanggal Lahir</th>
                <th>Tempat Lahir</th>
                <th>No Telp</th>
                <th>Alamat</th>
               <!--  <th>Action</th> -->
                   
            </tr>
        </thead>
      <?php 
      $no = 1;
      foreach ($customer as $row) {
       
      ?>
        <tbody>
            <tr>

                <td><?=$no?></td>
                <td><?=$row->username ?></td>
                <td><?=$row->nik ?></td>
                <td><?=$row->jenis_kelamin ?></td>
                <td><?=$row->tanggal_lahir ?></td>
                <td><?=$row->tempat_lahir ?></td>
                <td><?=$row->no_telp ?></td>
                <td><?=$row->alamat ?></td>
                <!-- <td>
                  <a href="<?=base_url();?>Customer/edit_pelanggan/<?=$row->nik?>">
                      <button type="button" class="btn btn-block btn-primary">Edit</button>
                  </a></br>
                  <a href="<?=base_url();?>Pemesanan/delete_pemesanan/<?=$row->id_detail_pemesanan?>" onclick="return confirm('Apakah anda yakin menghapus data ini?')">
                       <button type="button"  class="btn btn-block btn-danger">Delete</button>
                  </a>
                </td> -->
               
            </tr>
                    </tbody>
      <?php
      $no++;
      }
      ?>  
    </table>
              </div>
            <!-- scroll bar -->
           </div>
          <!-- /.row -->
        </div>
        <!-- /.row -->
      </div>
      <!-- warpper -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->


        <script type="text/javascript">
        	$(document).ready(function() {
		     var table = $('#example').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
    } );
 
    table.buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );
		} );
        </script>

        
 