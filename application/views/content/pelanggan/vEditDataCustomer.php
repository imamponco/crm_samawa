  <!-- Main content -->
    <section class="content">
      <?php if($this->session->flashdata('msg')):?>
        <div class="col-4">
          <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fa fa-check"></i> Success!</h5>
                <?php echo $this->session->flashdata('msg'); ?><a href="<?=base_url()?>/customer/list">Lihat</a>
          </div>
        </div>
      <?php endif; ?>
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Data Pelanggan</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
            <?php foreach ($customer as $row) { ?>
              <form role="form" id="form" action="<?=base_url();?>Customer/do_edit_customer/<?=$row->nik?>/<?=$row->id_user?>" method="post">
                <div class="card-body">
              <div class="row">
                <div class="col-5">
                  <div class="form-group">
                    <label>Nama Lengkap</label>
                    <input type="text" name="username" class="form-control" placeholder="Nama lengkap" value="<?=$row->username?>">
                  </div>
                </div>
                
                <div class="col-5">
                  <div class="form-group">
                    <label>Jenis Kelamin</label>
                    <select class="form-control" name="jenis_kelamin">
                      <option value="<?=$row->jenis_kelamin?>" selected="selected"><?=$row->jenis_kelamin?></option>
                      <option value="M">M</option>
                      <option value="F">F</option>
                    </select>
                  </div>
                </div> 
                
                <div class="col-5">
                  <div class="form-group">
                    <label>Tanggal Lahir</label>
                    <input type="date" name="tgl_lahir" class="form-control" value="<?=$row->tanggal_lahir?>">
                  </div>
                </div>

                <div class="col-5">
                  <div class="form-group">
                    <label>Tempat Lahir</label>
                    <input type="text" name="tempat_lahir" class="form-control" placeholder="Tempat Lahir" value="<?=$row->tempat_lahir?>">
                  </div>
                </div>

               <!--  <div class="col-5">
                  <div class="form-group">
                    <label>Nomor Passport</label>
                    <input type="text" name="no_passport" class="form-control" placeholder="Nomor Passport" value="<?=$row->no_passport?>">
                  </div>
                </div>

                <div class="col-5">
                  <div class="form-group">
                    <label>Issuing Office</label>
                    <input type="text" name="issuing_office" class="form-control" placeholder="Issuing_office" value="<?=$row->issuing_office?>">
                  </div>
                </div>

                <div class="col-5">
                  <div class="form-group">
                    <label>Date Of Issue</label>
                    <input type="date" name="date_of_issue" class="form-control" value="<?=$row->date_of_issue?>">
                  </div>
                </div>

                <div class="col-5">
                  <div class="form-group">
                    <label>Date Of Expiry</label>
                    <input type="date" name="date_of_expiry" class="form-control" value="<?=$row->date_of_expiry?>">
                  </div>
                </div> -->

                <!-- <div class="col-5">
                  <div class="form-group">
                    <label>Relationship</label>
                    <select class="form-control" name="relationship">
                      <option value="<?=$row->relationship?>" selected="selected"><?=$row->relationship?></option>
                      <option value="Husband">Husband</option>
                      <option value="Wife">Wife</option>
                    </select>
                  </div>
                </div> -->

                <div class="col-5">
                        <div class="form-group">
                          <label>Relationship</label>
                          <select class="form-control" name="relationship">
                            <option value="<?=$row->relationship?>" selected="selected"><?=$row->relationship?></option>
                            <option value="Menikah">Menikah</option>
                            <option value="Belum Menikah">Belum Menikah</option>
                          </select>
                        </div>
                      </div> 

                <div class="col-5">
                  <div class="form-group">
                    <label>Nomor Passport</label>
                    <input type="text" name="no_passport" class="form-control" placeholder="Nomor Passport" value="<?=$row->no_passport?>">
                  </div>
                </div>

                <div class="col-5">
                  <div class="form-group">
                    <label>No Telfon</label>
                    <input type="text" name="no_telp" class="form-control" placeholder="Nomor Telefon" value="<?=$row->no_telp?>">
                  </div>
                </div>

                <div class="col-5">
                  <div class="form-group">
                    <label>Alamat Lengkap</label>
                    <textarea class="form-control" name="alamat" rows="3" placeholder="Enter ..."><?=$row->alamat?></textarea>
                  </div>
                </div>

                <div class="col-5">
                  <div class="form-group">
                    <label for="exampleInputPassword1">NIK</label>
                    <input type="text" name="nik" class="form-control" placeholder="NIK" value="<?=$row->nik?>">
                  </div>
                </div>

              <!--   <div class="col-5">
                  <div class="form-group">
                    <label for="exampleInputPassword1">Room Type</label>
                    <input type="text" name="room_type" class="form-control" placeholder="Room Type" value="<?=$row->room_type?>">
                  </div>
                </div> -->

                <div class="col-5">
                  <div class="form-group">
                    <label>Agen</label>
                    <select class="form-control" name="id_agen" value="1">
                      <option selected="selected" value="<?=$row->id_agen?>"><?=$row->nama_agen?></option>
                      <?php foreach ($agen as $baris) {?>
                      <option value="<?=$row->id_agen?>"><?=$baris->nama_agen ?></option>
                      <?php
                        }
                      ?>
                    </select>
                    <?php
                      }
                    ?>
                  </div>
                </div>

                </div>

            </div>
                <!-- /.card-body -->

                    <div class="card-footer">
                      <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                  </form>
                </div>
                <!-- /.card -->   
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>