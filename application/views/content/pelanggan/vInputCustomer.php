  <!-- Main content -->
    <section class="content">
      <?php if($this->session->flashdata('msg')):?>
        <div class="col-4">
          <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fa fa-check"></i> Success!</h5>
                <?php echo $this->session->flashdata('msg'); ?><a href="<?=base_url()?>/customer/list">Lihat</a>
          </div>
        </div>
      <?php endif; ?>

      <?php
      echo validation_errors();
      ?>
      
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Input Data Pelanggan</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="form" action="<?=base_url();?>Customer/do_add_customer" method="post">
                <div class="card-body">
              <div class="row">
               <div class="col-12">
                  <div class="form-group">
                    <label>User</label>
                    <select class="form-control" name="id_user">
                      <option value="" disabled selected>-- Pilih User --</option>
                      <?php foreach ($user as $usr):?>
                      <option value="<?=$usr->id_user?>"><?=$usr->username?></option>
                      <?php endforeach;?>
                    </select>
                  </div>
                </div> 
                
                <div class="col-6">
                  <div class="form-group">
                    <label for="exampleInputPassword1">NIK</label>
                    <input type="text" name="nik" class="form-control" placeholder="NIK" value="<?=set_value('nik');?>">
                  </div>
                </div> 
                
                <div class="col-6">
                  <div class="form-group">
                    <label>Jenis Kelamin</label>
                    <select class="form-control" name="jenis_kelamin">
                      <option value="" disabled selected>-- Jenis Kelamin --</option>
                      <?php
                      if (set_value('jenis_kelamin') != '') {
                        echo " <option value='".set_value('jenis_kelamin')."' selected='selected'>".set_value('jenis_kelamin')."</option>";
                      }
                      ?>
                      <option value="M">M</option>
                      <option value="F">F</option>
                    </select>
                  </div>
                </div> 
                
                <div class="col-6">
                  <div class="form-group">
                    <label>Tanggal Lahir</label>
                    <input type="date" name="tgl_lahir" class="form-control" value="<?=set_value('tgl_lahir');?>">
                  </div>
                </div>

                <div class="col-6">
                  <div class="form-group">
                    <label>Tempat Lahir</label>
                    <input type="text" name="tempat_lahir" class="form-control" placeholder="Tempat Lahir" value="<?=set_value('tempat_lahir');?>">
                  </div>
                </div>

                <div class="col-6">
                  <div class="form-group">
                    <label>No Telfon</label>
                    <input type="text" name="no_telp" class="form-control" placeholder="Nomor Telefon" value="<?=set_value('no_telp');?>">
                  </div>
                </div>

                <div class="col-6">
                      <div class="form-group">
                        <label>Relationship</label>
                        <select class="form-control" name="relationship">
                          <option value="<?=set_value('relationship');?>" selected="selected" disabled=""><?=set_value('relationship');?></option>
                          <option value="Menikah">Menikah</option>
                          <option value="Belum Menikah">Belum Menikah</option>
                        </select>
                      </div>
                    </div> 

                <div class="col-6">
                  <div class="form-group">
                    <label>Nomor Passport</label>
                    <input type="text" name="no_passport" class="form-control" placeholder="Nomor Passport" value="<?=set_value('alamat');?>">
                  </div>
                </div>

                <div class="col-6">
                  <div class="form-group">
                    <label>Alamat Lengkap</label>
                    <textarea class="form-control" name="alamat" rows="3" placeholder="Enter ..."><?=set_value('alamat');?></textarea>
                  </div>
                </div>

              

                <div class="col-12">
                  <div class="form-group">
                    <label>Agen</label>
                    <select class="form-control" name="id_agen" required="required">
                      <option disabled selected value="">-- Agen --</option>
                      <!--  <option value="AGM01">Gaido Travel And Tour</option> -->
                      ?>
                      <?php foreach ($agen as $row) {?>
                      <option value="<?=$row->id_agen?>"><?=$row->nama_agen ?></option>
                      <?php
                        }
                      ?>
                    </select>
                  </div>
                </div>

                </div>

            </div>
                <!-- /.card-body -->

                    <div class="card-footer">
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                  </form>
                </div>
                <!-- /.card -->   
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

      <script type="text/javascript">
      $(document).ready(function(){
        $("form").button(function(){
            alert("test");   
        });

      </script>