<!-- Main content -->
    <section class="content">
       <?php if($this->session->flashdata('edit')):?>
        <div class="col-3">
          <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fa fa-check"></i> Success!</h5>
                <?php echo $this->session->flashdata('edit'); ?>
          </div>
        </div>
      <?php endif; ?>
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Kritik dan Saran Pelanggan</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div id="example1_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
               <!--  <div class="row">

                <div class="col-sm-12 col-md-2"">
                <label>
                   <a href="<?=base_url();?>Kritik/add_kritik">
                      <button type="button" class="btn btn-block btn-success">Add Data</button>
                   </a>
                  </div>
                </label>
                </div> -->
                </div>

       <div class="row">
          <div class="col-sm-12">
            <div style ="width:auto; height:auto; overflow-x:scroll">
              <table id="example" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <center>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Kritik dan Saran</th>
                    <th>Action</th>
                  </center>
                </tr>
                </thead>
                <tbody>
                     <?php 
                      $no = 1;
                      foreach ($kritik as $row) {
                      ?>

                      <tr>
                        <td><?=$no?></td>
                        <td><?=$row->username ?></td>
                        <td><?=$row->message?></td>
                        <td>
                          <a href="<?=base_url();?>Kritik/add_kritik/<?=$row->jenis_content?>">
                          <button type="button" class="btn btn-block btn-primary">Reply</button>
                          </a><br>
                          <a href="<?=base_url();?>Kritik/delete_kritik/<?=$row->jenis_content?>" onclick="return confirm('Apakah anda yakin menghapus data ini?')">
                          <button type="button"  class="btn btn-block btn-danger">Delete</button>
                          <!-- </a> -->
                        </td>
                        
                      </tr>
                      <?php
                        $no++;
                       }
                      ?>
                </tbody>

               </table>

              </div>
            <!-- scroll bar -->
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- warpper -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">
          $(document).ready(function() {
         var table = $('#example').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
    } );
 
    table.buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );
    } );
        </script>