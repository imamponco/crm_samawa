  <!-- Main content -->
    <section class="content">
      <?php if($this->session->flashdata('msg')):?>
        <div class="col-4">
          <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fa fa-check"></i> Success!</h5>
                <?php echo $this->session->flashdata('msg'); ?>
          </div>
        </div>
      <?php endif; ?>

      <?php
      echo validation_errors();
      ?>

        <div class="container-fluid">
          <div class="row">
            <!-- left column -->
              <div class="col-md-12">
                <!-- general form elements -->
                  <div class="card card-primary">
                    <div class="card-header">
                      <h3 class="card-title">Kritik dan Saran</h3>
                        </div>
                        <!-- /.card-header -->
                          <!-- form start -->
                            <div class="card-body">
                              <table class="table table-bordered table-striped">
                                <thead>
                                  <tr>
                                    <center>
                                      <th>No</th>
                                      <th>Nama</th>
                                      <th>Kritik dan Saran</th>
                                      <th>Tanggal Balasan</th>
                                      <th>Action</th>
                                    </center>
                                  </tr>
                                </thead>
                                 <tbody>
                                 <?php 
                                  $no = 1;
                                  foreach ($kritik as $row) :
                                  ?>
                                  <tr>
                                    <td><?=$no?></td>
                                    <td><?=$row->username ?></td>
                                    <td><?=$row->message?></td>
                                    <td><?=$row->created_ct_at?></td>
                                    <td> <a href="<?=base_url();?>Kritik/delete_balasan/<?=$row->id_content?>/<?=$row->jenis_content?>" onclick="return confirm('Apakah anda yakin menghapus data ini?')">
                                    <button type="button"  class="btn btn-block btn-danger">Delete</button>
                                    </td>
                                  </tr>
                                </tbody>
                                <?php
                                $no++;
                                endforeach;
                                ?>
                              </table>
                            </div>
                         </div>
                        <!-- /.card-body -->
                      <div class="card-footer">

                  </div>  
                </div>
                <!-- /.card -->   
              </div>
              <!-- /.card-body -->
            </div>
      
      <div class="container-fluid">
          <div class="row">
            <!-- left column -->
              <div class="col-md-12">
                <!-- general form elements -->
                  <div class="card card-primary">
                    <div class="card-header">
                      <h3 class="card-title">Balas Kritik dan Saran</h3>
                        </div>
                        <!-- /.card-header -->
                          <!-- form start -->
                          <?php $jenis_content = $this->uri->segment(3);?>
                          <form role="form" id="form" action="<?=base_url();?>Kritik/do_add_balas/<?=$jenis_content?>" method="post">
                          <div class="card-body">
                          <div class="row">
                            <?php foreach ($kritik as $jk) :?>
                            <input type="hidden" name="jenis_content" value="<?=$jk->jenis_content?>">
                            <?php endforeach;?>
                            <div class="col-12">
                              <div class="form-group">
                                <label>Pesan</label>
                                <textarea class="form-control" name="message" rows="3" placeholder="Enter ..."><?=set_value('message');?></textarea>
                              </div>
                            </div>
                            
                            
                            
                          </div>
                         </div>
                        <!-- /.card-body -->
                      <div class="card-footer">
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                  </form>
                </div>
                <!-- /.card -->   
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>