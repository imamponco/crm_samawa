    <section class="content">
      <?php if($this->session->flashdata('edit')):?>
        <div class="col-4">
          <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fa fa-check"></i> Success!</h5>
                <?php echo $this->session->flashdata('edit'); ?>
          </div>
        </div>
      <?php endif; ?>
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            

             <?php
              echo validation_errors();
              $id = $this->uri->segment(3);
              ?>

            <!-- Main content -->
            <div class="invoice p-3 mb-3">
              <!-- title row -->
              <div class="row">
                <div class="col-12">
                  <h4>
                    <i class="fa fa-globe"></i> Gaido Travel And Tour.
                    <small class="float-right">Date: <?=date('Y-m-d');?></small>
                  </h4>
                </div>
                <!-- /.col -->
              </div>
              <!-- info row -->
              <form role="form" id="form" action="<?=base_url();?>Pemesanan/do_edit_pemesanan/<?=$id?>" method="post">
                <input type="hidden" name="id_user" value="<?=$this->session->userdata('id_user')?>">
                 <input type="hidden" name="status" value="Belum Lunas">
                <?php foreach ($detail_pemesanan as $tr) {?>
                <input type="hidden" value="<?=$tr->id_detail_pemesanan?>" name="id_transaksi[]"></td>
                <?php 
                 }
                ?>
                  <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">
                      <address>
                      <div class="row">
                        <div class="col-5">
                          <div class="form-group">
                            <label>Pelanggan</label>
                            <select class="form-control" name="nik" required="required" readonly>
                              <?php foreach ($header_pemesanan as $row) {?>                         
                              <option selected="selected"  readonly="readonly" value="<?=$row->nik?>"><?=$row->username?></option>
                              <?php
                                 }
                              ?>
                              <?php foreach ($pelanggan as $row) {?>                         
                              <option value="<?=$row->nik?>"><?=$row->username?></option>
                              <?php
                                 }
                              ?>
                            </select>
                          </div>
                        </div>

                       

                        <!-- <div class="col-3">
                          <div class="form-group">
                            <label>Add</label>
                             <a href="<?=base_url();?>Customer/add_customer">
                            <button type="submit" class="btn btn-block btn-success">+</button>
                            </a>
                          </div>
                        </div> -->
                      </div>
                      </address>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                      <address>
                      <div class="row">
                        <div class="col-5">
                          <div class="form-group">
                            <label>Tanggal Pemesanan</label>
                             <?php foreach ($header_pemesanan as $date) {
                              ?>
                            <input type="date" name="tgl_pesan" id="date" value="<?=$date->tanggal_pesan?>" >
                            <?php
                             } 
                            ?>
                          </div>
                        </div>
                        <div class="col-3">
                          
                        </div>
                      </div>
                      </address>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                     <!--  <b>Invoice #007612</b><br>
                      <br>
                      <b>Order ID:</b> 4F3S8J<br>
                      <b>Payment Due:</b> 2/22/2014<br>
                      <b>Account:</b> 968-34567 -->
                    </div>
                    <!-- /.col -->
                  </div>
                  <!-- /.row -->

                  <!-- Table row -->
                  <div class="row">
                    <div class="col-12 table-responsive">
                      <table class="table table-striped" id="myTable">
                        <thead>
                        <tr>
                          <th>No</th>
                          <th>Paket</th>
                          <th>Jadwal Keberangkatan</th>
                          <th>Jumlah</th>
                          <th>Harga</th>
                          <th>Total</th>
                          <th>Action</th>
                          
                        </tr>
                        </thead>
                        <tbody>
                        
                        <tr>
                          <?php 
                          $no = 1;
                          foreach ($detail_pemesanan as $baris) {?>
                          <td><span><?=$no?></span></td>
                          <td>
                            <div class="form-group">
                              <select class="form-control" name="id_paket[]" onchange="jadwal(this.id),harga(this.id)" id="id_paket-<?=$no;?>" required="required">
                                
                                <option selected="selected"  value="<?=$baris->id_paket?>"><?=$baris->nama_paket?></option>
                               
                                <?php foreach ($paket as $p) {?>
                                <option  value="<?=$p->id_paket?>"><?=$p->nama_paket?></option>
                                <?php
                                }
                                ?>
                              </select>
                            </div>
                          </td>
                          <td>
                            <div id="hasiljadwal-<?=$no;?>">
                              <select class="form-control" name="id_jadwal[]"  id="id_jadwal-<?=$no;?>" required="required">
                                <option disabled selected value="">-- Pilih Jadwal --</option>
                                <option value="<?=$baris->id_jadwal?>" selected="selected" readonly="readonly"> <?=$baris->tgl_keberangkatan?><?=$baris->jam_terbang?></option>                                  
                              </select>
                            </div>
                          </td>
                          <td><input type="number" name="jumlah[]" onchange="hitung(this.value,<?=$no;?>)" min="1" class="jumlah-<?=$no;?>" value="<?=$baris->pax?>"></td>
                          <td><input type="harga"  readonly="readonly"  name="harga[]"    class="harga-<?=$no;?>" value="<?=$baris->jumlah?>"></td>
                          <td><input type="total"   readonly="readonly" name="total[]"    class="total-<?=$no;?>" value="<?=$baris->subtotal?>"></td>
                          <td> 
                            <!-- <button type="button" class="btn btn-block btn-success" id="more_fields">+</button> -->
                            <a href="<?=base_url()?>pemesanan/delete_detail_pemesanan/<?=$baris->id_detail_pemesanan?>/<?=$baris->id_header_pemesanan?>" class="btn btn-block btn-danger" onclick="return confirm('Apakah anda yakin menghapus data ini?')">-</a>
                          </td>
                          </tr>
                        
                        <?php 
                          $no++;
                          }
                        ?>
                       
                        </tbody>
                      </table>
                    </div>
                    <!-- /.col -->
                  </div>
                  <!-- /.row -->

                  <div class="row">
                    <!-- accepted payments column -->
                    <div class="col-6">
                      <!-- <p class="lead">Payment Methods:</p>
                      <img src="../../dist/img/credit/visa.png" alt="Visa">
                      <img src="../../dist/img/credit/mastercard.png" alt="Mastercard">
                      <img src="../../dist/img/credit/american-express.png" alt="American Express">
                      <img src="../../dist/img/credit/paypal2.png" alt="Paypal">

                      <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                        Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem
                        plugg
                        dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
                      </p> -->
                    </div>
                    <!-- /.col -->
                    <div class="col-6">  
                     
                      <p class="lead">Amount Due <?=date('Y/m/d');?></p>
                      <div class="table-responsive">
                        <table class="table">
                          <tr>
                            <th style="width:50%">Subtotal:</th>
                            <?php 
                            
                              $subtotal  = 0;
                              $diskon = 0;
                              $tax    = 0;
                              $other  = 0;
                              $grandtotal  = 0;
                            foreach ($detail_pemesanan as $row){
                              $subtotal  = $subtotal + $row->subtotal;
                              $diskon = $diskon + $row->diskon;
                              $tax    = $tax + $row->tax;
                              $other  = $other + $row->other;
                              $grandtotal  = $grandtotal + $row->grand_total;

                              ?>

                            <?php 
                              } 
                            ?>
                              
                           
                            <td><input type="text" name="subtotal" id="subtotal" readonly="readonly" value=" <?=$subtotal?>"onchange="grandtotal(this.value)" readonly></td>
                          </tr>
                          <tr>
                            <th>Diskon:</th>
                            <td><input type="number" name="diskon"  id="diskon" min="0" value="<?=$diskon?>"  onchange="grandtotal(document.getElementById('subtotal').value)" readonly></td>
                          </tr>
                          <tr>
                          <tr>
                            <th>Tax(Ppn):</th>
                            <td><input type="number" name="tax" id="tax" min="0" value="<?=$tax?>"  onchange="grandtotal(document.getElementById('subtotal').value)" readonly></td>
                          </tr>
                          <tr>
                            <th>Other:</th>
                            <td><input type="number" name="other"  id="other" min="0" value="<?=$other?>"  onchange="grandtotal(document.getElementById('subtotal').value)" readonly></td>
                          </tr>
                          <tr>
                            <th>Grand Total:</th>
                            <td><input type="text" id="grandtotal" readonly="readonly" name="grandtotal" value="<?=$grandtotal?>"></td>
                          </tr>
                        </table>
                      </div>
                    </div>
                    <!-- /.col -->
                  </div>
                  <!-- /.row -->
             

              <!-- this row will not appear when printing -->
              <div class="row no-print">
                <div class="col-12">
                  <!-- <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a> -->
                  <button type="submit" class="btn btn-success float-right"><i class="fa fa-credit-card"></i> Save
                  </button>
                  <!-- <button type="button" class="btn btn-primary float-right" style="margin-right: 5px;">
                    <i class="fa fa-download"></i> Generate PDF
                  </button> -->
                </div>
                </form>
              </div>
            </div>
            <!-- /.invoice -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

      <script>
        $(document).ready(function() 
        {

            $("#more_fields").click(function() 
            {
                var id_paket = $("#id_paket").val();
                var total_name = $(':input[name*="id_paket"]').length+1;


                $.ajax({
                    url  : "<?php echo base_url('Ajax/get_paket'); ?>",
                    data : "total="+total_name,
                    type : "POST",
                    success: function(data) 
                    {
                    document.getElementById("myTable").insertRow(-1).innerHTML = data;
                    }
                });
            });
        });

        function hitung(number,no){
            var harga = $(".harga-"+no).val();
            var total = harga * number;
            
            $(".total-"+no).val(total);
           
            subtotal();
        }

        function jadwal(id){
        var pecah = id.split('-');
        var num = pecah[1]; 
        var id_paket = $("#"+id).val();
      
          $.ajax({
                  type : "POST",
                  url  : "<?php echo base_url('Ajax/jadwal'); ?>",
                  data : "id_paket=" + id_paket,
                  success: function(data) 
                  {
                    $("#hasiljadwal-"+num).html(data);
                  }
              });
        }

        function harga(id){
        var pecah = id.split('-');
        var num = pecah[1];
        var jumlah = $(".jumlah-"+num).val();
        var id_paket = $("#id_paket-"+num).val();
       
                $.ajax({
                    type : "POST",
                    url  : "<?php echo base_url('Ajax/fill_info'); ?>",
                    data : "id_paket=" + id_paket,
                    dataType : 'json',
                    success: function(data) 
                    {
                        $(".harga-"+num).val(data['jumlah']);

                        
                        hitung(jumlah,num);
                    }
                });
        }

        function subtotal(){
          var sum = 0;
          // alert($().length);
          $('input[name="total[]"]').each(function(){
            var nilai = $(this).val();
            sum += parseFloat(nilai);
          });
          $("#subtotal").val(sum);

          grandtotal(sum);
        }
      
      function grandtotal(subtotal){
        var diskon   = parseInt($("#diskon").val());
        var tax      = parseInt($("#tax").val());
        var other    = parseInt($("#other").val());

        var grandtotal = subtotal - diskon + tax + other;
        $('#grandtotal').val(grandtotal);
      }



      //         function add_fields() {   
      //         var no = getElementById("no").value;
      //         var php = "<?php foreach($paket as $baris) {?> <option value="<?=$baris->id_paket?>"><?=$baris->nama_paket?></option><?php }?>";

      //         document.getElementById("myTable").insertRow(-1).innerHTML = 
      //         ' <td><span id="no">'+(no+1)+'</span></td><td><div class="form-group"><select class="form-control" name="id_paket" id="id_paket" required="required"><option disabled selected value=""></option>'+php+'</select></div></td><td><input type="number" name="jumlah" min="1" id="jumlah"></td><td><input type="harga" name="harga"    id="harga"></td><td><input type="total" name="total"    id="total"></td><td> <button type="submit" class="btn btn-block btn-success" onclick="add_fields();" id="more_fields">+</button>';
      // }
    </script>