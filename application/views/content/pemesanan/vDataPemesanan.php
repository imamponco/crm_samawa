<!-- Main content -->

    <section class="content">
       <?php if($this->session->flashdata('edit')):?>
        <div class="col-3">
          <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fa fa-check"></i> Success!</h5>
                <?php echo $this->session->flashdata('edit'); ?>
          </div>
        </div>
      <?php endif; ?>
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Master Pemesanan</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div id="example1_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                 <!-- filter -->
                 <div class="row">
                 
                   <div class="col-sm-12 col-md-2">
                    <label>
                     <?php echo form_open('Pemesanan/list');?>
                    <div class="form-group">
                        <label>Nama</label>
                        <input type="text" name="nama" class="form-control" placeholder="Nama">
                      </div>
                    </label>
                    </div>

                    <div class="col-sm-12 col-md-2">
                    <label>
                       <div class="form-group">
                        <label>Agen</label>
                        <input type="text" name="agen" class="form-control" placeholder="Nama Agen">
                      </div>
                    </label>
                    </div>

                    <div class="col-sm-12 col-md-3">
                    <label>
                       <div class="form-group">
                        <label>First</label>
                        <input type="date" name="first_date" class="form-control">
                      </div>
                    </label>
                    </div>

                    <div class="col-sm-12 col-md-3">
                    <label>
                       <div class="form-group">
                        <label>End</label>
                        <input type="date" name="end_date" class="form-control">
                      </div>
                    </label>
                    </div>

                    <div class="col-sm-12 col-md-2">
                      <label>
                        Filter

                        <!--  <a href="<?=base_url();?>Pemesanan/list"> -->
                            <button type="submit" class="btn btn-block btn-success">Go</button>
                        <!--  </a> -->
                       </label> 
                     <?php echo form_close();?>
                    </div>
                </div>

             <!--  <div class="row">
                <div class="col-sm-12 col-md-6">
                <label>
                   <a href="<?=base_url();?>Pemesanan/add_pemesanan">
                      <button type="button" class="btn btn-block btn-success">Add Data</button>
                   </a>
                </label>
                </div>  
              </div> -->

       <div class="row">
          <div class="col-sm-12">
            <div style ="width:auto; height:auto; overflow-x:scroll">
              <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Pelanggan</th>
                <th>Nama Agen</th>
                <th>No Transaksi</th>
                <th>Subtotal</th>
                <th>Diskon</th>
                <th>Tax</th>
                <th>Other</th>
                <th>Grand Total</th>
                <th>Bukti Pembayaran</th>
                <th>Action</th>     
            </tr>
        </thead>
         <tbody>
            <?php 
            $no = 1;
            $total = 0;
            foreach ($pemesanan as $row) {
            $array = (array) $row;
           
            // var_dump($value);
            // die;
            ?>
             
                  <tr>
                      <td><?=$no?></td>
                      <td><?=$array["username"] ?></td>
                      <td><?=$array["nama_agen"] ?></td>
                      <td><?=$array["id_header_pemesanan"] ?></td>
                      <td><?=(!empty($array["subtotal"]) ? "Rp.".number_format($array["subtotal"],0,',','.'): "Rp.".number_format(0,0,',','.'));?></td>
                      <td><?=(!empty($array["diskon"]) ? "Rp.".number_format($array["diskon"],0,',','.') : "Rp.".number_format(0,0,',','.')); ?></td>
                      <td><?=(!empty($array["tax"]) ?  "Rp.".number_format($array["tax"],0,',','.') : "Rp.".number_format(0,0,',','.')); ?></td>
                      <td><?=(!empty($array["other"]) ? "Rp.".number_format($array["other"],0,',','.') : "Rp.".number_format(0,0,',','.'));?></td>
                      <td><?=(!empty($array["grand_total"]) ?  "Rp.".number_format($array["grand_total"],0,',','.') : "Rp.".number_format(0,0,',','.'));?></td>
                      <td><a href="<?=base_url();?>assets/images/<?=$row->bukti_pembayaran?>">View</a></td>
                      <td>
                           <a href="<?=base_url();?>pemesanan/detail_pemesanan/<?=$row->id_header_pemesanan?>">
                                <button type="button" class="btn btn-block btn-info">Detail</button>
                           </a></br>
                           <a href="<?=base_url();?>pemesanan/edit_pemesanan/<?=$row->id_header_pemesanan?>">
                                <button type="button" class="btn btn-block btn-primary">Edit</button>
                           </a></br>
                           <a href="<?=base_url();?>pemesanan/delete_pemesanan/<?=$row->id_header_pemesanan?>" onclick="return confirm('Apakah anda yakin menghapus data ini?')">
                                <button type="button"  class="btn btn-block btn-danger">Delete</button>
                           </a>
                      </td>
                  </tr>
          
            <?php
            $total = $total + $row->grand_total;
            $no++;
            }
            ?> 
            <!-- <tr>
              <td><center><?php//(!empty($total) ? echo "Total" : null );?></center></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td><?php//(!empty($total) ? echo "Rp ".number_format($total,0,',','.') : null );?></td>
              <td></td>
            </tr> -->
      </tbody> 
    </table>
              </div>
            <!-- scroll bar -->
           </div>
          <!-- /.row -->
        </div>
        <!-- /.row -->
      </div>
      <!-- warpper -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->


        <script type="text/javascript">
        	$(document).ready(function() {
		     var table = $('#example').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
    } );
 
    table.buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );
		} );
        </script>

        
 