    <section class="content">
      <?php if($this->session->flashdata('msg')):?>
        <div class="col-4">
          <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fa fa-check"></i> Success!</h5>
                <?php echo $this->session->flashdata('msg'); ?>
          </div>
        </div>
      <?php endif; ?>
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            

             <?php
              echo validation_errors();
              ?>

            <!-- Main content -->
            <div class="invoice p-3 mb-3">
              <!-- title row -->
              <div class="row">
                <div class="col-12">
                  <h4>
                    <i class="fa fa-globe"></i> Gaido Travel And Tour.
                    <small class="float-right">Date: <?=date('Y-m-d');?></small>
                  </h4>
                </div>
                <!-- /.col -->
              </div>
              <!-- info row -->
              <form role="form" id="form" action="<?=base_url();?>Pemesanan/do_add_pemesanan" method="post">
                <input type="hidden" name="id_user" value="<?=$this->session->userdata('id_user')?>">
                 <input type="hidden" name="status" value="Belum Lunas">
                  <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">
                      <address>
                      <div class="row">
                        <div class="col-5">
                          <div class="form-group">
                            <label>Pelanggan</label>
                            <select class="form-control" name="id_pelanggan" required="required">
                              <option disabled selected value=""> </option>
                              <?php foreach ($pelanggan as $row) {?>                         
                              <option value="<?=$row->nik?>"><?=$row->username?></option>
                              <?php
                                 }
                              ?>
                            </select>
                          </div>
                        </div>
                        <div class="col-3">
                          <div class="form-group">
                            <label>Add</label>
                             <a href="<?=base_url();?>Customer/add_customer">
                            <span class="btn btn-block btn-success">+</span>
                            </a>
                          </div>
                        </div>
                      </div>
                      </address>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                      <address>
                      <div class="row">
                        <div class="col-5">
                          <div class="form-group">
                            <label>Tanggal Pemesanan</label>
                            <input type="date" name="tgl_pesan" id="date" >
                          </div>
                        </div>
                        <div class="col-3">
                          
                        </div>
                      </div>
                      </address>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                     <!--  <b>Invoice #007612</b><br>
                      <br>
                      <b>Order ID:</b> 4F3S8J<br>
                      <b>Payment Due:</b> 2/22/2014<br>
                      <b>Account:</b> 968-34567 -->
                    </div>
                    <!-- /.col -->
                  </div>
                  <!-- /.row -->

                  <!-- Table row -->
                  <div class="row">
                    <div class="col-12 table-responsive">
                      <table class="table table-striped" id="myTable">
                        <thead>
                        <tr>
                          <th>No</th>
                          <th>Paket</th>
                          <th>Jadwal</th>
                          <th>Jumlah</th>
                          <th>Harga</th>
                          <th>Total</th>
                          <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                          <td><span>1</span></td>
                          <td>
                            <div class="form-group">
                              <select class="form-control" name="id_paket[]" onchange="jadwal(this.id),harga(this.id)" id="id_paket-1" required="required">
                                <option disabled selected value=""></option>
                                <?php foreach ($paket as $baris) {?>
                                <option value="<?=$baris->id_paket?>"><?=$baris->nama_paket?></option>
                                <?php 
                                  }
                                ?>
                              </select>
                            </div>
                          </td>

                          <td>
                            <div class="form-group">
                              <div id="hasiljadwal-1">
                              <select class="form-control" name="id_jadwal[]"  id="id_jadwal-1" required="required">
                                <option disabled selected value="">-- Pilih Jadwal --</option> 
                              </select>
                              </div>
                            </div>
                          </td>

                          <td><input type="number" name="jumlah[]" onchange="hitung(this.value,'1')" min="1" class="jumlah-1" value="1"></td>
                          <td><input type="number" name="harga[]" readonly="readonly" class="harga-1"></td>
                          <td><input type="number" name="total[]" readonly="readonly" class="total-1"></td>
                          <td> <button type="button" class="btn btn-block btn-success" id="more_fields">+</button></td>
                        </tr>
                        </tbody>
                      </table>
                    </div>
                    <!-- /.col -->
                  </div>
                  <!-- /.row -->

                  <div class="row">
                    <!-- accepted payments column -->
                    <div class="col-6">
                      <!-- <p class="lead">Payment Methods:</p>
                      <img src="../../dist/img/credit/visa.png" alt="Visa">
                      <img src="../../dist/img/credit/mastercard.png" alt="Mastercard">
                      <img src="../../dist/img/credit/american-express.png" alt="American Express">
                      <img src="../../dist/img/credit/paypal2.png" alt="Paypal">

                      <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                        Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem
                        plugg
                        dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
                      </p> -->
                    </div>
                    <!-- /.col -->
                    <div class="col-6">
                      <p class="lead">Amount Due <?=date('Y/m/d');?></p>

                      <div class="table-responsive">
                        <table class="table">
                          <tr>
                            <th style="width:50%">Subtotal:</th>
                            <td><input type="text" name="subtotal" id="subtotal" value="0" readonly="" onchange="grandtotal(this.value)"></td>
                          </tr>
                          <tr>
                            <th>Diskon:</th>
                            <td><input type="number" name="diskon" id="diskon" min="0" value="0"  onchange="grandtotal(document.getElementById('subtotal').value)"></td>
                          </tr>
                          <tr>
                          <tr>
                            <th>Tax(Ppn):</th>
                            <td><input type="number" name="tax" id="tax" min="0" value="0"  onchange="grandtotal(document.getElementById('subtotal').value)"></td>
                          </tr>
                          <tr>
                            <th>Other:</th>
                            <td><input type="number" name="other" id="other" min="0" value="0"  onchange="grandtotal(1)"></td>
                          </tr>
                          <tr>
                            <th>Grand Total:</th>
                            <td><input type="text" id="grandtotal" value="0" name="grandtotal" readonly="readonly" ></td>
                          </tr>
                        </table>
                      </div>
                    </div>
                    <!-- /.col -->
                  </div>
                  <!-- /.row -->
             

              <!-- this row will not appear when printing -->
              <div class="row no-print">
                <div class="col-12">
                  <!-- <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a> -->
                  <button type="submit" class="btn btn-success float-right"><i class="fa fa-credit-card"></i> Save
                  </button>
                  <!-- <button type="button" class="btn btn-primary float-right" style="margin-right: 5px;">
                    <i class="fa fa-download"></i> Generate PDF
                  </button> -->
                </div>
                </form>
              </div>
            </div>
            <!-- /.invoice -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

     <script>
        $(document).ready(function() 
        {

            $("#more_fields").click(function() 
            {
                var id_paket = $("#id_paket").val();
                var total_name = $(':input[name*="id_paket"]').length+1;
                $.ajax({
                    url  : "<?php echo base_url('Ajax/get_paket'); ?>",
                    data : "total="+total_name,
                    type : "POST",
                    success: function(data) 
                    {
                    document.getElementById("myTable").insertRow(-1).innerHTML = data;
                    }
                });
            });
        });

        function hitung(number,no){
            var harga = $(".harga-"+no).val();
            var total = harga * number;
            
            $(".total-"+no).val(total);
           
            subtotal();
        }

        function jadwal(id){
        var pecah = id.split('-');
        var num = pecah[1]; 
        var id_paket = $("#"+id).val();
        var id_jadwal = $("#id_jadwal-"+id).val();

          $.ajax({
                  type : "POST",
                  url  : "<?php echo base_url('Ajax/jadwal'); ?>",
                  data : "id_paket=" + id_paket +
                         "&id=" + id,
                  success: function(data) 
                  {
                    $("#hasiljadwal-"+num).html(data);
                  }
              });
        }

        function harga(id){
        var pecah = id.split('-');
        var num = pecah[1];
        var jumlah = $(".jumlah-"+num).val();
        var id_paket = $("#id_paket-"+num).val();
       
                $.ajax({
                    type : "POST",
                    url  : "<?php echo base_url('Ajax/fill_info'); ?>",
                    data : "id_paket=" + id_paket,
                    dataType : 'json',
                    success: function(data) 
                    {
                        $(".harga-"+num).val(data['harga']);
                        
                        hitung(jumlah,num);
                    }
                });
        }

        function subtotal(){
          var sum = 0;
          // alert($().length);
          $('input[name="total[]"]').each(function(){
            var nilai = $(this).val();
            sum += parseFloat(nilai);
          });
          $("#subtotal").val(sum);

          grandtotal(sum);
        }
      
      function grandtotal(subtotal){
        var diskon   = parseInt($("#diskon").val());
        var tax      = parseInt($("#tax").val());
        var other    = parseInt($("#other").val());

        var grandtotal = subtotal - diskon + tax + other;
        $('#grandtotal').val(grandtotal);
      }



      //         function add_fields() {   
      //         var no = getElementById("no").value;
      //         var php = "<?php foreach($paket as $baris) {?> <option value="<?=$baris->id_paket?>"><?=$baris->nama_paket?></option><?php }?>";

      //         document.getElementById("myTable").insertRow(-1).innerHTML = 
      //         ' <td><span id="no">'+(no+1)+'</span></td><td><div class="form-group"><select class="form-control" name="id_paket" id="id_paket" required="required"><option disabled selected value=""></option>'+php+'</select></div></td><td><input type="number" name="jumlah" min="1" id="jumlah"></td><td><input type="harga" name="harga" readonly="readonly" id="harga"></td><td><input type="total" name="total" readonly="readonly" id="total"></td><td> <button type="submit" class="btn btn-block btn-success" onclick="add_fields();" id="more_fields">+</button>';
      // }
    </script>