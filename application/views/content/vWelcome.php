   <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-1 col-6"></div>
          <div class="col-lg-3 col-6">

            <?php  
              $this->db->select('count("id_transaksi") as jumlah');
              $this->db->from('detail_pemesanan');
              $order = $this->db->get();
              $hasil = $order->row();

              $hasil2 = $this->db->query("SELECT COUNT(id_user) as jumlah FROM USER WHERE id_user LIKE '%CO%' AND verified_at IS NOT NULL")->row();

              $this->db->select('count("id_agen") as jumlah');
              $this->db->from('agen');
              $agen = $this->db->get();
              $hasil3 = $agen->row();
              ?>


            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3><?=$hasil->jumlah?></h3>

                <p>Orders</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <?php if($this->session->userdata('permission') == 'AF'):?>
              <a href="<?=base_url();?>Pemesanan/list" class="small-box-footer">
                More info <i class="fa fa-arrow-circle-right"></i>
              </a>
              <?php else:?>
              <a href="#" class="small-box-footer">
                More Info<i class="fa fa-arrow-circle-right"></i>
              </a>
              <?php endif;?>
            </div>
          </div>
        
          
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3><?=$hasil2->jumlah?></h3>

                <p>Jumlah Pelanggan</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <?php if($this->session->userdata('permission') == 'AM'):?>
              <a href="<?=base_url();?>Customer/list" class="small-box-footer">
                More info <i class="fa fa-arrow-circle-right"></i>
              </a>
              <?php else:?>
              <a href="#" class="small-box-footer">
                More Info<i class="fa fa-arrow-circle-right"></i>
              </a>
              <?php endif;?>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3><?=$hasil3->jumlah?></h3>

                <p>Jumlah Agen</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <?php if($this->session->userdata('permission') == 'AO'):?>
              <a href="<?=base_url();?>Agen/list" class="small-box-footer">
                More info <i class="fa fa-arrow-circle-right"></i>
              </a>
              <?php else:?>
              <a href="#" class="small-box-footer">
                More Info<i class="fa fa-arrow-circle-right"></i>
              </a>
              <?php endif;?>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div class="row" >
          <!-- Left col -->
          <div class="col-3"></div>
          <section class="col-lg-6 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            
            <!-- Calendar -->
            <div class="card bg-success-gradient">
              <div class="card-header no-border">

                <h3 class="card-title">
                  <i class="fa fa-calendar"></i>
                  Calendar
                </h3>
                <!-- tools card -->
                <div class="card-tools">
                  <!-- button with a dropdown -->
                  <div class="btn-group">
                    <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">
                      <i class="fa fa-bars"></i></button>
                    <div class="dropdown-menu float-right" role="menu">
                      <a href="#" class="dropdown-item">Add new event</a>
                      <a href="#" class="dropdown-item">Clear events</a>
                      <div class="dropdown-divider"></div>
                      <a href="#" class="dropdown-item">View calendar</a>
                    </div>
                  </div>
                  <button type="button" class="btn btn-success btn-sm" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-success btn-sm" data-widget="remove">
                    <i class="fa fa-times"></i>
                  </button>
                </div>
               
                <!-- /. tools -->
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">

                <!--The calendar -->
                <div id="calendar" style="width: 100%"></div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </section>
          <div class="col-3"></div>
       
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>