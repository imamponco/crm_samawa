  <!-- Main content -->
    <section class="content">
      <?php if($this->session->flashdata('msg')):?>
        <div class="col-4">
          <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fa fa-check"></i> Success!</h5>
                <?php echo $this->session->flashdata('msg'); ?><a href="<?=base_url()?>/konten/list">Lihat</a>
          </div>
        </div>
      <?php endif; ?>

      <?php
      echo validation_errors();
      ?>
      
      <div class="container-fluid">
          <div class="row">
            <!-- left column -->
              <div class="col-md-12">
                <!-- general form elements -->
                  <div class="card card-primary">
                    <div class="card-header">
                      <h3 class="card-title">Input Data Penawaran</h3>
                        </div>
                        <!-- /.card-header -->
                          <!-- form start -->
                          <form role="form" id="form" action="<?=base_url();?>konten/do_add_konten" method="post">
                            <div class="card-body">
                          <div class="row">

                             <div class="col-6">
                              <div class="form-group">
                                <label>Pelanggan</label>
                                <select class="form-control" name="id_user">
                                  <option value="" disabled selected>-- Pilih Pelanggan --</option>
                                  <?php
                                  if (set_value('id_user') != '') {
                                    echo " <option value='".set_value('id_user')."' selected='selected'>".set_value('id_user')."</option>";
                                  }
                                  ?>
                                  <?php foreach($pelanggan as $row):?>
                                  <option value="<?=$row->id_user?>"><?=$row->username?></option>
                                  <?php endforeach;?>
                                </select>
                              </div>
                            </div> 
                            
                            <div class="col-6">
                              <div class="form-group">
                                <label>Pesan</label>
                                <textarea class="form-control" name="message" rows="3" placeholder="Enter ..."><?=set_value('message');?></textarea>
                              </div>
                            </div>
                            
                          </div>

                         </div>
                        <!-- /.card-body -->
                      <div class="card-footer">
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                  </form>
                </div>
                <!-- /.card -->   
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>