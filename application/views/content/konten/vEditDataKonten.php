  <!-- Main content -->
    <section class="content">
      <?php if($this->session->flashdata('msg')):?>
        <div class="col-4">
          <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fa fa-check"></i> Success!</h5>
                <?php echo $this->session->flashdata('msg'); ?>
          </div>
        </div>
      <?php endif; ?>
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Data Penawaran</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
            <?php foreach ($konten as $row) { ?>
              <form role="form" id="form" action="<?=base_url();?>konten/do_edit_konten/<?=$row->id_content?>" method="post">
                        <div class="card-body">
                          <div class="row">
                          
                             <div class="col-6">
                              <div class="form-group">
                                <label>Pelanggan</label>
                                <select class="form-control" name="id_user">
                                  <option value="<?=$row->id_user?>" selected="selected"><?=$row->username?></option>
                                  <?php foreach($pelanggan as $pel):?>
                                  <option value="<?=$pel->id_user?>"><?=$pel->username?></option>
                                  <?php endforeach;?>
                                </select>
                              </div>
                            </div> 
                            
                            <div class="col-6">
                              <div class="form-group">
                                <label>Pesan</label>
                                <textarea class="form-control" name="message" rows="3" placeholder="Enter ..."><?=$row->message?></textarea>
                              </div>
                            </div>
                
                          </div>
                         </div>
                        <!-- /.card-body -->
                      <div class="card-footer">
                      <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                  </form>
                  <?php }?>
                </div>
                <!-- /.card -->   
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>