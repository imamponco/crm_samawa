  <!-- Main content -->
    <section class="content">
      <?php if($this->session->flashdata('msg')):?>
        <div class="col-4">
          <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fa fa-check"></i> Success!</h5>
                <?php echo $this->session->flashdata('msg'); ?>
          </div>
        </div>
      <?php endif; ?>

      <?php echo validation_errors(); ?>

      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Data Paket</h3>
              </div>
              <div class="col-12">
              <!-- /.card-header -->
              <!-- form start -->
            <?php foreach ($paket as $row) { ?>
              <form role="form" id="form" action="<?=base_url();?>paket/do_edit_paket/<?=$row->id_paket?>" method="post" enctype="multipart/form-data">
            <div class="card-body">
              <div class="row">
              
                <div class="col-12">
                  <div class="form-group">
                     <center>
                      
                      <?php if(!empty($row->gambar_paket)){?>
                        <a href="<?=base_url();?>paket/delete_image/<?=$row->gambar_paket?>/<?=$row->id_paket?>" id="delete" onclick="return confirm('Apakah anda yakin menghapus gambar ini?')"><i class="fa fa-times-circle" style="padding-left: 50%;"></i></a><br>
                      <?php
                        }
                      ?> 
                      
                      <img id="prev_foto" width="500px" src="<?=base_url();?>/assets/images/<?=$row->gambar_paket?>" class="img-responsive img-thumbnail" alt="Preview Image">
                     </center>
                    </div>
                  </div>

                  <div class="col-8">
                    <div class="form-group">
                      <label>Image</label>
                      <div class="form-control">
                      <input type="file" name="img" id="file_gambar" accept="image/*" value=""> 
                      </div>
                    </div>
                  </div>

                  <div class="col-6">
                    <div class="form-group">
                      <label>Nama Paket</label>
                      <input type="text" name="nama_paket" class="form-control" placeholder="Nama Paket" value="<?=$row->nama_paket?>">
                    </div>
                  </div>

                  <div class="col-6">
                    <div class="form-group">
                      <label>Harga</label>
                      <input type="number" name="harga" class="form-control" placeholder="Harga" value="<?=$row->jumlah?>">
                    </div>
                  </div>

                  <div class="col-6">
                      <div class="form-group">
                      <label>Country</label>
                      <select class="form-control" name="country">
                        <option value="<?=$row->country_code?>" selected="selected" readonly="readonly"><?=$row->country_name?></option>
                        <?php foreach ($country as $cou):?>
                        <option value="<?=$cou->country_code?>"><?=$cou->country_name?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                  </div>

                  <div class="col-6">
                    <div class="form-group">
                      <label>Diskon</label>
                      <input type="number" name="diskon" class="form-control" placeholder="Diskon" value="<?=$row->diskon?>">
                    </div>
                  </div>

                  <div class="col-6">
                    <div class="form-group">
                      <label>Tax</label>
                      <input type="number" name="tax" class="form-control" placeholder="Tax" value="<?=$row->tax?>">
                    </div>
                  </div>

                  <div class="col-6">
                    <div class="form-group">
                      <label>Other</label>
                      <input type="number" name="other" class="form-control" placeholder="Other" value="<?=$row->other?>">
                    </div>
                  </div>

                  <div class="col-12">
                    <div class="form-group">
                      <label>Durasi</label>
                      <input type="number" name="durasi" class="form-control" placeholder="Durasi" value="<?=$row->durasi?>">
                    </div>
                  </div>   

                  <div class="col-12">
                    <div class="form-group">
                      <label>Keterangan</label>
                      <textarea class="form-control" name="keterangan" rows="3" placeholder="Enter ..."><?=$row->keterangan?></textarea>
                    </div>
                  </div>
                    <?php
                    }
                  ?>

                  </div>

                </div>
                <!-- /.card-body -->

                    <div class="card-footer">
                      <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                  </form>
                 </div>
                </div>
                <!-- /.card -->   
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <script type="text/javascript">
  function readURL(input) {
   if (input.files && input.files[0]) {
    var reader = new FileReader();
    console.log(input.files)
    reader.onload = function (e) {
     console.log(e.target.result)
     $('#prev_foto').attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.files[0]);
   }
  }

  $(document).ready(function(){
   $('#file_gambar').change(function(){
     readURL(this);
   });

  });

  </script>