  <!-- Main content -->
    <section class="content">
      <?php if($this->session->flashdata('msg')):?>
        <div class="col-4">
          <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fa fa-check"></i> Success!</h5>
                <p><?php echo $this->session->flashdata('msg'); ?> <a href="<?=base_url()?>paket/list">Lihat</a></p>
          </div>
        </div>
      <?php endif; ?>

      <?php
      echo validation_errors();
      ?>
      
      <div class="container-fluid">
          <div class="row">
            <!-- left column -->
              <div class="col-md-12">
                <!-- general form elements -->
                  <div class="card card-primary">
                    <div class="card-header">
                      <h3 class="card-title">Input Data Paket</h3>
                        </div>
                         <div class="col-12">
                        <!-- /.card-header -->
                          <!-- form start -->
                          <form role="form" id="form" action="<?=base_url();?>paket/do_add_paket" method="post" enctype="multipart/form-data">
                            <div class="card-body">
                          <div class="row">

                            <div class="col-12">
                              <div class="form-group">
                               <center>
                                <img id="prev_foto" width="500px" src="" class="img-responsive img-thumbnail" alt="Preview Image">
                               </center>
                              </div>
                            </div>

                            <div class="col-10">
                              <div class="form-group">
                                <label>Image</label>
                                <input type="file" name="img" id="file_gambar" accept="image/*" class="form-control" value="<?=set_value('img');?>">
                              </div>
                            </div>

                            <div class="col-6">
                              <div class="form-group">
                                <label>Nama Paket</label>
                                <input type="text" name="nama_paket" class="form-control" placeholder="Nama Paket" value="<?=set_value('nama_paket');?>">
                              </div>
                            </div>

                            <div class="col-6">
                              <div class="form-group">
                                <label>Harga</label>
                                <input type="number" name="harga" class="form-control" placeholder="Harga" value="<?=set_value('harga');?>">
                              </div>
                            </div>

                            <div class="col-6">
                              <div class="form-group">
                                <label>Country</label>
                                <select class="form-control" name="country">
                                  <option value="" disabled selected>-- Pilih Negara --</option>
                                  <?php foreach ($country as $cou):?>
                                  <option value="<?=$cou->country_code?>"><?=$cou->country_name?></option>
                                  <?php endforeach;?>
                                </select>
                              </div>
                            </div>


                            <div class="col-6">
                              <div class="form-group">
                                <label>Diskon</label>
                                <input type="number" name="diskon" class="form-control" placeholder="Diskon" value="<?=set_value('diskon');?>">
                              </div>
                            </div>

                            <div class="col-6">
                              <div class="form-group">
                                <label>Tax</label>
                                <input type="number" name="tax" class="form-control" placeholder="Tax" value="<?=set_value('tax');?>">
                              </div>
                            </div>

                            <div class="col-6">
                              <div class="form-group">
                                <label>Other</label>
                                <input type="number" name="other" class="form-control" placeholder="Other" value="<?=set_value('other');?>">
                              </div>
                            </div>
                            

                            <div class="col-12">
                              <div class="form-group">
                                <label>Durasi</label>
                                <input type="number" name="durasi" class="form-control" placeholder="Durasi" value="<?=set_value('durasi');?>">
                              </div>
                            </div>   

                            <div class="col-12">
                              <div class="form-group">
                                <label>Keterangan</label>
                                <textarea class="form-control" name="keterangan" rows="3" placeholder="Enter ..."><?=set_value('keterangan');?></textarea>
                              </div>
                            </div>

                          </div>

                         </div>
                        <!-- /.card-body -->
                      <div class="card-footer">
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                  </form>
                 </div>
                </div>
                <!-- /.card -->   
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <script type="text/javascript">
    function readURL(input) {
   if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function (e) {
     $('#prev_foto').attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.files[0]);
   }
  }

  $(document).ready(function(){
   $('#file_gambar').change(function(){
     readURL(this);
   });

  });

  </script>