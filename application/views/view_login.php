<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Monitoring System</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
<script src="<?=base_url();?>assets/jquery-3.4.1.min.js"></script>
<script src="<?=base_url();?>/assets/bootstrap.v3.3.7.min.js"></script> 
<style type="text/css">
	.login-form {
		width: 340px;
    	margin: 50px auto;
	}
    .login-form form {
    	margin-bottom: 15px;
        background: #f7f7f7;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;
    }
    .login-form h2 {
        margin: 0 0 15px;
    }
    .form-control, .btn {
        min-height: 38px;
        border-radius: 2px;
    }
    .btn {        
        font-size: 15px;
        font-weight: bold;
    }.modal-content{
    	width: 100%;
    }.wrap{
    	padding: 10%;
    	padding-left: 20%;
    	padding-right: 20%;
    }.enter{
    	padding-left: 15px;
    	padding-right: 5px;
    }#titik{
    	padding-left: 20px;
    	padding-right: 10px;
    }#inputemail{
    	padding-left: 20px;
    	padding-right: 10px;
    }#button{
    	padding-left: 5px;
    	padding-right: 10px;
    }
    body{
    	background-image: url('assets/login/background.jpg');
    	background-size: cover;
    }
</style>
</head>
	<body>
	<?php if($this->session->flashdata('login')):?>
       <script type='text/javascript'>
		 alert("<?=$this->session->flashdata('login')?>");
	   </script>;
      <?php endif; ?>
		<div class="login-form">
		    <form action="login/validation" method="post">
		        <h2 class="text-center">Log in</h2>       
		        <div class="form-group">
		        	<label>Email:</label>
		            <input type="email" name="email" class="form-control" placeholder="Email" required="required" value="<?=(isset($_COOKIE['email']) ? $_COOKIE['email'] : null);?>">
		        </div>
		        <div class="form-group">
		        	<label>Password:</label>
		            <input type="password" name="password" class="form-control" placeholder="Password" required="required" value="<?=(isset($_COOKIE['password']) ? $_COOKIE['password'] : null);?>">
		        </div>
		        <div class="form-group">
		            <button type="submit" class="btn btn-primary btn-block">Log in</button>
		        </div>
		        <div class="clearfix">
		            <label class="pull-left checkbox-inline"><input type="checkbox" name="cookie"> Remember me</label>
		           <!--  <a href="#" id="forgot" class="pull-right">Forgot Password?</a> -->
		        </div>
		      <!--  <p class="text-center" style="padding-top: 10px;"><a href="<?=base_url();?>register">Create an Account</a></p> -->
		    </form>
		   
		</div>
	</body>
<script type="text/javascript">
$('#forgot').click(function(){
	 $('#myModal').modal('show');
});

$('.close').click(function(){
  $('#myModal').modal('hide');
  }); 

// When the user clicks anywhere outside of the modal, close it
// $(window).click(function(e) {
// 	alert(e.target.id);
// 	if(e.target.className == ""){
// 		$('#myModal').modal('hide');
// 	}
//   }); 


</script>
<!-- </div> -->
</html>                                		                            