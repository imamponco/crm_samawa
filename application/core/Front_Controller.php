<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Front_Controller extends CI_Controller 
 { 

   //set the class variable.
   var $template  = array();
   var $data      = array();
   //Load layout    
   public function layout_front() {
   	date_default_timezone_set("Asia/Jakarta"); 
   
     $this->CI =& get_instance();
    
     
     $this->template['header']   = $this->load->view('layout_front/header', $this->data, true);

     $this->template['breadcrumb']   = $this->load->view('layout_front/breadcrumb', $this->data, true);

     $this->template['sidebar']   = $this->load->view('layout_front/body', $this->data, true);
    
     $this->template['content'] = $this->load->view($this->content, $this->data, true);
   
     $this->template['footer'] = $this->load->view('layout_front/footer', $this->data, true);
     $this->load->view('layout_front/index', $this->template);

    

   }
   
		
}