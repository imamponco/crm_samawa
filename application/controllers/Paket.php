<?php defined('BASEPATH') or exit('No direct script access allowed.');
/**
 * 
 */
require('Dashboard.php');

class Paket extends Dashboard
{
	function __construct()
	{
		parent:: __construct();
		$this->load->library('upload');
	}

	public function index(){
		$this->list();	
	}

	public function add_paket(){
		$this->data['paket'] = $this->global_models->getTable('paket');
		$this->data['country'] = $this->global_models->getTable('country');
		$this->content = 'content/paket/vInputPaket';
		$this->layout();
	}

	public function list(){
		$this->db->select("*");
      	$this->db->from('paket');
      	$this->db->join('country', 'paket.country_code = country.country_code','left');
      	$this->db->join('harga', 'paket.id_paket = harga.id_paket','left');
      	$this->db->where('harga.deleted_at',null);
      	$this->db->order_by('paket.id_paket','desc');
      	
      	$query = $this->db->get();

		$this->data['paket']  = $query->result();

		$data= array("menu_back" => "menu_paket");
			
		$this->session->set_userdata($data);
	
		$this->content = 'content/paket/vDataPaket';
		$this->layout();
	}

	public function edit_paket(){
		$id = $this->uri->segment(3);

		$this->data['country'] = $this->global_models->getTable('country');

		$this->db->select("*");
      	$this->db->from('paket');
      	$this->db->join('harga', 'paket.id_paket = harga.id_paket','left');
      	$this->db->join('country', 'paket.country_code = country.country_code','left');
      	$this->db->where('harga.deleted_at',null);
      	$this->db->where('paket.id_paket = "'.$id.'"');

      	
     	$query = $this->db->get();

     	$this->data['paket'] = $query->result();
   
		$this->content = 'content/paket/vEditDatapaket';
		$this->layout();
	}

	function _create_thumbs($file_name){
        // Image resizing config
        $config = array(
            // Image Large
            array(
                'image_library' => 'GD2',
                'source_image'  => '././assets/images/'.$file_name,
                'maintain_ratio'=> FALSE,
                'width'         => 700,
                'height'        => 467,
                'new_image'     => '././assets/images/large/'.$file_name
                ),
            // image Medium
            // array(
            //     'image_library' => 'GD2',
            //     'source_image'  => '././assets/images/'.$file_name,
            //     'maintain_ratio'=> FALSE,
            //     'width'         => 600,
            //     'height'        => 400,
            //     'new_image'     => '././assets/images/medium/'.$file_name
            //     ),
            // Image Small
            // array(
            //     'image_library' => 'GD2',
            //     'source_image'  => '././assets/images/'.$file_name,
            //     'maintain_ratio'=> FALSE,
            //     'width'         => 100,
            //     'height'        => 67,
            //     'new_image'     => '././assets/images/small/'.$file_name
            // )
    		);
 
        $this->load->library('image_lib', $config[0]);
        foreach ($config as $item){
            $this->image_lib->initialize($item);
            if(!$this->image_lib->resize()){
                return false;
            }
            $this->image_lib->clear();
        }
    }

	public function do_add_paket(){

		$this->form_validation->set_rules('nama_paket', 'Nama Paket', 'required|is_unique[paket.nama_paket]|alpha');
		$this->form_validation->set_rules('harga', 'Harga', 'required|integer');
		$this->form_validation->set_rules('country', 'Country', 'required');
		$this->form_validation->set_rules('diskon', 'Diskon', 'required|integer');
		$this->form_validation->set_rules('durasi', 'Durasi', 'required|integer');
		$this->form_validation->set_rules('keterangan', 'keterangan', 'required');

		if ($this->form_validation->run() == FALSE){
            $this->add_paket();   
        }else{
                 $config['upload_path'] = '././assets/images/'; //path folder
				 $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang image yang dizinkan
				 $config['encrypt_name'] = TRUE; //enkripsi nama file
				       
		         $this->upload->initialize($config);

		        if(!empty($_FILES['img']['name'])){
		        	if ($this->upload->do_upload('img')){
			        	$gbr = $this->upload->data();
			            $img = $gbr['file_name'];
			            //Compress Image
			            $this->_create_thumbs($img);

			            $title = "img_paket";
			            $image_name = $gbr['file_name'];
			            // $image_medium = $gbr['file_name'];
			            // $image_small = $gbr['file_name'];
			            
						
			            $data = array(
			        	'nama_paket' => $this->input->post("nama_paket"),
			        	'country_code' => $this->input->post("country"),
			        	'gambar_paket' => $image_name,
			        	'durasi' => $this->input->post("durasi"),
			        	'keterangan' => $this->input->post("keterangan")
			             );

						$field1 = "paket";
						$sukses = $this->global_models->insert_data($field1,$data);

						if ($sukses) {
					      	switch (strlen($sukses->no_paket)) {
			                case 1:
			                 $digit = 2;
			                break;
			                case 2:
			                 $digit = 1;
			                break;
			                case 3:
			                 $digit = 0;
			                break;
			                default:
			                  $digit = 0;
			                break;
		                    }

							$prefix = "P";
		                    $id_paket = $prefix.str_repeat("0", $digit).$sukses->no_paket;

		                    //insert data harga
		                    $data_harga = array(
					            'id_paket'      => $id_paket,
					            'tax' => $this->input->post("tax"),
					        	'other' => $this->input->post("other"),
					        	'diskon' => $this->input->post("diskon"),
					            'jumlah'   		=> $this->input->post('harga'),
					            'created_at'  	=> date("Y-m-d"),
					      	);
					      	$field3 = "harga";
					      	$harga = $this->global_models->insert_data($field3,$data_harga);

					      	if ($harga) {
					      	switch (strlen($harga->no_hr)) {
			                case 1:
			                 $digit = 2;
			                break;
			                case 2:
			                 $digit = 1;
			                break;
			                case 3:
			                 $digit = 0;
			                break;
			                default:
			                  $digit = 0;
			                break;
		                    }

			                $prefix = "HR";
		                    $id_harga = $prefix.str_repeat("0", $digit).$harga->no_hr; 

		                    //Generate primary key harga
		                    $this->db->set('id_harga',$id_harga);
		                    $this->db->where('no_hr', $harga->no_hr);
		                    $this->db->update('harga');

					      	$data_update = array(
			                    'id_paket'   => $id_paket,
		                    	);
					      	 //Generate primary paket
		                    $this->db->set($data_update);
		                    $this->db->where('no_paket', $sukses->no_paket);
		                    $this->db->update('paket');
						 	$this->session->set_flashdata('msg', 'Data paket berhasil diinput. ');
						 	$this->add_paket();
							}
		        	}else{
		            echo $this->upload->display_errors();
		        	}	 
		        }else{
		        	$this->session->set_flashdata('img','<div class="alert alert-info">Image kosong atau type image tidak di izinkan</div>');
		            redirect('paket/add_paket');
		    	}
		    //buat data paket pakai gambar
			}else{
				$image_name = null;
				$data = array(
			        	'nama_paket' => $this->input->post("nama_paket"),
			        	'country_code' => $this->input->post("country"),
			        	'gambar_paket' => $image_name,
			        	'durasi' => $this->input->post("durasi"),
			        	'keterangan' => $this->input->post("keterangan")
			             );

				$field1 = "paket";
				$sukses = $this->global_models->insert_data($field1,$data);

					if ($sukses) {
				      	switch (strlen($sukses->no_paket)) {
		                case 1:
		                 $digit = 2;
		                break;
		                case 2:
		                 $digit = 1;
		                break;
		                case 3:
		                 $digit = 0;
		                break;
		                default:
		                  $digit = 0;
		                break;
	                    }

						$prefix = "P";
	                    $id_paket = $prefix.str_repeat("0", $digit).$sukses->no_paket;

	                    //insert data harga
	                    $data_harga = array(
				            'id_paket'      => $id_paket,
				            'tax' => $this->input->post("tax"),
				        	'other' => $this->input->post("other"),
				        	'diskon' => $this->input->post("diskon"),
				            'jumlah'   		=> $this->input->post('harga'),
				            'created_at'  	=> date("Y-m-d"),
				      	);
				      	$field3 = "harga";
				      	$harga = $this->global_models->insert_data($field3,$data_harga);

				      	if ($harga) {
				      	switch (strlen($harga->no_hr)) {
		                case 1:
		                 $digit = 2;
		                break;
		                case 2:
		                 $digit = 1;
		                break;
		                case 3:
		                 $digit = 0;
		                break;
		                default:
		                  $digit = 0;
		                break;
	                    }

		                $prefix = "HR";
	                    $id_harga = $prefix.str_repeat("0", $digit).$harga->no_hr; 

	                    //Generate primary key harga
	                    $this->db->set('id_harga',$id_harga);
	                    $this->db->where('no_hr', $harga->no_hr);
	                    $this->db->update('harga');

				      	$data_update = array(
		                    'id_paket'   => $id_paket,
	                    	);
				      	 //Generate primary paket
	                    $this->db->set($data_update);
	                    $this->db->where('no_paket', $sukses->no_paket);
	                    $this->db->update('paket');
					 	$this->session->set_flashdata('msg', 'Data paket berhasil diinput. ');
					 	$this->add_paket();
						}
	        	}else{
	            echo $this->upload->display_errors();
	        	}

				//buat data paket tanpa gambar
			}
		}
	}		 

	public function do_edit_paket(){
		$id = $this->uri->segment(3);
		$this->db->select("*");
      	$this->db->from('paket');
      	$this->db->join('harga', 'paket.id_paket = harga.id_paket','left');
      	$this->db->where('paket.id_paket = "'.$id.'"');
      	$this->db->where('deleted_at', null);
      	$query = $this->db->get();

     	$current_data = $query->row();
		
		$this->form_validation->set_rules('nama_paket', 'Nama Paket', 'required');
		$this->form_validation->set_rules('harga', 'Harga', 'required');
		$this->form_validation->set_rules('country', 'Country', 'required');
		$this->form_validation->set_rules('diskon', 'Diskon', 'required');
		$this->form_validation->set_rules('durasi', 'Durasi', 'required');
		$this->form_validation->set_rules('keterangan', 'keterangan', 'required');

		if ($this->form_validation->run() == FALSE){
            redirect('paket/edit_paket/'.$id);
        }else{
        	$old_img = $this->input->post('old_img');
		    $config['upload_path'] = '././assets/images/'; //path folder
			$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang image yang dizinkan
			$config['encrypt_name'] = TRUE; //enkripsi nama file
			$this->upload->initialize($config);
			if(!empty($_FILES['img']['name'])){
				if ($this->upload->do_upload('img')){
					$gbr = $this->upload->data();
		            $img = $gbr['file_name'];
		            //Compress Image
		            $this->_create_thumbs($img);

		            $image_name = $gbr['file_name'];

		            $data = array(
			        	'nama_paket' => $this->input->post("nama_paket"),
			        	'country_code' => $this->input->post("country"),
			        	'gambar_paket' => $image_name,
			        	'durasi' => $this->input->post("durasi"),
			        	'keterangan' => $this->input->post("keterangan")
			             );
				   
				    $target = "assets/images/".$current_data->gambar_paket;
				   	unlink($target);
				}else{
					//jika upload gagal
					$this->session->set_flashdata('img','<div class="alert alert-info">Image kosong atau type image tidak di izinkan</div>');
					redirect('paket/edit_paket/'.$id);
				}
			}else{
				//jika tidak ada file yang di upload
				$data = array(
		        	'nama_paket' => $this->input->post("nama_paket"),
		        	'country_code' => $this->input->post("country"),
		        	'durasi' => $this->input->post("durasi"),
		        	'keterangan' => $this->input->post("keterangan")
		            );
			}
			
			//create harga baru
			if ($current_data->jumlah != $this->input->post("harga") || $current_data->tax != $this->input->post("tax") || $current_data->other != $this->input->post("other") || $current_data->diskon != $this->input->post("diskon")) {
				$data_harga = array(
		            'id_paket'      => $id,
		            'tax' => $this->input->post("tax"),
		        	'other' => $this->input->post("other"),
		        	'diskon' => $this->input->post("diskon"),
		            'jumlah'   		=> $this->input->post('harga'),
		            'created_at'  	=> date("Y-m-d"),
		      	);

		      	$field3 = "harga";
		      	$harga = $this->global_models->insert_data($field3,$data_harga);

		      	switch (strlen($harga->no_hr)) {
		                case 1:
		                 $digit = 2;
		                break;

		                case 2:
		                 $digit = 1;
		                break;

		                case 3:
		                 $digit = 0;
		                break;

		                default:
		                  $digit = 0;
		                break;
		            }
		        $prefix = "HR";
                $id_harga = $prefix.str_repeat("0", $digit).$harga->no_hr;

                $this->db->set('id_harga',$id_harga);
                $this->db->where('no_hr', $harga->no_hr);
                $this->db->update('harga');

                $data_update = array(
                	'deleted_at' => date("Y-m-d"),
                );

                $this->db->set($data_update);
                $this->db->where('id_harga', $current_data->id_harga);
                $this->db->update('harga');
			}

		   $query = $this->db->update('paket',$data,array('id_paket' => $id));

		   if ($query) {
			 	$this->session->set_flashdata('edit', 'Data paket berhasil diedit');
			 	 redirect('paket/edit_paket/'.$id);
			}else{
        	$this->session->set_flashdata('edit','Gagal mengedit data paket.');
             redirect('paket/edit_paket/'.$id);
			}
        }
	}
	
	public function delete_paket(){
		$id = $this->uri->segment(3);
		$this->db->delete('paket', array('id_paket' => $id));
		$this->db->delete('harga', array('id_paket' => $id));

		$this->list();
	}

	public function delete_image(){
		$id = $this->uri->segment(3);
		$id_paket = $this->uri->segment(4);
		$target = "assets/images/".$id;
		$query = $this->db->delete('paket', array('gambar_paket' => $id));
		if($query){
			unlink($target);
		}
		redirect('paket/edit_paket/'.$id_paket);
	}
}