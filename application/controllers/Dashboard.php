<?php

/**
 * 
 */
class Dashboard extends MY_Controller
{
	
	function __construct()
	{
		parent:: __construct();

		$this->cekLogin();

		$this->load->model('global_models');

		$this->load->model('user_models');

		$this->load->library('form_validation');
	}

	public function index(){
		$data= array("menu_back" => "menu_home");
			
		$this->session->set_userdata($data);

		$this->content = 'content/vWelcome';
		$this->layout();
	}

	
	public function setting(){
		$is_login = $this->session->userdata('is_login');
		if ($is_login === TRUE) {
			$data['user'] = $this->global_models->getTable('user'); 
			$this->load->view('view_dashboard');
			$this->load->view('v_isi',$data);
		}else{
			redirect('login_baru');
		}
	}

	public function cekLogin(){
		$is_login = $this->session->userdata('is_login');
		$permission = $this->session->userdata('permission');

		if ($is_login === TRUE && $permission != "CO" ) {
			
		}else{
			redirect('login','refresh');
		}
	}
}
?>