<?php
/**
 * 
 */
class Invoice extends CI_Controller
{
	
    function __construct() {
        parent::__construct();
        $this->load->library('pdf');
        $this->load->model('global_models');
    }
    
    function index(){
    	$this->print();
    	
    }

    public function generate_pk($prefix,$number){
        switch (strlen($number)) {
            case 1:
                $digit = 2;
            break;

            case 2:
                $digit = 1;
            break;

            case 3:
                $digit = 0;
            break;

            default:
                $digit = 0;
            break;
        }
        $primary_key = $prefix.str_repeat("0", $digit).$number;
        return $primary_key; 
    }

    function approve() {
        $id_header = $this->uri->segment(3);
        $nik = $this->uri->segment(4);

        $this->db->select("*");
        $this->db->from('detail_pemesanan');
        $this->db->join('header_pemesanan', 'detail_pemesanan.id_header_pemesanan = header_pemesanan.id_header_pemesanan','left');
        $this->db->join('paket', 'detail_pemesanan.id_paket = paket.id_paket','left');
        $this->db->where('header_pemesanan.id_header_pemesanan = "'.$id_header.'"');
        $query = $this->db->get();

        $current_pemesanan = $query->row();

        $this->db->select("*");
        $this->db->from('visa');
        $this->db->join('header_pemesanan', 'visa.id_header_pemesanan = header_pemesanan.id_header_pemesanan','left');
        $this->db->join('detail_pemesanan', 'detail_pemesanan.id_header_pemesanan = header_pemesanan.id_header_pemesanan','left');
        $this->db->join('paket', 'detail_pemesanan.id_paket = paket.id_paket','left');
        $this->db->where('paket.country_code = "'.$current_pemesanan->country_code.'"');
        $this->db->where('header_pemesanan.nik = "'.$current_pemesanan->nik.'"');
        $query2 = $this->db->get();

        $current_visa = $query2->result();

        if($current_pemesanan->country_code == "ID" || !empty($current_visa) ){

        }else{
            $visa = array(
                'nik' => $nik,
                'id_header_pemesanan' => $id_header,
                'issuing_office' => "Jakarta",                   
                'status' => 0
                );

            $sukses = $this->global_models->insert_data('visa',$visa);
            if($sukses){
                $prefix = "V";
                $pk = $this->generate_pk($prefix,$sukses->no_visa);
                
                $this->db->set('id_visa',$pk);
                $this->db->where('no_visa', $sukses->no_visa);
                $this->db->update('visa');
            }
        }

        $data = array(                   
                'status' => 1
                );

        $approve = $this->db->update('header_pemesanan',$data,array('id_header_pemesanan' => $id_header));
        

        if($approve){
           
          
            $this->session->set_flashdata('msg', 'Berhasil mengkonfirmasi pembayaran');
            redirect('pemesanan/detail_pemesanan/'.$id_header);
        }else{
            $this->session->set_flashdata('msg', 'Gagal mengkonfirmasi pembayaran. ');
            redirect('pemesanan/detail_pemesanan/'.$id_header);
        }
        
    }

    function cancel() {
        $id_header = $this->uri->segment(3);
        $nik = $this->uri->segment(4);
        $visa = $this->uri->segment(5);

        $data = array(                   
                'status' => 0
                );

        $approve = $this->db->update('header_pemesanan',$data,array('id_header_pemesanan' => $id_header));

        $sukses = $this->db->delete('visa', array('id_header_pemesanan' => $id_header,'id_visa' => $visa));

        if($sukses){
            $this->session->set_flashdata('msg', 'Berhasil membatalkan bukti pembayaran');
            redirect('pemesanan/detail_pemesanan/'.$id_header);
        }else{
            $this->session->set_flashdata('msg', 'Gagal membatalkan bukti pembayaran. ');
            redirect('pemesanan/detail_pemesanan/'.$id_header);
        }
        
    }

    function print(){
    	$id = $this->uri->segment(3);

		$this->db->select("*");
    	$this->db->from('detail_pemesanan');
    	$this->db->join('header_pemesanan', 'detail_pemesanan.id_header_pemesanan = header_pemesanan.id_header_pemesanan','left');
    	$this->db->join('paket','detail_pemesanan.id_paket = paket.id_paket','left');
        $this->db->join('harga','harga.id_harga = detail_pemesanan.id_harga','left');
    	$this->db->join('jadwal','detail_pemesanan.id_jadwal = jadwal.id_jadwal','left');
    	$this->db->where('detail_pemesanan.id_header_pemesanan = "'.$id.'"');      	
     	$query = $this->db->get();

     	$this->db->select("*");
    	$this->db->from('header_pemesanan');
    	$this->db->join('pelanggan', 'header_pemesanan.nik = pelanggan.nik','left');
        $this->db->join('detail_pemesanan','header_pemesanan.id_header_pemesanan = detail_pemesanan.id_header_pemesanan','left');
        $this->db->join('paket','detail_pemesanan.id_paket = paket.id_paket','left');
        $this->db->join('harga','harga.id_harga = detail_pemesanan.id_harga','left');
    	$this->db->join('user','pelanggan.id_user = user.id_user','left');
    	$this->db->where('header_pemesanan.id_header_pemesanan = "'.$id.'"');     
    	$query2 = $this->db->get();

    	$this->db->select('subtotal,diskon,tax,other,grandtotal');
    	$this->db->from('header_pemesanan');
    	$this->db->where('header_pemesanan.id_header_pemesanan = "'.$id.'"'); 

     	$this->data['header_pemesanan'] = $query2->result();
     	$this->data['detail_pemesanan'] = $query->result();
     	
     	//tanggal
     	$date = explode("-",$this->data['header_pemesanan'][0]->tanggal_pesan);
  		$implode = array($hari  = $date[2],$bulan = $date[1],$tahun = $date[0]);
  		$tanggal = implode("-", $implode);

        $date_now = date("Y-m-d");
        $date_now = explode("-",$date_now);
        $implode = array($hari  = $date_now[2],$bulan = $date_now[1],$tahun = $date_now[0]);
        $date_now = implode("-", $implode);



        $pdf = new FPDF('l','mm','A5');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','B',16);
        // mencetak string 
        $pdf->Cell(190,7,'SAMAWA TOUR AND TRAVEL INVOICE',0,1,'C');
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(190,7,'DETAIL PEMESANAN PAKET PERJALANAN TANGGAL '.$tanggal,0,1,'C');
        $pdf->Cell(190,7,'ATAS NAMA : '.$this->data['header_pemesanan'][0]->username,0,1,'C');
        
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10,7,'',0,1);
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(20,6,'NO',1,0);
        $pdf->Cell(52,6,'PAKET',1,0);
        $pdf->Cell(52,6,'JADWAL KEBERANGKATAN',1,0);
        $pdf->Cell(20,6,'JUMLAH',1,0);
        $pdf->Cell(22,6,'HARGA',1,0);
        $pdf->Cell(22,6,'TOTAL',1,1);

        $pdf->SetFont('Arial','',10);
        $no = 1;
        
        foreach ($this->data['detail_pemesanan'] as $row){
        	$pdf->Cell(20,6,$no,1,0,'C');
            $pdf->Cell(52,6,$row->nama_paket,1,0,'C');
            $pdf->Cell(52,6,$row->tgl_keberangkatan,1,0);
            $pdf->Cell(20,6,$row->pax,1,0);
            $pdf->Cell(22,6,$row->jumlah,1,0);
            $pdf->Cell(22,6,($row->pax * $row->jumlah),1,1); 
            $no++;
        }

		
        $pdf->Cell(166,6,"SUBTOTAL",1,0);
        $pdf->Cell(22,6,$this->data['header_pemesanan'][0]->subtotal,1,1);
        $pdf->Cell(166,6,"DISKON",1,0);
        $pdf->Cell(22,6,$this->data['header_pemesanan'][0]->diskon,1,1);
        $pdf->Cell(166,6,"TAX(PPN)",1,0);
        $pdf->Cell(22,6,$this->data['header_pemesanan'][0]->tax,1,1); 
        $pdf->Cell(166,6,"OTHER",1,0);
        $pdf->Cell(22,6,$this->data['header_pemesanan'][0]->other,1,1);
        $pdf->Cell(166,6,"GRAND TOTAL",1,0);
        $pdf->Cell(22,6,$this->data['header_pemesanan'][0]->grand_total,1,1);

        $pdf->Cell(10,7,'',0,1);
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(330,7,"Jakarta, ".$date_now,0,'R','C');
        $pdf->Cell(10,22,'',0,1,'C');
        $pdf->Cell(330,7,"SAMAWA MANAGEMENT",0,'R','C');
        $pdf->Output();
    }
}

?>