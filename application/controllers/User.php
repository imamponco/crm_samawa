<?php defined('BASEPATH') or exit('No direct script access allowed.');
/**
 * 
 */
require('Dashboard.php');

class User extends Dashboard
{
	public $filter;

	function __construct()
	{
		parent:: __construct();
	}

	public function index(){
			$this->list();	
	}

	public function add_user(){
		$this->data['user'] = $this->global_models->getTable('user');
		$this->content = 'content/user/vInputUser';
		$this->layout();
	}

	public function generate_pk($prefix,$number){
		switch (strlen($number)) {
		    case 1:
		    	$digit = 2;
		    break;

		    case 2:
		        $digit = 1;
		    break;

		    case 3:
		        $digit = 0;
		    break;

		    default:
		        $digit = 0;
		    break;
		}
        $primary_key = $prefix.str_repeat("0", $digit).$number;
        return $primary_key; 
	}

	public function do_add_user(){
		$this->form_validation->set_rules('nama_user', 'Nama Lengkap', 'required');
		$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
		$this->form_validation->set_rules('no_telp_user', 'Nomor Telepon', 'required|is_unique[user.no_telp_user]');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');

		if ($this->form_validation->run() == FALSE)
                {
                        $this->add_user();   
                }
                else{

				 $data = array(
		        	'nama_user' => $this->input->post("nama_user"),
		        	'jenis_kelamin_user' => $this->input->post("jenis_kelamin"),
		        	'no_telp_user' => $this->input->post("no_telp_user"),
		        	'alamat_user' => $this->input->post("alamat")
		             );

				 $field = "user";
				 $sukses = $this->global_models->insert_data($field,$data);
				 
				 if ($sukses) {
				 	$prefix = "AG";
				 	$pk = $this->generate_pk($prefix,$sukses->no_user);

				 	$this->db->set('id_user',$pk);
	                $this->db->where('no_user', $sukses->no_user);
	                $this->db->update('user');

				 	$this->session->set_flashdata('msg', 'Data user berhasil diinput. ');
				 	$this->add_user();
				 }
			}
	}

	public function list(){
		if ($this->filter === "0") {
			$filter = $this->filter;
		}else{
			$filter = $this->input->post('filter');
		}

		$data= array("menu_back" => "menu_user");
			
		$this->session->set_userdata($data);
		

		if($filter==1){
			$this->db->select("*");
	      	$this->db->from('user');
	      	$this->db->where('verified_at',null);
	      	$this->db->order_by('id_user','desc');
	      	$query = $this->db->get();

			$this->data['user'] = $query->result();

			$this->content = 'content/user/vDataUser';
			$this->layout();
		}else if($filter===NULL){
			$this->db->select("*");
	      	$this->db->from('user');
	      	$this->db->where('verified_at',null);
	      	$this->db->order_by('id_user','desc');
	      	$query = $this->db->get();

			$this->data['user'] = $query->result();

			$this->content = 'content/user/vDataUser';
			$this->layout();
		}else if($filter==="0"){
			$this->db->select("*");
	      	$this->db->from('user');
	      	$this->db->where('verified_at is NOT NULL', NULL, FALSE);
	      	$this->db->order_by('id_user','desc');
	      	$query = $this->db->get();

			$this->data['user'] = $query->result();

			$this->content = 'content/user/vDataUser';
			$this->layout();
		}
		
	}

	public function edit_user(){
		$id = $this->uri->segment(3);

		$this->db->select("*");
      	$this->db->from('user');
      	$this->db->where('user.id_user = "'.$id.'"');
      	
     	$query = $this->db->get();

     	$this->data['user'] = $query->result();
		$this->content = 'content/user/vEditDataUser';
		$this->layout();
	}

	public function verify(){
		 $id = $this->uri->segment(3);
		 date_default_timezone_set("Asia/Bangkok");
		 $data = array('verified_at' => date("Y-m-d h:i:sa"));
				
		 $query = $this->db->update('user',$data,array('id_user' => $id));
		 if ($query) {
		 	$this->session->set_flashdata('verify', 'Berhasil memverifikasi data user.');
		 	redirect('user/list');
		 }
	}

	public function do_edit_user(){

		$id = $this->uri->segment(3);
		$this->form_validation->set_rules('username', 'Full Name', 'required');

        $this->db->select("email");
      	$this->db->from('user');  
      	$this->db->where('id_user',$id); 	
     	$current_data = $this->db->get()->row();

     	$this->db->select("email");
      	$this->db->from('user');
     	$validasi_data =$this->db->get()->result();
		
		if ($this->form_validation->run() == FALSE)
                {
                   redirect('user/edit_user/'.$id);
                }
                else{
               	//validasi password
                if(!empty($this->input->post("password"))){
                	if ($this->input->post("password")==$this->input->post("repassword")) {
                		$password =  password_hash($this->input->post("password"), PASSWORD_DEFAULT);
                	}else{
            		$this->session->set_flashdata('error', 'Password dan Re-Password tidak mirip.');
			 		redirect('user/edit_user/'.$id);
                	}	
                } 

                //validasi email
                if(!empty($this->input->post("email"))){
                	if($this->input->post("email")==$current_data->email){
                		$email = $this->input->post("email");
                	}else{

	                	foreach ($validasi_data as $row) {
	                		if ($this->input->post("email")==$row->email) {
	                			$email_error = 1;
	                		}
	                	}

	                	if(isset($email_error)){
	                		$this->session->set_flashdata('error', 'Email telah digunakan.');
				 			redirect('user/edit_user/'.$id);
	                	}else{
	                		$email = $this->input->post("email");
	                	}

                	}
                }

                if(empty($this->input->post("email"))&&empty($this->input->post("password"))){
                	$data = array(
		                'username' => $this->input->post("username"),
		             );
                }else if(empty($this->input->post("password"))){
                	$data = array(
		                'username' => $this->input->post("username"),
		                'email'    => $email
		             );
                }else{
                	$data = array(
		                'username' => $this->input->post("username"),
		                'email'    => $email,
		                'password' => $password
		             );
                }
				
			    $query = $this->db->update('user',$data,array('id_user' => $id));
			   
			   	 if ($query) {
			   	 	// $this->session->set_userdata('nama',$this->input->post('username'));
			   	 	$this->session->set_flashdata('edit', 'Data user berhasil diedit');
				 	redirect('user/edit_user/'.$id);
				 }
			}

	}

	public function delete_user(){
		$id = $this->uri->segment(3);
		$this->db->delete('user', array('id_user' => $id));
		$this->db->delete('pelanggan', array('id_user' => $id));

		$this->filter = "0";
		
		$this->list();
	}

	public function json(){
		$this->load->library('datatables');
        $this->datatables->select('*');
        $this->datatables->from('user');
        return print_r($this->datatables->generate());
	}
	

}