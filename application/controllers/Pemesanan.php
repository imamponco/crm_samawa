<?php defined('BASEPATH') or exit('No direct script access allowed.');
/**
 * 
 */
require('Dashboard.php');

class Pemesanan extends Dashboard
{
	function __construct()
	{
		parent:: __construct();
	}

	public function index(){
			$this->list();	
	}

	public function add_pemesanan(){
		$this->data['pelanggan'] = $this->global_models->getTable('pelanggan');
		$this->data['user'] = $this->global_models->getTable('user');
		$this->data['paket'] = $this->global_models->getTable('paket');

		// $this->db->select("*");
		// $this->db->from('paket');
		// $this->db->join('jadwal', 'paket.id_paket = jadwal.id_paket','left');
		// $this->db->where('jadwal.id_paket is not null AND status = "belum"');
		// $query = $this->db->get();
		// $this->data['paket'] = $query->result();

		

		$this->content = 'content/pemesanan/vInputPemesanan1';
		$this->layout();
	}

	public function do_add_pemesanan(){

		$this->form_validation->set_rules('nik', 'Nama Pelanggan', 'required');
		$this->form_validation->set_rules('tgl_pesan', 'Tanggal Pemesanan', 'required');
		$this->form_validation->set_rules('id_paket[]', 'Paket', 'required');
		$this->form_validation->set_rules('id_jadwal[]', 'jadwal', 'required');
		$this->form_validation->set_rules('jumlah[]', 'Jumlah', 'required');
		$this->form_validation->set_rules('harga[]', 'Harga', 'required');
		$this->form_validation->set_rules('total[]', 'Total', 'required');


		if ($this->form_validation->run() == FALSE)
                {
                        $this->add_pemesanan();   
                }
                else{

            	$nik = $this->input->post("nik");
            	$id_paket = $this->input->post("id_paket[]");
            	$id_jadwal = $this->input->post("id_jadwal[]");
            	$jumlah = $this->input->post("jumlah[]");
            	$harga = $this->input->post("harga[]");
            	$total = $this->input->post("total");
            	$tgl_pesan = $this->input->post("tgl_pesan");

            	$hari = substr($tgl_pesan,8, 2);
            	$bulan = substr($tgl_pesan, 5, 2);
				$tahun = substr($tgl_pesan,0, 4);
				//2019-02-06
            	
            	$date = implode(array($tahun,$bulan,$hari));
            	$jml = count($id_paket);
            	$trans= "TRANS";
            	$det = "DET";

            	//get id header
            	$this->db->select_max("id_header_pemesanan");
		      	$this->db->from('header_pemesanan');
		      	$this->db->like('id_header_pemesanan',$date);      	
		     	$query = $this->db->get();
		     	$hasilheader = $query->row('id_header_pemesanan');

		     	if ($hasilheader != NULL) {
		     		$no2 = intval(substr($hasilheader, 11,1))+1;
		     		$id_header_pemesanan = implode(array($det,$date,$no2));
		     	}else{
		     		$no2 = 1;
		     		$id_header_pemesanan = implode(array($det,$date,$no2));
		     	}

		     	//get id detail
		     	$this->db->select_max("id_detail_pemesanan");
		      	$this->db->from('detail_pemesanan');
		      	$this->db->like('id_detail_pemesanan',$date);      	
		     	$query = $this->db->get();
		     	$detail = $query->row('id_detail_pemesanan');

		     	if ($detail != NULL) {
		     		$no1 = intval(substr($detail, 13,1))+1;
		     	}else{
		     		$no1 = 1;
		     	}
			    

			    //header pemesanan
	     		$data2 = array(
			  	'id_header_pemesanan' => $id_header_pemesanan,
			  	'nik' => $this->input->post("nik"),
	        	'id_user' => $this->input->post("id_user"),
			  	'tanggal_pesan' => $this->input->post("tgl_pesan"),
            	'subtotal' => $this->input->post("subtotal"),
            	'diskon' => $this->input->post("diskon"),
            	'tax' => $this->input->post("tax"),
            	'other' => $this->input->post("other"),
            	'grand_total' => $this->input->post("grandtotal")
            	);
	            

	          
                $field = "header_pemesanan";
            	$sukses2 = $this->global_models->insert_data($field,$data2);


			    //tambah detail pemesanan
                for ($x=0; $x < ($jml) ; $x++) {
                
                $id_detail_pemesanan = implode(array($trans,$date,$no1));
				 $data1 = array(
				 	'id_header_pemesanan' => $id_header_pemesanan,
		        	'id_detail_pemesanan' => $id_detail_pemesanan,
		        	'id_paket' => $id_paket[$x],
		        	'id_jadwal' => $id_jadwal[$x],
		        	'jumlah' => $jumlah[$x],
		        	'harga' => $harga[$x],
		        	'total' => $total[$x]

		             );
				
				 $no1++;
				 $field = "detail_pemesanan";
				 $sukses = $this->global_models->insert_data($field,$data1);
				

				 }
				
				
				 

				 if ($sukses&&$sukses2) {
				 	$this->session->set_flashdata('msg', 'Data pemesanan berhasil diinput');
				 	$this->add_pemesanan();
				 }
			}
	}

	public function list(){
		if (!empty($this->input->post())) {
			$nama 			= $this->input->post("nama");
			$agen	   		= $this->input->post("agen");
			$first_date 	= $this->input->post("first_date");
			$end_date 		= $this->input->post("end_date");

			$this->db->select("*");
	      	$this->db->from('header_pemesanan');
	      	$this->db->join('pelanggan', 'header_pemesanan.nik = pelanggan.nik','left');
	      	$this->db->join('detail_pemesanan', 'header_pemesanan.id_header_pemesanan = detail_pemesanan.id_header_pemesanan','left');
	      	$this->db->join('paket', 'detail_pemesanan.id_paket = paket.id_paket','left');
	      	$this->db->join('user', 'pelanggan.id_user = user.id_user','left');
	      	$this->db->join('agen', 'pelanggan.id_agen = agen.id_agen','inner');
			$this->db->where('tanggal_pesan BETWEEN "'.$first_date. '" AND "'.$end_date.'"');
			$this->db->or_where('nama_agen',$agen);
			$this->db->or_where('username',$nama);
			$this->db->order_by('header_pemesanan.id_header_pemesanan','desc');

	      	$query = $this->db->get();

			$this->data['pemesanan'] = $query->result();

			$data= array("menu_back" => "menu_pemesanan");
			
			$this->session->set_userdata($data);

			$this->content = 'content/pemesanan/vDataPemesanan';
			$this->layout();
		}else{
			
			$this->db->select("*");
	      	$this->db->from('header_pemesanan');
	      	$this->db->join('pelanggan', 'header_pemesanan.nik = pelanggan.nik','left');
	      	$this->db->join('detail_pemesanan', 'header_pemesanan.id_header_pemesanan = detail_pemesanan.id_header_pemesanan','left');
	      	$this->db->join('paket', 'detail_pemesanan.id_paket = paket.id_paket','left');
	      	$this->db->join('user', 'pelanggan.id_user = user.id_user','left');
	      	$this->db->join('agen', 'pelanggan.id_agen = agen.id_agen','inner');
	      	$this->db->order_by('header_pemesanan.id_header_pemesanan','desc');
	      	$query = $this->db->get();

			$this->data['pemesanan'] = $query->result();

			$data= array("menu_back" => "menu_pemesanan");
			
			$this->session->set_userdata($data);

			$this->content = 'content/pemesanan/vDataPemesanan';
			$this->layout();
		}

		
	}

	public function detail_pemesanan(){
		$id = $this->uri->segment(3);
		$this->db->select("*");
      	$this->db->from('detail_pemesanan');
      	$this->db->join('header_pemesanan', 'detail_pemesanan.id_header_pemesanan = header_pemesanan.id_header_pemesanan','left');
      	$this->db->join('paket','detail_pemesanan.id_paket = paket.id_paket','left');
      	$this->db->join('harga','paket.id_paket = harga.id_paket','left');
      	$this->db->join('jadwal','detail_pemesanan.id_jadwal = jadwal.id_jadwal','left');
      	$this->db->where('header_pemesanan.id_header_pemesanan = "'.$id.'"');      	
     	$this->db->where('harga.deleted_at',null);
     
     	$query = $this->db->get();

     	$this->db->select("*,header_pemesanan.status as flag,pelanggan.nik,visa.id_visa");
      	$this->db->from('header_pemesanan');
      	$this->db->join('pelanggan', 'header_pemesanan.nik = pelanggan.nik','left');
      	$this->db->join('detail_pemesanan', 'header_pemesanan.id_header_pemesanan = detail_pemesanan.id_header_pemesanan','left');
      	$this->db->join('paket','detail_pemesanan.id_paket = paket.id_paket','left');
      	$this->db->join('harga','detail_pemesanan.id_harga = harga.id_harga','left');
      	$this->db->join('visa', 'header_pemesanan.id_header_pemesanan = visa.id_header_pemesanan','left');
      	$this->db->join('user','pelanggan.id_user = user.id_user','left');
      	$this->db->where('header_pemesanan.id_header_pemesanan = "'.$id.'"');     
      	$this->db->where('harga.deleted_at',null);
      	
      	$query2 = $this->db->get();

     	$this->data['header_pemesanan'] = $query2->result();
     	$this->data['detail_pemesanan'] = $query->result();

     	$this->content = 'content/pemesanan/vDetailPemesanan';
     	$this->layout();
	}

	public function edit_pemesanan(){
		$id = $this->uri->segment(3);

		$this->db->select("*");
      	$this->db->from('detail_pemesanan');
      	$this->db->join('header_pemesanan', 'header_pemesanan.id_header_pemesanan = detail_pemesanan.id_header_pemesanan','right');
      	$this->db->join('paket','detail_pemesanan.id_paket = paket.id_paket','left');
      	$this->db->join('harga', 'paket.id_paket = harga.id_paket','left');
      	$this->db->join('jadwal','detail_pemesanan.id_jadwal = jadwal.id_jadwal','left');
      	$this->db->where('header_pemesanan.id_header_pemesanan = "'.$id.'"');      	
     	$this->db->where('harga.deleted_at',null);
     	
     	$query = $this->db->get();

     	$this->db->select("*");
      	$this->db->from('header_pemesanan');
      	$this->db->join('pelanggan', 'header_pemesanan.nik = pelanggan.nik','left');
      	$this->db->join('detail_pemesanan', 'header_pemesanan.id_header_pemesanan = detail_pemesanan.id_header_pemesanan','left');
      	$this->db->join('user','pelanggan.id_user = user.id_user','left');
      	$this->db->join('paket','detail_pemesanan.id_paket = paket.id_paket','left');
      	$this->db->join('harga', 'paket.id_paket = harga.id_paket','left');
      	$this->db->where('header_pemesanan.id_header_pemesanan = "'.$id.'"');     
      	$this->db->where('harga.deleted_at',null);
      	
      	$query2 = $this->db->get();

      	$this->data['pelanggan'] = $this->global_models->getTable('pelanggan');
      	$this->data['paket'] = $this->global_models->getTable('paket');
     	$this->data['header_pemesanan'] = $query2->result();
     	$this->data['detail_pemesanan'] = $query->result();

     	// var_dump($this->data['detail_pemesanan']);
     	// die;

		$this->content = 'content/pemesanan/vEditDatapemesanan';
		$this->layout();
	}

	public function do_edit_pemesanan(){
		$id_header = $this->uri->segment(3);
		
		$this->form_validation->set_rules('nik', 'Nama Pelanggan', 'required');
		$this->form_validation->set_rules('tgl_pesan', 'Tanggal Pemesanan', 'required');
		$this->form_validation->set_rules('id_paket[]', 'Paket', 'required');
		$this->form_validation->set_rules('id_jadwal[]', 'jadwal', 'required');
		$this->form_validation->set_rules('jumlah[]', 'Jumlah', 'required');
		$this->form_validation->set_rules('harga[]', 'Harga', 'required');
		$this->form_validation->set_rules('total[]', 'Total', 'required');
		

		 if ($this->form_validation->run() == FALSE)
                {
                        $this->edit_pemesanan();
                       
                }
                else{
                $tranksasi_lama = count($this->input->post("id_transaksi[]"));

                $transaksi_baru =  count($this->input->post("id_paket"));

                $jml_baru = $transaksi_baru - $tranksasi_lama;

				$id_transaksi = $this->input->post("id_transaksi[]");
            	$nik = $this->input->post("nik");
            	$id_paket = $this->input->post("id_paket[]");
            	$id_jadwal = $this->input->post("id_jadwal[]");
            	$jumlah = $this->input->post("jumlah[]");
            	$harga = $this->input->post("harga[]");
            	$subtotal = $this->input->post("total");
            	$subtotal = $subtotal[0];
            	$grandtotal = $this->input->post("grandtotal");
            	
            	$terhitung = 0;
			   //edit detail pemesanan 
			     for ($x=0; $x < ($tranksasi_lama) ; $x++) {
			   	 $terhitung++;
				 $data1 = array( 
		        	'id_paket' => $id_paket[$x],
		        	'id_jadwal' => $id_jadwal[$x],
		        	'pax' => $jumlah[$x]
		             );
				 
				 $sukses = $this->db->update('detail_pemesanan',$data1,array('id_detail_pemesanan' => $id_transaksi[$x],
				 															 'id_header_pemesanan' => $id_header));
			    }
				
				 $data2 = array( 
		        	'subtotal' => $subtotal,
		        	'grand_total' => $grandtotal,
		             );

				
				$sukses2 = $this->db->update('header_pemesanan',$data2,array('id_header_pemesanan' => $id_header));
			  

			   	 if ($sukses && $sukses2) {
				 	$this->session->set_flashdata('edit', 'Data pemesanan berhasil diedit');
				 	header("Location: ".base_url()."Pemesanan/edit_pemesanan/".$id_header);
				 }

			}

	}

	public function delete_detail_pemesanan(){
		$id_transaksi = $this->uri->segment(3);
		$id_header = $this->uri->segment(4);
		$this->db->delete('header_pemesanan', array('id_header_pemesanan' => $id_header));
		$this->db->delete('visa', array('id_header_pemesanan' => $id_header));
		$sukses = $this->db->delete('detail_pemesanan', array('id_detail_pemesanan' => $id_transaksi));

		if($sukses){
		$this->session->set_flashdata('delete', 'Data berhasil dihapus');
		header("Location: ".base_url()."Pemesanan/list/");
		}
	}

	public function delete_pemesanan(){
		$id = $this->uri->segment(3);
		$this->db->delete('header_pemesanan', array('id_header_pemesanan' => $id));
		$this->db->delete('detail_pemesanan', array('id_header_pemesanan' => $id));
		$sukses = $this->db->delete('visa', array('id_header_pemesanan' => $id_header));

		if($sukses){
			$this->session->set_flashdata('delete', 'Data berhasil dihapus');
			header("Location: ".base_url()."Pemesanan/list/");
		}


		$this->list();
	}

}