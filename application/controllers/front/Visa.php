<?php defined('BASEPATH') or exit('No direct script access allowed.');
/**
 * 
 */
require('Index.php');

class Visa extends Index
{
	function __construct()
	{
		parent:: __construct();
	}

	public function index(){
			$this->list();
			$this->cekLogin();	
	}

	public function add_visa(){
		$this->data['visa'] = $this->global_models->getTable('visa');
		$this->content = 'content_front/visa/vInputvisa';
		$this->layout_front();
	}

	public function generate_pk($prefix,$number){
		switch (strlen($number)) {
		    case 1:
		    	$digit = 2;
		    break;

		    case 2:
		        $digit = 1;
		    break;

		    case 3:
		        $digit = 0;
		    break;

		    default:
		        $digit = 0;
		    break;
		}
        $primary_key = $prefix.str_repeat("0", $digit).$number;
        return $primary_key; 
	}

	public function do_add_visa(){
		$this->form_validation->set_rules('nama_visa', 'Nama Lengkap', 'required');
		$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
		$this->form_validation->set_rules('no_telp_visa', 'Nomor Telepon', 'required|is_unique[visa.no_telp_visa]');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');

		if ($this->form_validation->run() == FALSE)
                {
                        $this->add_visa();   
                }
                else{

				 $data = array(
		        	'nama_visa' => $this->input->post("nama_visa"),
		        	'jenis_kelamin_visa' => $this->input->post("jenis_kelamin"),
		        	'no_telp_visa' => $this->input->post("no_telp_visa"),
		        	'alamat_visa' => $this->input->post("alamat")
		             );

				 $field = "visa";
				 $sukses = $this->global_models->insert_data($field,$data);
				 
				 if ($sukses) {
				 	$prefix = "AG";
				 	$pk = $this->generate_pk($prefix,$sukses->no_visa);

				 	$this->db->set('id_visa',$pk);
	                $this->db->where('no_visa', $sukses->no_visa);
	                $this->db->update('visa');

				 	$this->session->set_flashdata('msg', 'Data visa berhasil diinput. ');
				 	$this->add_visa();
				 }
			}
	}

	public function list(){
		$id_user = $this->session->userdata('id_user_pelanggan');

		$this->db->select("*");
      	$this->db->from('visa');
      	$this->db->join('pelanggan', 'visa.nik = pelanggan.nik');
      	$this->db->join('user', 'pelanggan.id_user = user.id_user');
      	$this->db->where('pelanggan.id_user',$id_user);
      	$query = $this->db->get();

		$this->data['visa'] = $query->result();


		$this->content = 'content_front/visa/v_visa';
		$this->layout_front();

			
	}

	public function verify(){
		 $id = $this->uri->segment(3);
		 $value = $this->uri->segment(4);
	
		 $data = array('status' => $value);
				
		 $query = $this->db->update('visa',$data,array('id_visa' => $id));
		 if ($query) {
		 	$this->session->set_flashdata('verify', 'Berhasil mengubah data.');
		 	redirect('Visa/list');
		 }
	}

	public function edit_visa(){
		$id = $this->uri->segment(3);

		$this->db->select("*");
      	$this->db->from('visa');
      	$this->db->join('pelanggan', 'visa.nik = pelanggan.nik');
      	$this->db->where('visa.id_visa = "'.$id.'"');
      	
     	$query = $this->db->get();

     	$this->data['visa'] = $query->result();
		$this->content = 'content_front/visa/vEditDatavisa';
		$this->layout_front();
	}

	public function do_edit_visa(){

		$id = $this->uri->segment(3);

		$this->form_validation->set_rules('nama_visa', 'Nama Lengkap', 'required');
		$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
		$this->form_validation->set_rules('no_telp_visa', 'Nomor Telepon', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');

		 if ($this->form_validation->run() == FALSE)
                {
                        $this->index();
                       
                }
                else{
                 
				 $data = array(
		        	'nama_visa' => $this->input->post("nama_visa"),
		        	'jenis_kelamin_visa' => $this->input->post("jenis_kelamin"),
		        	'no_telp_visa' => $this->input->post("no_telp_visa"),
		        	'alamat_visa' => $this->input->post("alamat")
		        					
		             );
				
				
			    $query = $this->db->update('visa',$data,array('id_visa' => $id));
			   
			   	 if ($query) {
				 	$this->session->set_flashdata('edit', 'Data visa berhasil diedit');
				 	redirect('visa/list');
				 }
			}

	}

	public function delete_visa(){
		$id = $this->uri->segment(3);
		$this->db->delete('visa', array('id_visa' => $id));

		redirect('visa/list');
	}

	public function json(){
		$this->load->library('datatables');
        $this->datatables->select('*');
        $this->datatables->from('visa');
        return print_r($this->datatables->generate());
	}
	

}