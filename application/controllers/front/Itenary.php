<?php defined('BASEPATH') or exit('No direct script access allowed.');
/**
 * 
 */
require('Index.php');

class Itenary extends Index
{
	function __construct()
	{
		parent:: __construct();
		$this->load->library('upload');
	}

	public function index(){
		$this->list();	
		$this->cekLogin();
	}

	public function add_itenary(){
		$this->data['itenary'] = $this->global_models->getTable('itenary');
		$this->content = 'content/itenary/vInputitenary';
		$this->layout_front();
	}

	public function list(){
		$id_user = $this->session->userdata('id_user_pelanggan');

		$this->db->select("*,header_pemesanan.status as status_pemesanan");
      	$this->db->from('detail_pemesanan');
      	$this->db->join('header_pemesanan', 'detail_pemesanan.id_header_pemesanan = header_pemesanan.id_header_pemesanan','left');
      	$this->db->join('paket', 'detail_pemesanan.id_paket = paket.id_paket','left');
      	$this->db->join('harga', 'paket.id_paket = harga.id_paket','left');
      	$this->db->join('jadwal', 'detail_pemesanan.id_jadwal = jadwal.id_jadwal','left');
      	$this->db->join('pelanggan', 'header_pemesanan.nik = pelanggan.nik','left');
      	$this->db->join('user', 'pelanggan.id_user = user.id_user','left');
      	$this->db->where('harga.deleted_at',null);
      	$this->db->where('user.id_user',$id_user);
      	$query = $this->db->get();

		$this->db->select("*,visa.status as flag");
      	$this->db->from('visa');
      	$this->db->join('header_pemesanan', 'visa.id_header_pemesanan = header_pemesanan.id_header_pemesanan');
      	$this->db->join('detail_pemesanan', 'header_pemesanan.id_header_pemesanan = detail_pemesanan.id_header_pemesanan');
      	$this->db->join('paket', 'detail_pemesanan.id_paket = paket.id_paket');
      	$this->db->join('country', 'paket.country_code = country.country_code');
      	$this->db->join('pelanggan', 'visa.nik = pelanggan.nik');
      	$this->db->join('user', 'pelanggan.id_user = user.id_user');
      	$this->db->where('pelanggan.id_user',$id_user);
      	
      	$query2 = $this->db->get();
		
		$this->data['itenary']  = $query->result();

		$this->data['visa'] = $query2->result();

		$this->data['kategori'] = $this->uri->segment(4);

		$data= array("menu"	=> "menu_itenary");
			
		$this->session->set_userdata($data);
		
		$this->content = 'content_front/itenary/v_itenary';
		$this->layout_front();
	}

	
	public function delete_itenary(){

		$id = $this->uri->segment(4);

		$this->db->delete('visa', array('id_header_pemesanan' => $id));
		$this->db->delete('detail_pemesanan', array('id_header_pemesanan' => $id));
		$this->db->delete('header_pemesanan', array('id_header_pemesanan' => $id));
		

		$this->list();
		
	}

	public function delete_image(){
		$id = $this->uri->segment(3);
		$id_itenary = $this->uri->segment(4);
		$target = "assets/images/".$id;
		$query = $this->db->delete('images', array('image_name' => $id));
		if($query){
			unlink($target);
		}
		redirect('itenary/edit_itenary/'.$id_itenary);
	}

	
}