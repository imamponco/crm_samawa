<?php defined('BASEPATH') or exit('No direct script access allowed.');
/**
 * 
 */
require('Index.php');

class Settings extends Index
{
	function __construct()
	{
		parent:: __construct();
		$this->load->library('upload');
	}

	public function index(){
		$this->list();
		$this->cekLogin();	
	}

	public function list(){

		$id_user = $this->session->userdata('id_user_pelanggan');

		$this->db->select("*");
      	$this->db->from('user');
      	$this->db->join('pelanggan', 'user.id_user = pelanggan.id_user','left');
      	$this->db->where('user.id_user = "'.$id_user.'"');
      	
     	$query = $this->db->get();

     	$this->data['user'] = $query->result();

     	$data= array("menu"	=> "menu_setting");
			
		$this->session->set_userdata($data);

		$this->content = 'content_front/settings/v_setting';
		$this->layout_front();
	}

	public function do_edit_user(){

		$id = $this->session->userdata('id_user_pelanggan');


		$this->form_validation->set_rules('username', 'Nama Lengkap', 'required|alpha');
		$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
		$this->form_validation->set_rules('tgl_lahir', 'Tangal Lahir', 'required');
		$this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required|alpha');
		// $this->form_validation->set_rules('no_passport', 'Nomor Passport', 'required');
		// $this->form_validation->set_rules('relationship', 'Relationship', 'required');
		// $this->form_validation->set_rules('issuing_office', 'Issuing Office', 'required');
		// $this->form_validation->set_rules('date_of_issue', 'Date of Issue', 'required');
		$this->form_validation->set_rules('nik', 'NIK', 'required|integer');
		// $this->form_validation->set_rules('date_of_expiry', 'Date of Expiry', 'required');
		$this->form_validation->set_rules('no_telp', 'Nomor Telepon', 'required|integer');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');

        $this->db->select("email");
      	$this->db->from('user');  
      	$this->db->where('id_user',$id); 	
     	$current_data = $this->db->get()->row();

     	$this->db->select("email");
      	$this->db->from('user');
     	$validasi_data =$this->db->get()->result();
		
		if ($this->form_validation->run() == FALSE)
                {
                   redirect('user/edit_user/'.$id);
                }
                else{
               	//validasi password
                if(!empty($this->input->post("password"))){
                	if ($this->input->post("password")==$this->input->post("repassword")) {
                		$password =  password_hash($this->input->post("password"), PASSWORD_DEFAULT);
                	}else{
            		$this->session->set_flashdata('error', 'Password dan Re-Password tidak mirip.');
			 		redirect('user/edit_user/'.$id);
                	}	
                } 

                //validasi email
                if(!empty($this->input->post("email"))){
                	if($this->input->post("email")==$current_data->email){
                		$email = $this->input->post("email");
                	}else{

	                	foreach ($validasi_data as $row) {
	                		if ($this->input->post("email")==$row->email) {
	                			$email_error = 1;
	                		}
	                	}

	                	if(isset($email_error)){
	                		$this->session->set_flashdata('error', 'Email telah digunakan.');
				 			redirect('user/edit_user/'.$id);
	                	}else{
	                		$email = $this->input->post("email");
	                	}

                	}
                }

                if(empty($this->input->post("email"))&&empty($this->input->post("password"))){
                	$data = array(
		                'username' => $this->input->post("username"),
		             );
                }else if(empty($this->input->post("password"))){
                	$data = array(
		                'username' => $this->input->post("username"),
		                'email'    => $email
		             );
                }else{
                	$data = array(
		                'username' => $this->input->post("username"),
		                'email'    => $email,
		                'password' => $password
		             );
                }

                $pelanggan = array(
		        	'jenis_kelamin' => $this->input->post("jenis_kelamin"),
		        	'tanggal_lahir' => $this->input->post("tgl_lahir"),
		        	'tempat_lahir' => $this->input->post("tempat_lahir"),
		        	'no_passport' => $this->input->post("no_passport"),
		        	'relationship' => $this->input->post("relationship"),
		        	'no_telp' => $this->input->post("no_telp"),
		        	'alamat' => $this->input->post("alamat"),
		        	'nik' => $this->input->post("nik")			
		            );
				
				
			    
				
			    $query = $this->db->update('user',$data,array('id_user' => $id));
			    $query2 = $this->db->update('pelanggan',$pelanggan,array('id_user' => $id));
			   
			   	if ($query && $query2) {
			   	 	$this->session->set_flashdata('edit', 'Data user berhasil diedit');
				 	redirect('front/settings');
				 }
			}
	}
	
}