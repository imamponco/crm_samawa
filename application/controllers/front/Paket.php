<?php defined('BASEPATH') or exit('No direct script access allowed.');
/**
 * 
 */
require('Index.php');

class Paket extends Index
{
	function __construct()
	{
		parent:: __construct();
		$this->load->library('upload');
	}

	public function index(){
		$this->list();
		$this->cekLogin();	
	}

	public function list(){

		$query = $this->db->query("SELECT a.*,h.*,t.jml,c.* FROM paket a
LEFT JOIN (SELECT COUNT(*) AS jml, id_paket,status FROM jadwal GROUP BY id_paket) t ON a.id_paket = t.id_paket
LEFT JOIN harga h ON a.id_paket = h.id_paket
LEFT JOIN country c ON a.country_code = c.country_code
WHERE h.deleted_at IS NULL AND t.status = 0
HAVING t.jml > 0
")->result();

		$data= array("menu"	=> "menu_paket");
			
		$this->session->set_userdata($data);

		$this->data['paket']  = $query;

		$this->data['kategori'] = $this->uri->segment(4);

		$this->content = 'content_front/paket/v_paket';
		$this->layout_front();
	}


}