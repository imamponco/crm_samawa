<?php

/**
 * 
 */
class Index extends MY_Controller
{
	
	function __construct()
	{
		parent:: __construct();

		// $this->cekLogin();

		$this->load->model('global_models');

		$this->load->model('user_models');

		$this->load->library('form_validation');

	}

	public function index(){
	

		$query = $this->db->query("SELECT a.*,h.*,t.jml,c.* FROM paket a
LEFT JOIN (SELECT COUNT(*) AS jml, id_paket FROM jadwal GROUP BY id_paket) t ON a.id_paket = t.id_paket
LEFT JOIN harga h ON a.id_paket = h.id_paket
LEFT JOIN country c ON a.country_code = c.country_code
WHERE h.deleted_at IS NULL
HAVING t.jml > 0
")->result();

		$this->data["jml_user"] = $this->db->query("SELECT COUNT(id_user) as jml_user FROM USER WHERE id_user LIKE '%CO%' AND verified_at IS NOT NULL")->row();

		$this->data["jml_paket"] = $this->db->query("SELECT t.jml,COUNT(t.jml) AS jml_paket FROM paket a LEFT JOIN (SELECT COUNT(*) AS jml, id_paket FROM jadwal GROUP BY id_paket) t ON a.id_paket = t.id_paket HAVING t.jml > 0")->row();

		$this->data["jml_transaksi"] = $this->db->query("SELECT COUNT(id_header_pemesanan) AS jml_transaksi FROM header_pemesanan  WHERE STATUS != 0")->row();

		$this->data["jml_review"] = $this->db->query("SELECT COUNT(id_content) AS jml_review FROM content WHERE jenis_content LIKE '%KDR%'")->row();

		$data= array("menu"	=> "menu_home");
			
		$this->session->set_userdata($data);

		$this->data['paket'] = $query;
		$this->data['agen'] = $this->global_models->getTable('agen');
		$this->content = 'content_front/index';
		$this->layout_front();
	}

	public function kontak(){
		$data= array("menu"	=> "menu_kontak");
			
		$this->session->set_userdata($data);
		// $this->session->userdata('menu') = "menu_kontak";
		$this->content = 'content_front/kontak';
		$this->layout_front();
	}

	
	public function setting(){

		$data= array("menu"	=> "menu_setting");
			
		$this->session->set_userdata($data);

		$is_login = $this->session->userdata('is_login_user');
		$permission = $this->session->userdata('permission_user');
		if ($is_login === TRUE && $permission == "CO") {
			$data['user'] = $this->global_models->getTable('user'); 
			$this->load->view('view_dashboard');
			$this->load->view('v_isi',$data);
		}else{
			redirect('login_baru');
		}
	}

	public function cekLogin(){
		$is_login = $this->session->userdata('is_login_user');
		$permission = $this->session->userdata('permission_user');

		if ($is_login === TRUE && $permission == "CO") {
			
		}else{
			redirect('front/index','refresh');
		}
	}
}
?>