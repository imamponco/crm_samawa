<?php defined('BASEPATH') or exit('No direct script access allowed.');
/**
 * 
 */
require('Index.php');

class Kritik extends Index
{
	function __construct()
	{
		parent:: __construct();
	}

	public function index(){
			$this->list();
			$this->cekLogin();	
	}

	public function notifikasi(){
		$id_user = $this->session->userdata('id_user_pelanggan');

		$this->db->select("*");
      	$this->db->from('content');
      	$this->db->join('user', 'content.id_user = user.id_user','left');
      	$this->db->where('content.id_user = "'.$id_user.'"');
      	$this->db->like('content.jenis_content','PR');
      	$this->db->order_by('created_ct_at','asc');

	   	$query = $this->db->get();

	   $data= array("menu"	=> "menu_notifikasi");
			
		$this->session->set_userdata($data);

		$this->data['message'] = $query->result();
		$this->content = 'content_front/kritik/v_notifikasi';
		$this->layout_front();
	}



	public function list(){
		$id_user = $this->session->userdata('id_user_pelanggan');

		$this->db->select("*");
      	$this->db->from('content');
      	$this->db->join('user', 'content.id_user = user.id_user','left');
      	$this->db->where('content.id_user = "'.$id_user.'"');
      	$this->db->like('content.jenis_content', 'KDR');
      	$this->db->order_by('created_ct_at','asc');

	   	$query = $this->db->get();

	   	$cek = $query->result();

      	if (!empty($cek)) {
      		$this->db->select("*");
	      	$this->db->from('content');
	      	$this->db->join('user', 'content.id_user = user.id_user','left');
	      	$this->db->where('content.jenis_content = "'.$cek[0]->jenis_content.'"');
	      	$this->db->like('content.jenis_content', 'KDR');
	      	$this->db->order_by('created_ct_at','asc');

	      	$query = $this->db->get();
      	}
      	
      	$data= array("menu"	=> "menu_kritik");
			
		$this->session->set_userdata($data);

		$this->data['kritik'] = $query->result();
		$this->content = 'content_front/kritik/v_kritik';
		$this->layout_front();
	}

	public function generate_pk($prefix,$number){
		switch (strlen($number)) {
		    case 1:
		    	$digit = 2;
		    break;

		    case 2:
		        $digit = 1;
		    break;

		    case 3:
		        $digit = 0;
		    break;

		    default:
		        $digit = 0;
		    break;
		}
        $primary_key = $prefix.str_repeat("0", $digit).$number;
        return $primary_key; 
	}


	public function do_add_kritik(){
		$this->form_validation->set_rules('message', 'Pesan', 'required');
		if ($this->form_validation->run() == FALSE)
                {
                    $this->session->set_flashdata('msg', 'Gagal menambah kritik');
					redirect('front/kritik');   
                }
                else{
	                	date_default_timezone_set("Asia/Bangkok");
                		$id_user = $this->session->userdata("id_user_pelanggan");

						 $data = array(
				        	'message' 		=> $this->input->post("message"),
				        	'id_user' 		=> $id_user,
				        	'created_ct_at' => date("Y-m-d h:i:sa")
				             );
				
						 $field = "content";
						 $sukses = $this->global_models->insert_data($field,$data);

						 if ($sukses) {
						 	$prefix = "CT";
						 	$pk = $this->generate_pk($prefix,$sukses->no_content);

						 	$prefix = "KDR";
						 	$jenis_content = $this->generate_pk($prefix,$sukses->no_content);

						 	$data_update = array(
				        	'id_content' => $pk,
				        	'jenis_content' => $jenis_content
				             );

						 	$this->db->set($data_update);
			                $this->db->where('no_content', $sukses->no_content);
			                $this->db->update('content');

						 	$this->session->set_flashdata('msg', 'Data berhasil diinput. ');
						 	redirect('front/kritik');
	                }
			}
	}

	public function do_add_balas(){
		$id = $this->uri->segment(4);
		$this->form_validation->set_rules('message', 'Pesan', 'required');

		if ($this->form_validation->run() == FALSE)
                {
                    redirect('front/kritik');   
                }
                else{
	                if (!empty($this->input->post("jenis_content"))) {
	                	date_default_timezone_set("Asia/Bangkok");
                		$id_user = $this->session->userdata("id_user_pelanggan");


						 $data = array(
				        	'message' 		=> $this->input->post("message"),
				        	'jenis_content' => $this->input->post("jenis_content"),
				        	'id_user' 		=> $id_user,
				        	'created_ct_at' => date("Y-m-d h:i:sa")
				             );

						 $field = "content";
						 $sukses = $this->global_models->insert_data($field,$data);

						 if ($sukses) {
						 	$prefix = "CT";
						 	$pk = $this->generate_pk($prefix,$sukses->no_content);

						 	$prefix = "KDR";
						 	$jenis_content = $this->generate_pk($prefix,$sukses->no_content);

						 	$data_update = array(
				        	'id_content' => $pk
				             );

						 	$this->db->set($data_update);
			                $this->db->where('no_content', $sukses->no_content);
			                $this->db->update('content');

						 	$this->session->set_flashdata('msg', 'Data berhasil diinput. ');
						 	redirect('front/kritik');
	                }
				}
			}
	}

	public function delete_balasan(){
		$id_content = $this->uri->segment(4);
		
		$this->db->delete('content', array('id_content' => $id_content));

		redirect('front/kritik');
	}

	public function delete_notifikasi(){
		$id_content = $this->uri->segment(4);
		
		$this->db->delete('content', array('id_content' => $id_content));

		redirect('front/kritik/notifikasi');
	}

	public function delete_kritik(){
		$id = $this->uri->segment(3);
		$this->db->delete('content', array('jenis_content' => $id));

		redirect('kritik/list');
	}



}