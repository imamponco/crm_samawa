<?php defined('BASEPATH') or exit('No direct script access allowed.');
/**
 * 
 */
require('Dashboard.php');

class Jadwal extends Dashboard
{
	function __construct()
	{
		parent:: __construct();
	}

	public function index(){
			$this->list();	
	}

	public function add_jadwal(){
		$this->data['paket'] = $this->global_models->getTable('paket');

		$this->content = 'content/jadwal/vInputjadwal';
		$this->layout();
	}

	public function generate_pk($prefix,$number){
		switch (strlen($number)) {
		    case 1:
		    	$digit = 2;
		    break;

		    case 2:
		        $digit = 1;
		    break;

		    case 3:
		        $digit = 0;
		    break;

		    default:
		        $digit = 0;
		    break;
		}
        $primary_key = $prefix.str_repeat("0", $digit).$number;
        return $primary_key; 
	}

	public function do_add_jadwal(){

	$this->form_validation->set_rules('id_paket', 'Nama Paket', 'required');
	$this->form_validation->set_rules('nama_maskapai', 'Nama Maskapai', 'required|alpha');
	$this->form_validation->set_rules('kuota', 'Kuota', 'required|integer');
	$this->form_validation->set_rules('no_flight', 'Nomor Penerbangan', 'required|is_unique[jadwal.no_flight]|alpha_numeric');
	$this->form_validation->set_rules('kota_asal', 'Kota Asal', 'required');
	$this->form_validation->set_rules('jam_terbang', 'Jam Keberangkatan', 'required');
	$this->form_validation->set_rules('kota_tujuan', 'Kota Tujuan', 'required');
	$this->form_validation->set_rules('jam_tiba', 'Jam Tiba', 'required');
	$this->form_validation->set_rules('tgl_keberangkatan', 'Tanggal Keberangkatan', 'required');
	// $this->form_validation->set_rules('status', 'Status', 'required');

		if ($this->form_validation->run() == FALSE)
                {
                        $this->add_jadwal();   
                }
                else{

				 $data = array(
		        	'id_paket' 			=> $this->input->post("id_paket"),
		        	'nama_maskapai' 	=> $this->input->post("nama_maskapai"),
		        	'kuota' 			=> $this->input->post("kuota"),
		        	'no_flight' 		=> $this->input->post("no_flight"),
		        	'kota_asal' 		=> $this->input->post("kota_asal"),
		        	'jam_terbang' 		=> $this->input->post("jam_terbang"),
		        	'kota_tujuan'		=> $this->input->post("kota_tujuan"),
		        	'jam_tiba' 			=> $this->input->post("jam_tiba"),
		        	'tgl_keberangkatan' => $this->input->post("tgl_keberangkatan"),
		        	'status' 			=> 0
		             );

				 $field = "jadwal";
				 $sukses = $this->global_models->insert_data($field,$data);

				 if ($sukses) {
				 	$prefix = "J";
				 	$pk = $this->generate_pk($prefix,$sukses->no_jd);

				 	$this->db->set('id_jadwal',$pk);
	                $this->db->where('no_jd', $sukses->no_jd);
	                $this->db->update('jadwal');

				 	$this->session->set_flashdata('msg', 'Data jadwal berhasil diinput. ');
				 	$this->add_jadwal();
				 }
			}
	}

	public function list(){
		$filter = $this->input->post("filter");

		if($filter != NULL AND $filter != "all"){
			
			$filter = $this->input->post("filter");

			$this->db->select("*");
	      	$this->db->from('jadwal');
	      	$this->db->join('paket', 'jadwal.id_paket = paket.id_paket','left');
	      	$this->db->where('jadwal.status = "'.$filter.'"');
	      	$this->db->order_by('id_jadwal','desc');
	      	$query = $this->db->get();


			$this->data['jadwal'] = $query->result();
			
			$data= array("menu_back" => "menu_jadwal");
			
			$this->session->set_userdata($data);

			$this->content = 'content/jadwal/vDatajadwal';
			$this->layout();

		}else if($filter == "all" or $filter == NULL){
			
			$this->db->select("*");
	      	$this->db->from('jadwal');
	      	$this->db->join('paket', 'jadwal.id_paket = paket.id_paket','left');
	      	$this->db->order_by('id_jadwal','desc');
	      	$query = $this->db->get();


			$this->data['jadwal'] = $query->result();

			$data= array("menu_back" => "menu_jadwal");
			
			$this->session->set_userdata($data);

			$this->content = 'content/jadwal/vDatajadwal';
			$this->layout();
			
		}
	}

	public function manifest(){
		$id_jadwal = $this->uri->segment(4);
		$id_paket = $this->uri->segment(5);
		$this->db->select("*");
      	$this->db->from('pelanggan');
      	$this->db->join('user', 'pelanggan.id_user = user.id_user','left');
     	$this->db->join('header_pemesanan', 'pelanggan.nik = header_pemesanan.nik','left');
     	$this->db->join('detail_pemesanan', 'header_pemesanan.id_header_pemesanan = detail_pemesanan.id_header_pemesanan');
     	$this->db->where('detail_pemesanan.id_jadwal = "'.$id_jadwal.'" AND detail_pemesanan.id_paket = "'.$id_paket.'"');

      	$query = $this->db->get();

		$this->data['customer'] = $query->result();
		// var_dump($this->data['customer']);
		// die;
		$this->content = 'content/jadwal/vListManifest';
		$this->layout();
	}

	public function edit_jadwal(){
		$id = $this->uri->segment(3);
		$id_paket = $this->uri->segment(4);

		$this->db->select("*");
      	$this->db->from('jadwal');
      	$this->db->join('paket','paket.id_paket = jadwal.id_paket','right');
      	$this->db->where('jadwal.id_jadwal = "'.$id.'" AND paket.id_paket="'.$id_paket.'"');
     	$query = $this->db->get();

     	$this->data['paket'] = $this->global_models->getTable('paket');
     	$this->data['jadwal'] = $query->result();

		$this->content = 'content/jadwal/vEditDatajadwal';
		$this->layout();
	}

	public function do_edit_jadwal(){

		$id_jadwal = $this->uri->segment(3);
		$id_paket = $this->uri->segment(4);

		$this->form_validation->set_rules('id_paket', 'Paket', 'required');
		$this->form_validation->set_rules('kuota', 'Kuota', 'required');
		$this->form_validation->set_rules('nama_maskapai', 'Nama Maskapai', 'required');
		$this->form_validation->set_rules('no_flight', 'Nomor Penerbangan', 'required');
		$this->form_validation->set_rules('kota_asal', 'Kota Asal', 'required');
		$this->form_validation->set_rules('jam_terbang', 'Jam Keberangkatan', 'required');
		$this->form_validation->set_rules('kota_tujuan', 'Kota Tujuan', 'required');
		$this->form_validation->set_rules('jam_tiba', 'Jam Tiba', 'required');
		$this->form_validation->set_rules('tgl_keberangkatan', 'Tanggal Keberangkatan', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');
		// var_dump($this->input->post("id_paket"));
		// die;
			

		 if ($this->form_validation->run() == FALSE)
                {
                    $this->edit_jadwal();
                }
                else{

				  $data = array(
				  	'id_paket' => $this->input->post("id_paket"),
		        	'nama_maskapai' => $this->input->post("nama_maskapai"),
		        	'kuota' => $this->input->post("kuota"),
		        	'no_flight' => $this->input->post("no_flight"),
		        	'kota_asal' => $this->input->post("kota_asal"),
		        	'jam_terbang' => $this->input->post("jam_terbang"),
		        	'kota_tujuan' => $this->input->post("kota_tujuan"),
		        	'jam_tiba' => $this->input->post("jam_tiba"),
		        	'tgl_keberangkatan' => $this->input->post("tgl_keberangkatan"),
		        	'status' => $this->input->post("status")
		             );
	
				
			    $query = $this->db->update('jadwal',$data,array('id_jadwal' => $id_jadwal));
			   
			   	 if ($query) {
				 	$this->session->set_flashdata('edit', 'Data jadwal berhasil diedit');
				 	$this->list();
				 }
			}

	}

	public function delete_jadwal(){
		$id = $this->uri->segment(3);
		$data = array(
				  	'status' => 1
				);
					
		$query = $this->db->update('jadwal',$data,array('id_jadwal' => $id));
		$this->list();
	}

	public function do_delete_jadwal(){
		$id = $this->uri->segment(3);
		
		$query = $this->db->delete('jadwal', array('id_jadwal' => $id));
		$this->list();
	}

}