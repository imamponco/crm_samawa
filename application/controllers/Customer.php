<?php defined('BASEPATH') or exit('No direct script access allowed.');
/**
 * 
 */
require('Dashboard.php');

class Customer extends Dashboard
{
	function __construct()
	{
		parent:: __construct();
	}

	public function index(){
			$this->list();	
	}

	public function add_customer(){
		$this->data['agen'] = $this->global_models->getTable('agen');

		$this->db->select("*");
      	$this->db->from('user');
      	$this->db->join('pelanggan', 'user.id_user = pelanggan.id_user');
      	$this->db->where('user.verified_at is NOT NULL', NULL, FALSE);
      	$this->db->where('pelanggan.id_user', NULL);
      	$this->db->like('user.id_user','CO');
      	$user = $this->db->get();

		$this->data['user'] = $user->result();
		$this->content = 'content/pelanggan/vInputCustomer';
		$this->layout();
	}

	public function do_add_customer(){

	   	$this->form_validation->set_rules('id_user', 'User', 'required|is_unique[pelanggan.id_user]');
		$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
		$this->form_validation->set_rules('tgl_lahir', 'Tangal Lahir', 'required');
		$this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required');
		$this->form_validation->set_rules('no_passport', 'Nomor Passport', 'is_unique[pelanggan.no_passport]');
		// $this->form_validation->set_rules('issuing_office', 'Issuing Office', 'required');
		// $this->form_validation->set_rules('date_of_issue', 'Date of Issue', 'required');
		$this->form_validation->set_rules('nik', 'NIK', 'required|is_unique[pelanggan.nik]');
		// $this->form_validation->set_rules('date_of_expiry', 'Date of Expiry', 'required');
		$this->form_validation->set_rules('no_telp', 'Nomor Telepon', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('nama_agen', 'nama_agen','required|alpha');

		if ($this->form_validation->run() == FALSE)
                {
                    $this->add_customer();   
                }
                else{

				 $data = array(
		        	'id_agen' => $this->input->post("id_agen"),
		        	'id_user' => $this->input->post("id_user"),
		        	'jenis_kelamin' => $this->input->post("jenis_kelamin"),
		        	'tanggal_lahir' => $this->input->post("tgl_lahir"),
		        	'tempat_lahir' => $this->input->post("tempat_lahir"),
		        	'no_passport' => $this->input->post("no_passport"),
		        	// 'issuing_office' => $this->input->post("issuing_office"),
		        	// 'date_of_issue' => $this->input->post("date_of_issue"),
		        	// 'date_of_expiry' => $this->input->post("date_of_expiry"),
		        	'relationship' => $this->input->post("relationship"),
		        	'no_telp' => $this->input->post("no_telp"),
		        	'alamat' => $this->input->post("alamat"),
		        	'nik' => $this->input->post("nik")
		        	// 'room_type' => $this->input->post("room_type")
		        					
		             );

				 $field = "pelanggan";
				 $sukses = $this->global_models->insert_data($field,$data);

				 if ($sukses) {
				 	$this->session->set_flashdata('msg', 'Data pelanggan berhasil diinput. ');
				 	$this->add_customer();
				 }
			}
	}

	public function list(){
		$this->db->select("*");
      	$this->db->from('pelanggan');
     	$this->db->join('agen', 'pelanggan.id_agen = agen.id_agen');
     	$this->db->join('user', 'pelanggan.id_user = user.id_user','left');
     	$this->db->where('user.verified_at is NOT NULL', NULL, FALSE);
      	$this->db->like('user.id_user','CO');
      	$query = $this->db->get();

		$this->data['customer'] = $query->result();

		$data= array("menu_back" => "menu_customer");
			
		$this->session->set_userdata($data);

		$this->content = 'content/pelanggan/vDataCustomer';
		$this->layout();
	}

	public function edit_pelanggan(){
		$id = $this->uri->segment(3);

		$this->db->select("*");
      	$this->db->from('pelanggan');
      	$this->db->join('agen', 'pelanggan.id_agen = agen.id_agen','left');
      	$this->db->join('user', 'pelanggan.id_user = user.id_user','left');
      	$this->db->where('pelanggan.nik = "'.$id.'"');
      	$this->db->where('user.verified_at is NOT NULL', NULL, FALSE);
      	$this->db->like('user.id_user','CO');
      	
     	$query = $this->db->get();

     	$this->data['customer'] = $query->result();
     	$this->data['agen'] = $this->global_models->getTable('agen');

		$this->content = 'content/pelanggan/vEditDataCustomer';
		$this->layout();
	}

	public function do_edit_customer(){

		$id = $this->uri->segment(3);
		$id_user = $this->uri->segment(4);

		$this->form_validation->set_rules('username', 'Nama Lengkap', 'required');
		$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
		$this->form_validation->set_rules('tgl_lahir', 'Tangal Lahir', 'required');
		$this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required');
		// $this->form_validation->set_rules('no_passport', 'Nomor Passport', 'required');
		// $this->form_validation->set_rules('issuing_office', 'Issuing Office', 'required');
		// $this->form_validation->set_rules('date_of_issue', 'Date of Issue', 'required');
		$this->form_validation->set_rules('nik', 'NIK', 'required');
		// $this->form_validation->set_rules('date_of_expiry', 'Date of Expiry', 'required');
		$this->form_validation->set_rules('no_telp', 'Nomor Telepon', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');

		 if ($this->form_validation->run() == FALSE)
                {
                        $this->index();
                       
                }
                else{

				 $data = array(
		        	'id_agen' => $this->input->post("id_agen"),
		        	'jenis_kelamin' => $this->input->post("jenis_kelamin"),
		        	'tanggal_lahir' => $this->input->post("tgl_lahir"),
		        	'tempat_lahir' => $this->input->post("tempat_lahir"),
		        	'no_passport' => $this->input->post("no_passport"),
		        	// 'issuing_office' => $this->input->post("issuing_office"),
		        	// 'date_of_issue' => $this->input->post("date_of_issue"),
		        	// 'date_of_expiry' => $this->input->post("date_of_expiry"),
		        	'relationship' => $this->input->post("relationship"),
		        	'no_telp' => $this->input->post("no_telp"),
		        	'alamat' => $this->input->post("alamat"),
		        	'nik' => $this->input->post("nik")
		        	// 'room_type' => $this->input->post("room_type")
		        					
		             );
				 $user = array(
				 	'username' => $this->input->post("username")
				 );
				
				
			    $query = $this->db->update('pelanggan',$data,array('nik' => $id));
			    $query2 = $this->db->update('user',$user,array('id_user' => $id_user));

			   	 if ($query && $query2) {
				 	$this->session->set_flashdata('edit', 'Data pelanggan berhasil diedit');
				 	$this->list();
				 }
			}

	}

	public function delete_pelanggan(){
		$id = $this->uri->segment(3);
		$id_user = $this->uri->segment(4);
		$this->db->delete('pelanggan', array('nik' => $id));
		$this->db->delete('user', array('id_user' => $id_user));

		redirect('pelanggan/list');
	}

	public function json(){

		$this->load->library('datatables');
        $this->datatables->select('nik,nama,jenis_kelamin,tanggal_lahir,tempat_lahir,nno_telp,alamat,nik,nama_agen');
        $this->datatables->from('pelanggan');
        $this->datatables->join('agen', 'pelanggan.id_agen = agen.id_agen','left');
        return print_r($this->datatables->generate());
	}

	public function call(){
		$this->load->view('json');
	}
	

}