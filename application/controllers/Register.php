<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 
 */
class Register extends CI_Controller
{
    public $pelanggan;

    function __construct()
    {
        parent:: __construct();
        $this->load->model('user_models');
        $this->load->library('form_validation');
    }
	
	function index(){
		$this->load->view('view_register');
    }

    public function generate_pk($prefix,$number){
        switch (strlen($number)) {
            case 1:
                $digit = 2;
            break;

            case 2:
                $digit = 1;
            break;

            case 3:
                $digit = 0;
            break;

            default:
                $digit = 0;
            break;
        }
        $primary_key = $prefix.str_repeat("0", $digit).$number;
        return $primary_key; 
    }
    		

    public function do_register(){
        $this->form_validation->set_rules('username', 'Full Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|is_unique[user.email]|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('repassword', 'Re-Password', 'trim|required|matches[password]');
        $this->form_validation->set_rules('role', 'Role', 'required');

         
         if ($this->form_validation->run() == FALSE){
           $this->index();   
         }else{
            $password =  password_hash($this->input->post("password"), PASSWORD_DEFAULT);
            $role = $this->input->post("role");

                $data = array(
                    'username' => $this->input->post("username"),
                    'email'    => $this->input->post("email"),
                    'password' => $password,
                     );

                 $sukses = $this->user_models->insert_user($data);

                 if ($sukses) {
                    $pk_user = $this->generate_pk($role,$sukses->no);

                    $this->db->set('id_user', $pk_user);
                    $this->db->where('no', $sukses->no);
                    $this->db->update('user');

                    $this->session->set_flashdata('register', 'Akun anda berhasil terdaftar! Silahkan login');
                    $this->index();
                 }
        }
    }


     public function do_register_front(){
       
        echo "String";
        die;

        $this->form_validation->set_rules('username', 'Full Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|is_unique[user.email]|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('repassword', 'Re-Password', 'trim|required|matches[password]');
        $this->form_validation->set_rules('role', 'Role', 'required');
         
         if ($this->form_validation->run() == FALSE){
           $this->index();   
         }else{
            $password =  password_hash($this->input->post("password"), PASSWORD_DEFAULT);

                $data = array(
                    'username' => $this->input->post("username"),
                    'email'    => $this->input->post("email"),
                    'password' => $password,
                     );

                 $sukses = $this->user_models->insert_user($data);

                 if ($sukses) {
                    switch (strlen($sukses->no)) {
                    case 1:
                    $digit = 2;
                    break;
                    case 2:
                    $digit = 1;
                    break;
                    case 3:
                    $digit = 0;
                    break;
                    default:
                    $digit = 0;
                    break;
                    }

                    $prefix = $this->input->post('role');
                    $id_user = $prefix.str_repeat("0", $digit).$sukses->no;

                    $this->db->set('id_user', $id_user);
                    $this->db->where('no', $sukses->no);
                    $this->db->update('user');

                    $pelanggan = array(
                    'username' => $this->input->post("username"),
                    'email'    => $this->input->post("email"),
                    'password' => $password,
                     );

                    $this->session->set_flashdata('register', 'Akun anda berhasil terdaftar! Silahkan login');
                    redirect('/front/index');
                 }
        }
    }

}


?>