<?php defined('BASEPATH') or exit('No direct script access allowed.');
/**
 * 
 */
require('Dashboard.php');

class Agen extends Dashboard
{
	function __construct()
	{
		parent:: __construct();
	}

	public function index(){
			$this->list();	
	}

	public function add_agen(){
		$this->data['agen'] = $this->global_models->getTable('agen');
		$this->data['paket'] = $this->global_models->getTable('paket');
		$this->content = 'content/agen/vInputAgen';
		$this->layout();
	}

	public function generate_pk($prefix,$number){
		switch (strlen($number)) {
		    case 1:
		    	$digit = 2;
		    break;

		    case 2:
		        $digit = 1;
		    break;

		    case 3:
		        $digit = 0;
		    break;

		    default:
		        $digit = 0;
		    break;
		}
        $primary_key = $prefix.str_repeat("0", $digit).$number;
        return $primary_key; 
	}

	public function do_add_agen(){
		$this->form_validation->set_rules('nama_agen', 'Nama Lengkap', 'required|alpha');
		$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
		$this->form_validation->set_rules('no_telp_agen', 'Nomor Telepon', 'required|is_unique[agen.no_telp_agen]|integer');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');

		if ($this->form_validation->run() == FALSE)
                {
                        $this->add_agen();   
                }
                else{

				 $data = array(
		        	'nama_agen' => $this->input->post("nama_agen"),
		        	'jenis_kelamin_agen' => $this->input->post("jenis_kelamin"),
		        	'no_telp_agen' => $this->input->post("no_telp_agen"),
		        	'alamat_agen' => $this->input->post("alamat")
		             );

				 $field = "agen";
				 $sukses = $this->global_models->insert_data($field,$data);
				 
				 if ($sukses) {
				 	$prefix = "AG";
				 	$pk = $this->generate_pk($prefix,$sukses->no_agen);

				 	$this->db->set('id_agen',$pk);
	                $this->db->where('no_agen', $sukses->no_agen);
	                $this->db->update('agen');

				 	$this->session->set_flashdata('msg', 'Data agen berhasil diinput. ');
				 	$this->add_agen();
				 }
			}
	}

	public function list(){
		$this->db->select("*");
      	$this->db->from('agen');
      	$this->db->order_by('id_agen','desc');
      	$query = $this->db->get();

      	$data= array("menu_back" => "menu_agen");
			
		$this->session->set_userdata($data);

		$this->data['agen'] = $query->result();

		$this->content = 'content/agen/vDataAgen';
		$this->layout();
	}

	public function edit_agen(){
		$id = $this->uri->segment(3);

		$this->db->select("*");
      	$this->db->from('agen');
      	$this->db->where('agen.id_agen = "'.$id.'"');
      	
     	$query = $this->db->get();

     	$this->data['agen'] = $query->result();
		$this->content = 'content/agen/vEditDataAgen';
		$this->layout();
	}

	public function do_edit_agen(){

		$id = $this->uri->segment(3);

		$this->form_validation->set_rules('nama_agen', 'Nama Lengkap', 'required');
		$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
		$this->form_validation->set_rules('no_telp_agen', 'Nomor Telepon', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');

		 if ($this->form_validation->run() == FALSE)
                {
                        $this->index();
                       
                }
                else{
                 
				 $data = array(
		        	'nama_agen' => $this->input->post("nama_agen"),
		        	'jenis_kelamin_agen' => $this->input->post("jenis_kelamin"),
		        	'no_telp_agen' => $this->input->post("no_telp_agen"),
		        	'alamat_agen' => $this->input->post("alamat")
		        					
		             );
				
				
			    $query = $this->db->update('agen',$data,array('id_agen' => $id));
			   
			   	 if ($query) {
				 	$this->session->set_flashdata('edit', 'Data agen berhasil diedit');
				 	redirect('agen/list');
				 }
			}

	}

	public function delete_agen(){
		$id = $this->uri->segment(3);
		$this->db->delete('agen', array('id_agen' => $id));

		redirect('agen/list');
	}

	public function json(){
		$this->load->library('datatables');
        $this->datatables->select('*');
        $this->datatables->from('agen');
        return print_r($this->datatables->generate());
	}
	

}