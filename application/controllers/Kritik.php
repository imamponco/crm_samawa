<?php defined('BASEPATH') or exit('No direct script access allowed.');
/**
 * 
 */
require('Dashboard.php');

class Kritik extends Dashboard
{
	function __construct()
	{
		parent:: __construct();
	}

	public function index(){
			$this->list();	
	}

	public function add_kritik(){
		$id = $this->uri->segment(3);

		$this->db->select("*");
      	$this->db->from('content');
      	$this->db->join('user', 'content.id_user = user.id_user','left');
      	$this->db->where('content.jenis_content = "'.$id.'"');
      	$this->db->order_by('created_ct_at','asc');
     	
     	$query = $this->db->get();
     	$this->data['kritik'] = $query->result();
		$this->data['content'] = $this->global_models->getTable('content');

		$this->content = 'content/kritik/vInputkritik';
		$this->layout();
	}

	public function generate_pk($prefix,$number){
		switch (strlen($number)) {
		    case 1:
		    	$digit = 2;
		    break;

		    case 2:
		        $digit = 1;
		    break;

		    case 3:
		        $digit = 0;
		    break;

		    default:
		        $digit = 0;
		    break;
		}
        $primary_key = $prefix.str_repeat("0", $digit).$number;
        return $primary_key; 
	}

	public function do_add_kritik(){
		$id = $this->uri->segment(3);
		$this->form_validation->set_rules('message', 'Pesan', 'required');
		if ($this->form_validation->run() == FALSE)
                {
                        $this->add_kritik();   
                }
                else{
	                if (!empty($this->input->post("jenis_content"))) {
	                	date_default_timezone_set("Asia/Bangkok");
                		$id_user = $this->session->userdata("id_user");

						 $data = array(
				        	'message' 		=> $this->input->post("message"),
				        	'jenis_content' => $this->input->post("jenis_content"),
				        	'id_user' 		=> $id_user,
				        	'created_ct_at' => date("Y-m-d h:i:sa")
				             );


						 $field = "content";
						 $sukses = $this->global_models->insert_data($field,$data);

						 if ($sukses) {
						 	$prefix = "CT";
						 	$pk = $this->generate_pk($prefix,$sukses->no_content);

						 	$prefix = "KDR";
						 	$jenis_content = $this->generate_pk($prefix,$sukses->no_content);

						 	$data_update = array(
				        	'id_content' => $pk
				             );

						 	$this->db->set($data_update);
			                $this->db->where('no_content', $sukses->no_content);
			                $this->db->update('content');

						 	$this->session->set_flashdata('msg', 'Data berhasil diinput. ');
						 	redirect('kritik/add_kritik/'.$id);
	                }else{
	                		date_default_timezone_set("Asia/Bangkok");
                		$id_user = $this->session->userdata("id_user");

						 $data = array(
				        	'message' 		=> $this->input->post("message"),
				        	'id_user' 		=> $id_user,
				        	'created_ct_at' => date("Y-m-d h:i:sa")
				             );

						 $field = "content";
						 $sukses = $this->global_models->insert_data($field,$data);

						 if ($sukses) {
						 	$prefix = "CT";
						 	$pk = $this->generate_pk($prefix,$sukses->no_content);

						 	
						 	$jenis_content = $this->uri->segment(3);

						 	$data_update = array(
				        	'id_content' => $pk,
				        	'jenis_content' => $jenis_content
				             );

						 	$this->db->set($data_update);
			                $this->db->where('no_content', $sukses->no_content);
			                $this->db->update('content');

						 	$this->session->set_flashdata('msg', 'Data berhasil diinput. ');
						 	redirect('kritik/add_kritik/'.$id);
	                	}
                
				 	}
				}
			}
	}

	public function do_add_balas(){
		$id = $this->uri->segment(3);
		$this->form_validation->set_rules('message', 'Pesan', 'required');

		if ($this->form_validation->run() == FALSE)
                {
                        $this->add_kritik();   
                }
                else{
	                if (!empty($this->input->post("jenis_content"))) {
	                	date_default_timezone_set("Asia/Bangkok");
                		$id_user = $this->session->userdata("id_user");

						 $data = array(
				        	'message' 		=> $this->input->post("message"),
				        	'jenis_content' => $this->input->post("jenis_content"),
				        	'id_user' 		=> $id_user,
				        	'created_ct_at' => date("Y-m-d h:i:sa")
				             );

						 $field = "content";
						 $sukses = $this->global_models->insert_data($field,$data);

						 if ($sukses) {
						 	$prefix = "CT";
						 	$pk = $this->generate_pk($prefix,$sukses->no_content);

						 	$prefix = "KDR";
						 	$jenis_content = $this->generate_pk($prefix,$sukses->no_content);

						 	$data_update = array(
				        	'id_content' => $pk
				             );

						 	$this->db->set($data_update);
			                $this->db->where('no_content', $sukses->no_content);
			                $this->db->update('content');

						 	$this->session->set_flashdata('msg', 'Data berhasil diinput. ');
						 	redirect('kritik/add_kritik/'.$id);
	                }else{
	                		date_default_timezone_set("Asia/Bangkok");
                		$id_user = $this->session->userdata("id_user");

						 $data = array(
				        	'message' 		=> $this->input->post("message"),
				        	'id_user' 		=> $id_user,
				        	'created_ct_at' => date("Y-m-d h:i:sa")
				             );

						 $field = "content";
						 $sukses = $this->global_models->insert_data($field,$data);

						 if ($sukses) {
						 	$prefix = "CT";
						 	$pk = $this->generate_pk($prefix,$sukses->no_content);

						 	$prefix = "KDR";
						 	$jenis_content = $this->generate_pk($prefix,$sukses->no_content);

						 	$data_update = array(
				        	'id_content' => $pk,
				        	'jenis_content' => $jenis_content
				             );

						 	$this->db->set($data_update);
			                $this->db->where('no_content', $sukses->no_content);
			                $this->db->update('content');

						 	$this->session->set_flashdata('msg', 'Data berhasil diinput. ');
						 	redirect('kritik/add_kritik/'.$id);
	                	}
                
				 	}
				}
			}
	}

	public function list(){
		$data= array("menu_back" => "menu_kritik");
			
		$this->session->set_userdata($data);
		
		$query = $this->db->query("SELECT *  FROM USER u
LEFT JOIN (SELECT * FROM content GROUP BY jenis_content HAVING COUNT(*) >= 1) c ON u.id_user = c.id_user
WHERE c.jenis_content 
LIKE '%KDR%' ESCAPE '!'
ORDER BY c.created_ct_at ASC");

		$this->data['kritik'] = $query->result();

		$this->content = 'content/kritik/vDatakritik';
		$this->layout();
	}

	public function edit_kritik(){
		$id = $this->uri->segment(3);

		$this->db->select("*");
      	$this->db->from('content');
      	$this->db->where('content.id_content = "'.$id.'"');
     	$query = $this->db->get();

     	$this->data['kritik'] = $query->result();
		$this->content = 'content/kritik/vEditDatakritik';
		$this->layout();
	}

	public function do_edit_kritik(){

		$id = $this->uri->segment(3);

		$this->form_validation->set_rules('nama_kritik', 'Nama Lengkap', 'required');
		$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
		$this->form_validation->set_rules('no_telp_kritik', 'Nomor Telepon', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');

		 if ($this->form_validation->run() == FALSE)
                {
                        $this->index();
                       
                }
                else{

				 $data = array(
		        	'nama_kritik' => $this->input->post("nama_kritik"),
		        	'jenis_kelamin_kritik' => $this->input->post("jenis_kelamin"),
		        	'no_telp_kritik' => $this->input->post("no_telp_kritik"),
		        	'alamat_kritik' => $this->input->post("alamat")
		        					
		             );
				
				
			    $query = $this->db->update('kritik',$data,array('id_content' => $id));
			   
			   	 if ($query) {
				 	$this->session->set_flashdata('edit', 'Data kritik berhasil diedit');
				 	redirect('kritik/list');
				 }
			}

	}

	public function delete_balasan(){
		$id = $this->uri->segment(3);
		$id_content = $this->uri->segment(4);
		$this->db->delete('content', array('id_content' => $id));

		redirect('kritik/add_kritik/'.$id_content);
	}

	public function delete_kritik(){
		$id = $this->uri->segment(3);
		$this->db->delete('content', array('jenis_content' => $id));

		redirect('kritik/list');
	}

	public function json(){
		$this->load->library('datatables');
        $this->datatables->select('*');
        $this->datatables->from('kritik');
        return print_r($this->datatables->generate());
	}
	

}