<?php defined('BASEPATH') or exit('No direct script access allowed.');
/**
 * 
 */
require('Dashboard.php');

class Konten extends Dashboard
{
	function __construct()
	{
		parent:: __construct();
	}

	public function index(){
			$this->list();	
	}

	public function add_konten(){
		$this->db->select("*");
      	$this->db->from('pelanggan');
     	$this->db->join('agen', 'pelanggan.id_agen = agen.id_agen');
     	$this->db->join('user', 'pelanggan.id_user = user.id_user');
     	$this->db->where('verified_at is NOT NULL', NULL, FALSE);
      	$query2 = $this->db->get();

      	$this->data['pelanggan'] = $query2->result();
		$this->data['content'] = $this->global_models->getTable('content');
		$this->content = 'content/konten/vInputkonten';
		$this->layout();
	}

	public function generate_pk($prefix,$number){
		switch (strlen($number)) {
		    case 1:
		    	$digit = 2;
		    break;

		    case 2:
		        $digit = 1;
		    break;

		    case 3:
		        $digit = 0;
		    break;

		    default:
		        $digit = 0;
		    break;
		}

        $primary_key = $prefix.str_repeat("0", $digit).$number;
        return $primary_key; 
	}

	public function do_add_konten(){
		$this->form_validation->set_rules('message', 'Pesan', 'required');
		$this->form_validation->set_rules('id_user', 'Pelanggan', 'required');

		if ($this->form_validation->run() == FALSE)
                {
                        $this->add_konten();   
                }
                else{

				 $data = array(
		        	'message' => $this->input->post("message"),
		        	'id_user' => $this->input->post("id_user"),
		        	'created_ct_at' => date("Y-m-d h:i:sa")
		             );

				 $field = "content";
				 $sukses = $this->global_models->insert_data($field,$data);

				 if ($sukses) {
				 	$prefix = "CT";
				 	$pk_content = $this->generate_pk($prefix,$sukses->no_content);

				 	$prefix2 = "PR";
				 	$pk_jenis_content = $this->generate_pk($prefix2,$sukses->no_content);

				 	$data_update = array(
		        	'id_content' => $pk_content,
		        	'jenis_content'	 => $pk_jenis_content
		             );

				 	$this->db->set($data_update);
	                $this->db->where('no_content', $sukses->no_content);
	                $this->db->update('content');

				 	$this->session->set_flashdata('msg', 'Data berhasil diinput. ');
				 	redirect('konten/add_konten');
				 }
			}
	}

	public function list(){
		$this->db->select("*");
      	$this->db->from('content');
      	$this->db->join('pelanggan', 'content.id_user = pelanggan.id_user','left');
      	$this->db->join('user', 'pelanggan.id_user = user.id_user');
      	$this->db->like('content.jenis_content', 'PR');

      	$query = $this->db->get();
		$this->data['konten'] = $query->result();

		$data= array("menu_back" => "menu_konten");
			
		$this->session->set_userdata($data);

		$this->content = 'content/konten/vDatakonten';
		$this->layout();
	}

	public function edit_konten(){
		$id = $this->uri->segment(3);

		$this->db->select("*");
      	$this->db->from('content');
      	$this->db->join('user', 'content.id_user = user.id_user');
      	$this->db->join('pelanggan', 'content.id_user = pelanggan.id_user');
      	$this->db->where('jenis_content = "'.$id.'"');
      	
     	$query = $this->db->get();

     	$this->db->select("*");
      	$this->db->from('pelanggan');
     	$this->db->join('agen', 'pelanggan.id_agen = agen.id_agen');
     	$this->db->join('user', 'pelanggan.id_user = user.id_user');
     	$this->db->where('verified_at is NOT NULL', NULL, FALSE);
      	$query2 = $this->db->get();

      	$this->data['pelanggan'] = $query2->result();
     	$this->data['konten'] = $query->result();
		$this->content = 'content/konten/vEditDatakonten';
		$this->layout();
	}

	public function do_edit_konten(){

		$id = $this->uri->segment(3);

		$this->form_validation->set_rules('message', 'Pesan', 'required');
		$this->form_validation->set_rules('id_user', 'Pelanggan', 'required');

		 if ($this->form_validation->run() == FALSE)
                {
                        $this->index();
                       
                }
                else{

				  $data = array(
		        	'message' => $this->input->post("message"),
		        	'id_user' => $this->input->post("id_user")
		             );
				
				
			    $query = $this->db->update('content',$data,array('id_content' => $id));
			   
			   	 if ($query) {
				 	$this->session->set_flashdata('edit', 'Data konten berhasil diedit');
				 	redirect('konten/list');
				 }
			}

	}

	public function delete_konten(){
		$id = $this->uri->segment(3);
		$this->db->delete('content', array('jenis_content' => $id));

		redirect('konten/list');
	}

	public function json(){
		$this->load->library('datatables');
        $this->datatables->select('*');
        $this->datatables->from('content');
        return print_r($this->datatables->generate());
	}
	

}