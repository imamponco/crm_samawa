<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Login extends CI_Controller
{
	function __construct()
	{
		parent:: __construct();
		
		$this->load->model('user_models');
		$this->load->library('form_validation');
		# code...
	}

	public function index(){
		
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		
		
		if ($this->form_validation->run() == FALSE)
                {
                	$this->load->view('view_login');

                        //login checker
                        $is_login = $this->session->userdata('is_login');
                        $permission = $this->session->userdata('permission');

						if ($is_login === TRUE && $permission != "CO") {
							redirect('dashboard');
						}else{
						
						}
                }
	}

	public function validation_front(){
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == FALSE)
        {	
        	echo "<script type='text/javascript'>alert('Gagal login');
			window.location.href='".base_url()."front/index';
        	</script>";
        }else{

		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$cookie = $this->input->post('cookie');

		$login = $this->user_models->get($email);
		$cek = password_verify($password,$login->password);

		if($email == $login->email AND empty($login->verified_at)){
			echo "<script type='text/javascript'>alert('Gagal login');
			window.location.href='".base_url()."front/index';
        	</script>";
		}else if ($login->email == null) {
			echo "<script type='text/javascript'>alert('Gagal login');
			window.location.href='".base_url()."front/index';
        	</script>";
		}

		if ($email == $login->email AND $cek) {
			if($cookie == "on"){
				setcookie("email", $email, time()+3600);
				setcookie("password", $password, time()+3600);
			}
			
		    $permission = substr($login->id_user,0,2);
		    
			$data= array(
				"is_login_user" 	 => TRUE,
				"id_user_pelanggan"  => $login->id_user,
				"permission_user" 	 => $permission,
				"nama_user" 	   	 => $login->username,
				"menu"				 => ""
			);
			
			$this->session->set_userdata($data);
			redirect('/front/index');
		}
		else{
			echo "<script type='text/javascript'>alert('Gagal login');
			window.location.href='".base_url()."front/index';
        	</script>";
			}
		}
	}

	public function validation(){
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('view_login');
        }else{

		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$cookie = $this->input->post('cookie');

		$login = $this->user_models->get($email);
		$cek = password_verify($password,$login->password);

		if($email == $login->email AND empty($login->verified_at)){
			$this->session->set_flashdata('login', 'Gagal login.');
			redirect('login');
		}else if ($login->email == null) {
			$this->session->set_flashdata('login', 'Gagal login.');
			redirect('login');
		}

		if ($email == $login->email AND $cek) {
			if($cookie == "on"){
				setcookie("email", $email, time()+3600);
				setcookie("password", $password, time()+3600);
			}
			
		    $permission = substr($login->id_user,0,2);

			$data= array(
				"is_login" 	 => TRUE,
				"id_user"  	 => $login->id_user,
				"permission" => $permission,
				"nama" 	   	 => $login->username
			);
			$this->session->set_userdata($data);
			redirect('dashboard');
		}
		else{
			$this->session->set_flashdata('login', 'Gagal login.');
			redirect('login');
			}
		}
	}

	public function reset_password(){
		$id = $this->uri->segment(3);
		$this->load->view('reset_password');
	}

	public function do_reset_password(){
		$old_password = $this->input->get('token');

		$this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('repassword', 'Re-Password', 'trim|required|matches[password]');

         if ($this->form_validation->run() == FALSE){
           $this->reset_password();   
         }else{
            $password =  password_hash($this->input->post("password"), PASSWORD_DEFAULT);
	         $this->db->set('password', $password);
	         $this->db->where('password', $old_password);
	         $sukses = $this->db->update('user');
	         if($sukses){
                 $this->session->set_flashdata('reset_password', 'Password anda telah terganti! Silahkan login');
                 redirect('login');
             }
		}
	}

	public function forgot_password(){
		$email = $this->input->post('email');
		$this->db->select("*");
      	$this->db->from('user');
      	$this->db->where("email = '".$email."'");
      	$query = $this->db->get()->result();

      	$config['protocol']    = 'smtp';
        $config['smtp_host']    = 'ssl://smtp.gmail.com';
        $config['smtp_port']    = '465';
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = 'imam.taufiq16@mhs.uinjkt.ac.id';
        $config['smtp_pass']    = '###';
        $config['charset']    = 'utf-8';
        $config['newline']    = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $config['validation'] = TRUE;
	    $this->load->library('email',$config);
	    $this->email->set_newline("\r\n");

	    $this->email->from('technicalsupport@samawatour.com','Admin Samawa Tour');
	    $this->email->to('$query[0]->email');
	    $this->email->subject('Reset Password');
	    $this->email->message('
	    	<center><b>Hi '.$query[0]->username.'!</b> <br>
	    	<p>
	    	You recently requested to reset your password for your Samawa Tour account.
	    	Click the button below to reset it. <br>
	    	<button style="background-color: #008CBA; font-size:20px; padding: 14px 40px;"><a style="color:white;" href="<?=base_url?>/login/reset_password?token='.$query[0]->password.'">Here </a></button><br><br>
	    	If you did not request a password reset, please ignore this email or reply to let us know.<br>
	    	<b> Thanks, </b><br><br>
	    	Admin Samawa Tour and Travel
	    	</p>
	    	</center>
	    	');

		if($this->email->send()){
            $this->session->set_flashdata("email_sent","Congratulations your new password has been sent!.");
            redirect('login');
		}
        else{
           show_error($this->email->print_debugger());
   		   return false;
        }
      	
      
      	
	}

	public function logout(){
        	$this->session->unset_userdata(array('is_login', 'id_user','permission','nama'));
        	redirect('login');
    }

    public function logout_front(){
        	$this->session->unset_userdata(array('is_login_user', 'id_user_pelanggan','permission_user','nama_user'));
        	redirect('front/index','refresh');
    }
	

}
?>