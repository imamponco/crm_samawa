<?php defined('BASEPATH') or exit('No direct script access allowed.');
/**
 * 
 */
require('Dashboard.php');

class Laporan extends Dashboard
{
	function __construct()
	{
		parent:: __construct();
	}

	public function index(){
			$this->list();	
	}

	public function laporan_keuangan(){
		$data= array("menu_back" => "menu_keuangan");
			
		$this->session->set_userdata($data);

		$filter = $this->input->post('filter');
		$first_date 	= $this->input->post("first_date");
		$end_date 		= $this->input->post("end_date");

		
		if($filter==1 && !empty($first_date) AND !empty($end_date)){
			$query = $this->db->query("SELECT *,h.*,h.status as flag FROM header_pemesanan h
			LEFT JOIN detail_pemesanan d ON h.id_header_pemesanan = d.id_header_pemesanan
			LEFT JOIN harga hr ON hr.id_harga = d.id_harga
			LEFT JOIN pelanggan p ON p.nik = h.nik
			LEFT JOIN user u ON u.id_user = p.id_user
			WHERE h.tanggal_pesan BETWEEN '".$first_date."' AND '".$end_date."' AND h.status = 1 ");
	      	

			$this->data['keuangan'] = $query->result();

			$this->content = 'content/laporan/vLaporanKeuangan';
			$this->layout();

		}else if($filter==0 && !empty($first_date) && !empty($end_date)){

			$query = $this->db->query("SELECT *,h.*,h.status as flag FROM header_pemesanan h
			LEFT JOIN detail_pemesanan d ON h.id_header_pemesanan = d.id_header_pemesanan
			LEFT JOIN harga hr ON hr.id_harga = d.id_harga
			LEFT JOIN pelanggan p ON p.nik = h.nik
			LEFT JOIN user u ON u.id_user = p.id_user
			WHERE h.tanggal_pesan BETWEEN '".$first_date."' AND '".$end_date."' AND h.status = 0 ");

			$this->data['keuangan'] = $query->result();
			
			$this->content = 'content/laporan/vLaporanKeuangan';
			$this->layout();
		}else if(!empty($first_date) && !empty($end_date)){

			$query = $this->db->query("SELECT *,h.*,h.status as flag FROM header_pemesanan h
			LEFT JOIN detail_pemesanan d ON h.id_header_pemesanan = d.id_header_pemesanan
			LEFT JOIN harga hr ON hr.id_harga = d.id_harga
			LEFT JOIN pelanggan p ON p.nik = h.nik
			LEFT JOIN user u ON u.id_user = p.id_user
			WHERE h.tanggal_pesan BETWEEN '".$first_date."' AND '".$end_date."' AND h.status = 0 ");

			$this->data['keuangan'] = $query->result();
			
			$this->content = 'content/laporan/vLaporanKeuangan';
			$this->layout();
		}else if($filter==1){

			$query = $this->db->query("SELECT *,h.*,h.status as flag FROM header_pemesanan h
			LEFT JOIN detail_pemesanan d ON h.id_header_pemesanan = d.id_header_pemesanan
			LEFT JOIN harga hr ON hr.id_harga = d.id_harga
			LEFT JOIN pelanggan p ON p.nik = h.nik
			LEFT JOIN user u ON u.id_user = p.id_user
			WHERE h.status = 1");

			$this->data['keuangan'] = $query->result();
			
			$this->content = 'content/laporan/vLaporanKeuangan';
			$this->layout();
		}else if($filter==0 || $filter==NULL){

			$query = $this->db->query("SELECT *,h.*,h.status as flag FROM header_pemesanan h
			LEFT JOIN detail_pemesanan d ON h.id_header_pemesanan = d.id_header_pemesanan
			LEFT JOIN harga hr ON hr.id_harga = d.id_harga
			LEFT JOIN pelanggan p ON p.nik = h.nik
			LEFT JOIN user u ON u.id_user = p.id_user
			WHERE h.status = 0");

			$this->data['keuangan'] = $query->result();
			
			$this->content = 'content/laporan/vLaporanKeuangan';
			$this->layout();
		}
			
	}

	public function laporan_pelanggan(){
		$data= array("menu_back" => "menu_lap_pelanggan");
			
		$this->session->set_userdata($data);

		$filter 		= $this->input->post('filter');
		$first_date 	= $this->input->post("first_date");
		$end_date 		= $this->input->post("end_date");

		if(!empty($first_date) AND !empty($end_date)){
			$this->db->select("*");
	      	$this->db->from('user');
	      	$this->db->join('pelanggan', 'user.id_user = pelanggan.id_user','left');
	      	$this->db->join('agen', 'agen.id_agen = pelanggan.id_agen','left');
	      	$this->db->where('verified_at BETWEEN "'.$first_date. '" AND "'.$end_date.'"');
	      	$this->db->where('verified_at is NOT NULL', NULL, FALSE);
	      	$this->db->like('user.id_user', 'CO');
	      	$query = $this->db->get();

	      	$this->db->select("*");
	      	$this->db->from('agen');
	      	$query2 = $this->db->get()->result();

			$this->data['pelanggan'] = $query->result();
			$this->data['agen'] = $query2;

			$this->content = 'content/laporan/vLaporanPelanggan';
			$this->layout();
		}else{
			$this->db->select("*");
	      	$this->db->from('user');
	      	$this->db->join('pelanggan', 'user.id_user = pelanggan.id_user','left');
	      	$this->db->join('agen', 'agen.id_agen = pelanggan.id_agen','left');
	      	$this->db->where('verified_at is NOT NULL', NULL, FALSE);
	      	$this->db->like('user.id_user', 'CO');
	      	$query = $this->db->get();

	      	$this->db->select("*");
	      	$this->db->from('agen');
	      	$query2 = $this->db->get()->result();

			$this->data['pelanggan'] = $query->result();
			$this->data['agen'] = $query2;
			
			$this->content = 'content/laporan/vLaporanPelanggan';
			$this->layout();
		}
	}

	public function laporan_keberangkatan(){
		$data= array("menu_back" => "menu_lap_keberangkatan");
			
		$this->session->set_userdata($data);

		$filter = $this->input->post('filter');
		$first_date 	= $this->input->post("first_date");
		$end_date 		= $this->input->post("end_date");

		//belum selesai
		if($filter==NULL || $filter==0 AND empty($first_date) AND empty($end_date)){
			$this->db->select("*");
	      	$this->db->from('jadwal');
	      	$this->db->join('paket', 'jadwal.id_paket = paket.id_paket','left');
	      	$this->db->where('status',0);
	      	$query = $this->db->get();

			$this->data['jadwal'] = $query->result();

			

			$this->content = 'content/laporan/vLaporanKeberangkatan';
			$this->layout();
		//telah selesai
		}else if($filter==1 AND !empty($first_date) AND !empty($end_date)){
			$this->db->select("*");
	      	$this->db->from('jadwal');
	      	$this->db->join('paket', 'jadwal.id_paket = paket.id_paket','left');
	      	$this->db->where('tgl_keberangkatan BETWEEN "'.$first_date. '" AND "'.$end_date.'"');
	      	$this->db->where('status',1);
	      	$query = $this->db->get();

			$this->data['jadwal'] = $query->result();

			$this->content = 'content/laporan/vLaporanKeberangkatan';
			$this->layout();
		}else if($filter==0 AND !empty($first_date) AND !empty($end_date)){
			$this->db->select("*");
	      	$this->db->from('jadwal');
	      	$this->db->join('paket', 'jadwal.id_paket = paket.id_paket','left');
	      	$this->db->where('tgl_keberangkatan BETWEEN "'.$first_date. '" AND "'.$end_date.'"');
	      	$this->db->where('status',0);
	      	$query = $this->db->get();

			$this->data['jadwal'] = $query->result();

			$this->content = 'content/laporan/vLaporanKeberangkatan';
			$this->layout();
		}elseif ($filter==1) {
			$this->db->select("*");
	      	$this->db->from('jadwal');
	      	$this->db->join('paket', 'jadwal.id_paket = paket.id_paket','left');
	      	$this->db->where('status',1);
	      	$query = $this->db->get();

			$this->data['jadwal'] = $query->result();

			$this->content = 'content/laporan/vLaporanKeberangkatan';
			$this->layout();
		}else{
			$this->db->select("*");
	      	$this->db->from('jadwal');
	      	$this->db->join('paket', 'jadwal.id_paket = paket.id_paket','left');
	      	$this->db->where('tgl_keberangkatan BETWEEN "'.$first_date. '" AND "'.$end_date.'"');
	      	$this->db->where('status',0);
	      	$query = $this->db->get();

			$this->data['jadwal'] = $query->result();

			$this->content = 'content/laporan/vLaporanKeberangkatan';
			$this->layout();
		}
		
	}
	

}