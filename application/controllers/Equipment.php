<?php defined('BASEPATH') or exit('No direct script access allowed.');
/**
 * 
 */
require('Dashboard.php');

class Equipment extends Dashboard
{
	function __construct()
	{
		parent:: __construct();
	}

	public function index(){
			$this->list();	
	}

	public function add_equipment(){
		$this->data['equipment'] = $this->global_models->getTable('equipment');
		$this->content = 'content/equipment/vInputEquipment';
		$this->layout();
	}

	public function add_transaksi(){
		$id_barang = $this->uri->segment(3);
		$this->db->select("*");
      	$this->db->from('equipment');
      	$this->db->where('id_barang',$id_barang);
      	$query = $this->db->get();

		$this->data['equipment'] = $query->result();

		$this->content = 'content/equipment/vInputTransaksi';
		$this->layout();
	}

	public function generate_pk($prefix,$number){
		switch (strlen($number)) {
		    case 1:
		    	$digit = 2;
		    break;

		    case 2:
		        $digit = 1;
		    break;

		    case 3:
		        $digit = 0;
		    break;

		    default:
		        $digit = 0;
		    break;
		}
        $primary_key = $prefix.str_repeat("0", $digit).$number;
        return $primary_key; 
	}

	public function do_add_equipment(){
		$this->form_validation->set_rules('nama_equipment', 'Nama Barang', 'required|is_unique[equipment.nama_barang]|alpha');
		$this->form_validation->set_rules('stok', 'Stok Barang', 'required|integer');
		if ($this->form_validation->run() == FALSE)
                {
                        $this->add_equipment();   
                }
                else{

				 $data = array(
		        	'nama_barang' => $this->input->post("nama_equipment"),
		        	'stok' => $this->input->post("stok"),
		        	'id_user' => $this->session->userdata('id_user'),
		             );

				 $field = "equipment";
				 $sukses = $this->global_models->insert_data($field,$data);
				 
				 if ($sukses) {
				 	$prefix = "BR";
				 	$pk = $this->generate_pk($prefix,$sukses->no_eq);

				 	$this->db->set('id_barang',$pk);
	                $this->db->where('no_eq', $sukses->no_eq);
	                $this->db->update('equipment');

				 	$this->session->set_flashdata('msg', 'Data equipment berhasil diinput. ');
				 	$this->add_equipment();
				 }
			}
	}

	public function do_add_transaksi(){
		$id_barang = $this->uri->segment(3);

		$this->db->select("*");
      	$this->db->from('equipment');
      	$this->db->where('id_barang',$id_barang);
      	$current_data = $this->db->get()->row();

		$this->form_validation->set_rules('jumlah_barang', 'Jumlah Barang', 'required|integer');
		$this->form_validation->set_rules('total_harga', 'Total Harga', 'required|integer');
		if ($this->form_validation->run() == FALSE)
                {
                        $this->add_transaksi();   
                }
                else{

				 $data = array(
		        	'jumlah_barang' => $this->input->post("jumlah_barang"),
		        	'total_harga' => $this->input->post("total_harga"),
		        	'id_barang' => $id_barang,
		        	'created_tr_at' => date('Y-m-d')
		             );

				 $field = "transaksi_barang";
				 $sukses = $this->global_models->insert_data($field,$data);
				 
				 if ($sukses) {
				 	$prefix = "TB";
				 	$pk = $this->generate_pk($prefix,$sukses->no_transaksi);

				 	$this->db->set('id_transaksi_barang',$pk);
	                $this->db->where('no_transaksi', $sukses->no_transaksi);
	                $this->db->update('transaksi_barang');

	                $stok = $current_data->stok + $this->input->post('jumlah_barang');
	                $this->db->set('stok',$stok);
	                $this->db->where('id_barang', $id_barang);
	                $this->db->update('equipment');

				 	$this->session->set_flashdata('msg', 'Data transaksi berhasil diinput. ');
				 	$this->add_transaksi();
				 }
			}
	}

	public function list(){
		$this->db->select("*");
      	$this->db->from('equipment');
      	$this->db->order_by('id_barang','desc');
      	$query = $this->db->get();

		$this->data['equipment'] = $query->result();

		$data= array("menu_back" => "menu_equipment");
			
		$this->session->set_userdata($data);

		$this->content = 'content/equipment/vDataEquipment';
		$this->layout();
	}

	public function detail(){
		$id_barang = $this->uri->segment(3);
		$this->db->select("nama_barang");
      	$this->db->from('equipment');
      	$this->db->where('equipment.id_barang',$id_barang);
      	$query = $this->db->get();

		$this->db->select("*");
      	$this->db->from('equipment');
      	$this->db->join('transaksi_barang', 'equipment.id_barang = transaksi_barang.id_barang','left');
      	$this->db->where('transaksi_barang.id_barang',$id_barang);
      	$query2 = $this->db->get();

		$this->data['transaksi'] = $query2->result();
		
		$this->data['nama_barang'] = $query->result();

		$this->content = 'content/equipment/vDataTransaksiEquipment';
		$this->layout();
	}


	public function edit_equipment(){
		$id = $this->uri->segment(3);

		$this->db->select("*");
      	$this->db->from('equipment');
      	$this->db->where('equipment.id_barang = "'.$id.'"');
      	
     	$query = $this->db->get();

     	$this->data['equipment'] = $query->result();
		$this->content = 'content/equipment/vEditDataEquipment';
		$this->layout();
	}

	public function edit_transaksi(){
		$id = $this->uri->segment(3);

		$this->db->select("*");
      	$this->db->from('transaksi_barang');
      	$this->db->where('transaksi_barang.id_transaksi_barang = "'.$id.'"');
  
     	$query = $this->db->get();

     	$this->data['transaksi'] = $query->result();
		$this->content = 'content/equipment/vEditDataTransaksi';
		$this->layout();
	}

	public function do_edit_equipment(){

		$id = $this->uri->segment(3);

		$this->form_validation->set_rules('nama_equipment', 'Nama Barang', 'required');
		$this->form_validation->set_rules('stok', 'Stok', 'required');

		 if ($this->form_validation->run() == FALSE)
                {
                        $this->index();
                }
                else{
                 
				 $data = array(
		        	'nama_barang' => $this->input->post("nama_equipment"),
		        	'stok' => $this->input->post("stok")
		             );
				
				
			    $query = $this->db->update('equipment',$data,array('id_barang' => $id));

			   
			   	if ($query) {
				 	$this->session->set_flashdata('edit', 'Data equipment berhasil diedit');
				 	redirect('equipment/list');
				}
			}

	}

	public function do_edit_transaksi(){
		$id = $this->uri->segment(3);
		$id_barang = $this->uri->segment(4);

		$this->db->select("*");
      	$this->db->from('transaksi_barang');
      	$this->db->where('id_transaksi_barang',$id);
      	$current_transaksi = $this->db->get()->row();

		$this->db->select("*");
      	$this->db->from('equipment');
      	$this->db->where('id_barang',$id_barang);
      	$current_data = $this->db->get()->row();

		$this->form_validation->set_rules('jumlah_barang', 'Nama Barang', 'required|integer');
		$this->form_validation->set_rules('total_harga', 'Stok', 'required|integer');

		 if ($this->form_validation->run() == FALSE)
                {
                    $this->edit_transaksi();
                }
                else{
                 if($current_transaksi->jumlah_barang!=$this->input->post("jumlah_barang")){
                 	if ($this->input->post("jumlah_barang")>$current_transaksi->jumlah_barang) {
                 		$data = array(
			        	'jumlah_barang' => $this->input->post("jumlah_barang"),
			        	'total_harga' => $this->input->post("total_harga")
			             );
                 		$tambah = $this->input->post("jumlah_barang")-$current_transaksi->jumlah_barang;
                 		$stok = $current_data->stok + $tambah;

                 		$this->db->set('stok',$stok);
			            $this->db->where('id_barang', $id_barang);
			            $this->db->update('equipment');           		
                 	}elseif ($this->input->post("jumlah_barang")<$current_transaksi->jumlah_barang) {
                 		$data = array(
			        	'jumlah_barang' => $this->input->post("jumlah_barang"),
			        	'total_harga' => $this->input->post("total_harga")
			             );
                 		$kurang = $current_transaksi->jumlah_barang-$this->input->post("jumlah_barang");
                 		$stok = $current_data->stok - $kurang;

                 		$this->db->set('stok',$stok);
			            $this->db->where('id_barang', $id_barang);
			            $this->db->update('equipment');
                 	}
                 }else{
                 	$data = array(
		        	'jumlah_barang' => $this->input->post("jumlah_barang"),
		        	'total_harga' => $this->input->post("total_harga")
		             );	
                 }

			    $query = $this->db->update('transaksi_barang',$data,array('id_transaksi_barang' => $id));

			   
			   	if ($query) {
				 	$this->session->set_flashdata('edit', 'Data equipment berhasil diedit');
				 	redirect('equipment/list');
				}
			}

	}

	public function delete_equipment(){
		$id = $this->uri->segment(3);
		$this->db->delete('equipment', array('id_barang' => $id));

		redirect('equipment/list');
	}

	public function delete_transaksi(){
		$id = $this->uri->segment(3);
		$id_barang = $this->uri->segment(4);

		$this->db->select("*");
      	$this->db->from('equipment');
      	$this->db->where('id_barang',$id_barang);
      	$current_data = $this->db->get()->row();

      	$this->db->select("*");
      	$this->db->from('transaksi_barang');
      	$this->db->where('id_transaksi_barang',$id);
      	$current_transaksi = $this->db->get()->row();

		$delete = $this->db->delete('transaksi_barang', array('id_transaksi_barang' => $id));

		if($delete){
			$stok = $current_data->stok - $current_transaksi->jumlah_barang;
            $this->db->set('stok',$stok);
            $this->db->where('id_barang', $id_barang);
            $this->db->update('equipment');

	        redirect('equipment/detail/'.$id_barang);
		}

		
	}

	public function json(){
		$this->load->library('datatables');
        $this->datatables->select('*');
        $this->datatables->from('equipment');
        return print_r($this->datatables->generate());
	}
	

}