<?php defined('BASEPATH') or exit('No direct script access allowed.');
/**
 * 
 */
require('Dashboard.php');

class Ajax extends CI_Controller
{

	function fill_info()
	{
	    // retrieve the group and add to the data array
	    $id_paket = $this->input->post('id_paket');
	    $data = "0.00";
	    if($id_paket)
	    {
	        $this->load->model('global_models');
	        $baseamount = $this->global_models->getharga($id_paket);
	        echo json_encode($baseamount);
	    }
	    else 
	    {
	        echo $data;
	    }
	}

	function _create_thumbs($file_name){
        // Image resizing config
        $config = array(
            // Image Large
            array(
                'image_library' => 'GD2',
                'source_image'  => '././assets/images/'.$file_name,
                'maintain_ratio'=> FALSE,
                'width'         => 700,
                'height'        => 467,
                'new_image'     => '././assets/images/large/'.$file_name
                ),
    		);
 
        $this->load->library('image_lib', $config[0]);
        foreach ($config as $item){
            $this->image_lib->initialize($item);
            if(!$this->image_lib->resize()){
                return false;
            }
            $this->image_lib->clear();
        }
    }

	function upload(){
		$this->load->model('global_models');
		$this->load->library('upload');

		$config['upload_path'] = '././assets/images/'; //path folder
		$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang image yang dizinkan
		$config['encrypt_name'] = TRUE; //enkripsi nama file
				       
	    $this->upload->initialize($config);

		$gambar = $_FILES['file']['name'];
		$id = $this->input->post('id_header');
		$no = $this->input->post('no');
		
		if (!empty($gambar)) {
			if($this->upload->do_upload("file")){
            $upload = $this->upload->data();
            // Compress Image
            $img = $upload['file_name'];
            $this->_create_thumbs($img);

            $update = array(
            	'bukti_pembayaran' => $img
            );

            $query = $this->db->update('header_pemesanan',$update,array('id_header_pemesanan' => $id));

            if ($query) {
			 	$this->session->set_flashdata('pesan', 'Anda berhasil melakukan pemesanan! Silahkan konfirmasi pembayaran.');
			 	
	            $berhasil = "
				<form action='#' method='post'>
	            <a href='#' id='delete-".$no."'onclick='delete_image(this.id)'>
		            <input type='hidden' id='image_name-".$no."' value='".$img."'>
		            <input type='hidden' id='id_header-".$no."' value='".$id."'> 
		            <input type='hidden' id='no-".$no."' value='".$no."'>  
                <button type='button'  class='btn btn-block btn-danger'>Delete</button>
				</form>
                ";
                echo $berhasil;
			}else{
        		echo "<input type='file' id='pembayaran-".$no."' onclick='upload(this.id)' class='btn-block'>
                  <input type='hidden' value='".$id."' id='header-".$no."'>
                  <input type='hidden' value='".$no."' id='bar-".$no."'>
                  <div id='progress-wrp-".$no."'>
                      <div class='progress-bar'></div>
                      <div class='status'>0%</div>
                  </div>";
			}
		  }else{
			  	echo "<input type='file' id='pembayaran-".$no."' onclick='upload(this.id)' class='btn-block'>
	                  <input type='hidden' value='".$id."' id='header-".$no."'>
	                  <input type='hidden' value='".$no."' id='bar-".$no."'>
	                  <div id='progress-wrp-".$no."'>
	                      <div class='progress-bar'></div>
	                      <div class='status'>0%</div>
	                  </div>";
		  }
		}
	}

	public function delete_image(){
		$this->load->model('global_models');
		$data = $this->input->post('data');
		$data = json_decode($data);

		$img_name = $data->image_name;
		$id_header = $data->id_header;
		$no = $data->no;

		$target = "assets/images/".$img_name;

		$update = array(
            	'bukti_pembayaran' => NULL
           		);

		$query = $this->db->update('header_pemesanan',$update ,array('id_header_pemesanan' => $id_header));

		if($query){
			unlink($target);
			echo "<input type='file' id='pembayaran-".$no."' onclick='upload(this.id)' class='btn-block'>
                  <input type='hidden' value='".$id_header."' id='header-".$no."'>
                  <input type='hidden' value='".$no."' id='bar-".$no."'>
                  <div id='progress-wrp-".$no."'>
                      <div class='progress-bar'></div>
                      <div class='status'>0%</div>
                  </div>";
		}

	}	

	function jadwal()
	{
		 $id_paket = $this->input->post('id_paket');
		// var_dump($total);
		// die;
	    if($id_paket)
	    {
	        $this->load->model('global_models');
	        $baseamount = $this->global_models->getjadwal($id_paket);
	        if ($baseamount){
	        	$option ='<select class="form-control" name="id_jadwal[]"  id="id_jadwal-" required="required">
	         		  <option disabled selected value="">-- Pilih Jadwal --</option>';
	        foreach ($baseamount as $row) {
	        	$option = $option."<option value='".$row->id_jadwal."'>".$row->tgl_keberangkatan." ".$row->jam_terbang."</option>";
	        	// $option = $option."<input type='hidden' name='value_jadwal' value='".$row->tgl_keberangkatan."'</input>";
	        }
	        $option.= "</select>";
	        echo $option;
	    }else{
	    	$option ='<select class="form-control" name="id_jadwal[]"  id="id_jadwal-1" required="required">
	         		  <option disabled selected value="">-- Pilih Jadwal --</option>
	         		  <option value="" selected="selected">Jadwal Belum Tersedia</option>';
	        
	        $option.= "</select>";
	        echo $option;
	    }
	        
	    }
	    else 
	    {
	        echo "<option value=''></option>";
	    }
	}

	function get_paket_by_id(){
		$this->load->model('global_models');
		$id_paket = $this->input->post('id_paket');

		$row = $this->global_models->get_paket_by_id($id_paket);

		echo json_encode($row);
	}

	function get_kuota(){
		$this->load->model('global_models');
		$data = $this->input->post('data');
		$data = json_decode($data);

		$result = $this->global_models->get_kuota($data);

		echo json_encode($result);
	}

	function pesan_paket(){
		date_default_timezone_set("Asia/Bangkok");
		$this->load->model('global_models');
		$data = $this->input->post('data');
		$data = json_decode($data);

		$kuota[0] = $data->id_jadwal;
		$kuota[1] = $data->id_paket;

		$result = $this->global_models->get_kuota($kuota);

		$this->db->select("*");
      	$this->db->from('paket');
      	$this->db->join('harga', 'paket.id_paket = harga.id_paket','left');
      	$this->db->where('paket.id_paket = "'.$data->id_paket.'"');
      	$this->db->where('harga.deleted_at',null);
      	$query = $this->db->get();

     	$current_data = $query->row();

		$kuota = $result[1];
		$tersedia = $result[0];

		$cek = $kuota - $data->pax;

		if(!($data->pax < 0) && !($data->pax > $tersedia) && !($data->pax == 0)){
			$header = array(
	                'nik' => $data->nik_konfirmasi,
	                'tanggal_pesan'    => date("Y-m-d H:i:s"),
	                'subtotal' => $data->subtotal,
	                'grand_total' => $data->grandtotal,
	                'status' => 0
	             	);
			$sukses = $this->global_models->insert_data('header_pemesanan',$header);

			if ($sukses) {
	            $pk_header = $this->generate_pk('HD',$sukses->no_hd);

	            $this->db->set('id_header_pemesanan', $pk_header);
	            $this->db->where('no_hd', $sukses->no_hd);
	            $this->db->update('header_pemesanan');

			$detail = array(
	                'id_header_pemesanan' => $pk_header,
	                'id_harga'    => $current_data->id_harga,
	                'id_paket'    => $data->id_paket,
	                'id_jadwal' => $data->id_jadwal,
	                'pax' => $data->pax
	             	);

			$sukses2 = $this->global_models->insert_data('detail_pemesanan',$detail);
				if($sukses2){
					$pk_detail = $this->generate_pk('DET',$sukses2->no_dt);

		            $this->db->set('id_detail_pemesanan', $pk_detail);
		            $this->db->where('no_dt', $sukses2->no_dt);
		            $this->db->update('detail_pemesanan');

		            $query = "INSERT INTO `notifikasi` (`subjek_notifikasi`, `teks_notifikasi`, `status_notifikasi`,`id_user`) VALUES ('New pemesanan', 'terdapat pemesanan baru', 0,'".$this->session->userdata('id_user_pelanggan')."')";
		            if ($query) {
		            	 $this->session->set_flashdata('pesan', 'Anda berhasil melakukan pemesanan! Silahkan konfirmasi pembayaran <a href="'.base_url().'front/itenary">disini</a> paling lambat 1 jam.');
			            $berhasil = "Anda berhasil melakukan pemesanan! Silahkan konfirmasi pembayaran <a href='".base_url()."front/itenary'>disini</a> paling lambat 1 jam dari waktu pemesanan.";
		                echo $berhasil;
		            }

		
				}else{
					$gagal = "Anda gagal melakukan pemesanan,silahkan coba lagi.";
					echo $gagal;
				}
			}else{
				$gagal = "Anda gagal melakukan pemesanan,silahkan coba lagi.";
				echo $gagal;
			}
	 	}else if($data->pax < 0 && $data->pax == 0){
	 		$gagal = "Anda salah memasukan jumlah pax!";
			echo $gagal;
	 	}else{
	 		$gagal = "Pemesanan gagal! Kuota telah habis atau pemesanan anda melebihi kuota yang tersedia, kuota yang tesedia saat ini adalah ".$tersedia." (slot)";
			echo $gagal;
	 	}

	}

	function get_paket(){
		$this->load->model('global_models');
		$total= $this->input->post('total');
		$obj = $this->global_models->getTable('paket');

		echo '<td><span>'.$total.'</span></td>
			  <td>
			 	<div class="form-group">
			  	<select class="form-control" name="id_paket[]" onchange="jadwal(this.id),harga(this.id)" id="id_paket-'.$total.'" required="required">
			  	<option disabled selected value=""></option>
			  	';

			  	foreach ($obj as $row) {
			  		echo '<option value="'.$row->id_paket.'">'.$row->nama_paket.'</option>';
			  	}
		echo '	  		
			  	</select>
			  	</div>
			  </td>

			  <td>
                <div class="form-group">
                  <div id="hasiljadwal-'.$total.'">
                  <select class="form-control" name="id_jadwal[]"  id="id_jadwal-'.$total.'" required="required">
                    <option disabled selected value="">-- Pilih Jadwal --</option> 
                    
                  </select>
                  </div>
                </div>
              </td>
			  <td><input type="number" name="jumlah[]" min="1" onchange="hitung(this.value,'.$total.')" class="jumlah-'.$total.'" value="1"></td>
			  <td><input type="number" name="harga[]" readonly="readonly" class="harga-'.$total.'" value=""></td>
			  <td><input type="number" name="total[]" readonly="readonly" class="total-'.$total.'"></td>
			  <td></td>';
	}


	function generate_pk($prefix,$number){
        switch (strlen($number)) {
            case 1:
                $digit = 2;
            break;

            case 2:
                $digit = 1;
            break;

            case 3:
                $digit = 0;
            break;

            default:
                $digit = 0;
            break;
        }
        $primary_key = $prefix.str_repeat("0", $digit).$number;
        return $primary_key; 
    }


	function register_front(){
		$this->load->model('global_models');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('username', 'Full Name', 'required|alpha');
        $this->form_validation->set_rules('email_register', 'Email', 'required|is_unique[user.email]|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('repassword', 'Re-Password', 'trim|required|matches[password]');
        // $this->form_validation->set_rules('role', 'Role', 'required');
        // $this->form_validation->set_rules('id_user', 'User', 'required|is_unique[pelanggan.id_user]');
        // $this->form_validation->set_rules('nama', 'Nama Lengkap', 'required');
        $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
        $this->form_validation->set_rules('tgl_lahir', 'Tangal Lahir', 'required');
        $this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required|alpha');
        $this->form_validation->set_rules('no_passport', 'Nomor Passport', 'is_unique[pelanggan.no_passport]|alpha_numeric');
        // $this->form_validation->set_rules('issuing_office', 'Issuing Office', 'required');
        // $this->form_validation->set_rules('date_of_issue', 'Date of Issue', 'required');
        $this->form_validation->set_rules('nik', 'NIK', 'required|integer|is_unique[pelanggan.nik]');
        // $this->form_validation->set_rules('date_of_expiry', 'Date of Expiry', 'required');
        $this->form_validation->set_rules('no_telp', 'Nomor Telepon', 'required|integer');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        // $this->form_validation->set_rules('id_agen', 'Nama Agen');
        $this->form_validation->set_error_delimiters(" ");
		// $data = json_decode($this->input->post('data'));
		// $ini = json_decode($this->input->post('username);
		

		if ($this->form_validation->run() == FALSE){
		   $errors = json_encode(validation_errors());
		   // var_dump($errors);
           echo validation_errors(); 
        }else{
       	$id_agen = rtrim($this->input->post('id_agen'), '"');
		$password =  password_hash($this->input->post('password'), PASSWORD_DEFAULT);

                $user = array(
                    'username' => $this->input->post('username'),
                    'email'    => $this->input->post('email_register'),
                    'password' => $password,
                     );

                 $sukses = $this->global_models->insert_data('user',$user);

                 if ($sukses) {
                    $pk_user = $this->generate_pk('CO',$sukses->no);

                    $this->db->set('id_user', $pk_user);
                    $this->db->where('no', $sukses->no);
                    $this->db->update('user');

                $pelanggan = array(
                    'id_agen' => $id_agen,
                    'id_user' => $pk_user,
                    'jenis_kelamin' => $this->input->post('jenis_kelamin'),
                    'tanggal_lahir' => $this->input->post('tgl_lahir'),
                    'tempat_lahir' => $this->input->post('tempat_lahir'),
                    // 'no_passport' => $this->input->post('no_passport'),
                    // 'issuing_office' => $this->input->post('issuing_office'),
                    // 'date_of_issue' => $this->input->post('date_of_issue'),
                    // 'date_of_expiry' => $this->input->post('date_of_expiry'),
                    // 'relationship' => $this->input->post('relationship'),
                    'no_telp' => $this->input->post('no_telp'),
                    'alamat' => $this->input->post('alamat'),
                    'nik' => $this->input->post('nik'),
                    'no_passport' => $this->input->post('no_passport')
                    // 'room_type' => $this->input->post("room_type")           
                     );

                 $field = "pelanggan";
                 $sukses = $this->global_models->insert_data($field,$pelanggan);

	            $this->session->set_flashdata('register', 'Akun anda berhasil terdaftar! Silahkan login');
	            $berhasil = "Akun anda berhasil terdaftar!";
                echo $berhasil;
			}
		}
	}

	public function validation_front(){
		$this->load->model('global_models');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('view_login');
        }else{

		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$cookie = $this->input->post('cookie');

		$login = $this->user_models->get($email);
		$cek = password_verify($password,$login->password);

		if($email == $login->email AND empty($login->verified_at)){
			$this->session->set_flashdata('login', 'Gagal login.');
			redirect('/front/index');
		}else if ($login->email == null) {
			$this->session->set_flashdata('login', 'Gagal login.');
			redirect('/front/index');
		}

		if ($email == $login->email AND $cek) {
			if($cookie == "on"){
				setcookie("email", $email, time()+3600);
				setcookie("password", $password, time()+3600);
			}
			
		    $permission = substr($login->id_user,0,2);

			$data= array(
				"is_login" 	 => TRUE,
				"id_user"  	 => $login->id_user,
				"permission" => $permission,
				"nama" 	   	 => $login->username
			);
			$this->session->set_userdata($data);
			redirect('/front/index');
		}
		else{
			$this->session->set_flashdata('login', 'Gagal login.');
			redirect('/front/index');
			}
		}
	}

	public function get_number_notification(){
		$id_user = trim($this->input->post('data'), '"');
        $this->db->select('count("id_notifikasi") as jumlah');
        $this->db->from('notifikasi');
        $this->db->where('status_notifikasi',0);
        $this->db->where('id_user',$id_user);

        $hasil = $this->db->get()->result();

        if (!empty($hasil) && $hasil[0]->jumlah != 0) {
        	echo "
            <span class='badge badge-warning navbar-badge'>".(!empty($hasil) ? $hasil[0]->jumlah : "" )."</span>";
        }
          
	}

	public function get_notification(){

		$id_user = trim($this->input->post('data'), '"');

		$this->db->select("*");
      	$this->db->from('notifikasi');
      	$this->db->where("id_user = '".$id_user."'");
      	$this->db->where('status_notifikasi',0);
      	$this->db->order_by('id_user','desc');
      	$hasil = $this->db->get()->result();
      	$jumlah = count($hasil);

      	$this->db->set('status_notifikasi', 1);
        $this->db->where('id_user', $id_user);
        $this->db->update('notifikasi');

    	
    	if (!empty($hasil)) {
    	$link = trim($hasil[0]->subjek_notifikasi,"New ");
    		echo "<span class='dropdown-item dropdown-header'>".$jumlah." Notifications </span>
		      	<div class='dropdown-divider'></div>
			           <a href='".base_url().$link."/list' class='dropdown-item mr-2'>
			              ".$jumlah." ".$hasil[0]->subjek_notifikasi."
			            </a>
		      <span class='dropdown-item dropdown-footer'></span>
		          <div class='dropdown-divider'></div>
		            <a href='#' class='dropdown-item dropdown-footer' onclick='delete_notification();'>Clear All Notifications</a>
      		";
    	}else{
    		echo "<span class='dropdown-item dropdown-header'> Notifications </span>
		      	<div class='dropdown-divider'></div>
			           <a href='#' class='dropdown-item'>
			              <li>Tidak ada notifikasi terbaru</li>
			            </a>
				<span class='dropdown-item dropdown-footer'></span>
		          <div class='dropdown-divider'></div>
		            <a href='#' class='dropdown-item dropdown-footer' onclick='delete_notification();'>Clear All Notifications</a>
      		";
    	}
	          
	}

	public function clear_notification(){
		$id_user = trim($this->input->post('data'), '"');
		$this->db->delete('notifikasi', array('id_user' => $id_user));

		echo "berhasil delete";
	}


	
}